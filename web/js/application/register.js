
    $("#login-form input[type=submit]").on("click",function(event){
        event.preventDefault();

        var data = $("form#register").serialize();

        console.log(data);

        $.ajax({
            url:'/register_ajax?modal=1',
            method:"POST",
            data:data,
            dataType:'json',
            success:function(data, txtStatus, jqXHR) {
                console.log(data);
            },
            error:function(jqXHR, txtStatus, errorThrown){
                console.log(txtStatus);
            }
        });
    });

    $("a#login-btn").on("click",function(event){
        $("div#register.modal").modal('hide');
    });
