define( 'controller/emisora',[ 'marionette', 'blockui', 'underscore', 'package/paso02' ],
    function( Marionette, BlockUI, _, Package )
    {
        var Controller = Backbone.Marionette.Controller.extend({
            initialize:function(options)
            {
                this.app = options.app ;

                this.lugares = null ;
                this.lugar_active = null ;
            },
            re_render:function()
            {
                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);

                this.layout.lugares.show( new Package.View.Lugares({ collection : this.lugares })  );
                this.layout.emisoras.show( this.lugar_active.get('view') );
            },
            index:function(ubicacion)
            {
                var self = this;
                this.ubicacion = ubicacion ;

                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);

                self.lugares = new Backbone.Collection();
                this.ubicacion.lugares.each(function(lugar){
                    self.lugares.add( new Package.Model.Lugar(lugar.attributes) );
                });

                this.layout.lugares.show( new Package.View.Lugares({ collection : self.lugares }) );
                console.log(self.lugares.first());
                this.seleccionarLugar(self.lugares.first());
            },
            seleccionarLugar:function(lugar)
            {
                var self = this;
                var categoria = this.ubicacion.categoria;

                if( this.lugar_active != null )
                    this.lugar_active.set('active', false);

                this.lugar_active = lugar ;
                lugar.set('active',true);

                if( typeof lugar.get('repetidoras') == 'undefined' )
                {
                    var repetidoras = new Package.Model.Repetidoras([]);
                    repetidoras.meta('categoria' , categoria);
                    repetidoras.meta('lugar'     , lugar);

                    $.blockUI();

                    repetidoras.fetch({
                        success:function(responseObject,responseData){
                            $.unblockUI();

                            var view = new Package.View.Repetidoras({ collection : repetidoras });
                            self.layout.emisoras.show( view );

                            lugar.set('view',view);
                            lugar.set('repetidoras',repetidoras);
                        }
                    });
                }
                else self.layout.emisoras.show( lugar.get('view') );
            },
            seleccionarRepetidora : function(selected)
            {
                var self = this ;
                // retorna la cantidad de items seleccionados de esta categoria
                var collection = this.lugar_active.get('repetidoras') ;
                var repetidora = collection.get(selected) ;

                if( repetidora.get('selected') )
                    repetidora.set('selected',false);
                else 
                    repetidora.set('selected',true);

                var count = collection.where({'selected':true}).length;

                this.lugar_active.check( count );
            },
            getSelections:function()
            {
                var selecciones = new Array() ;

                this.lugares.each(function(lugar){
                    var collection = lugar.get('repetidoras');
                    var id = lugar.get('id');

                    if( typeof collection != 'undefined' )
                    {
                        var repetidoras = collection.where({'selected':true});
                        if( repetidoras.length != 0 )
                        {
                            selecciones[id] = {};
                            selecciones[id].lugar = lugar; // lugar del panel
                            selecciones[id].repetidoras = repetidoras; // array de repetidoras seleccionadas
                        }
                    }
                });

				return selecciones ;
            }
        });

        return Controller ;
    }
);
