define( 'controller/duracion',
    ['scrollbar','marionette','underscore','package/paso04','jquery.screwdefaultbuttonsV2'],
    function( Scrollbar, Marionette, _, Package, ScrewDefaultButtons )
    {
        var Controller = Backbone.Marionette.Controller.extend({
            re_render:function(){
                this.layout = new Package.Layout();
                this.app.main.show( this.layout );
                this.layout.duraciones.show( new Package.View.DuracionesCheckbox({ collection: this.duraciones }));
                this.layout.duracionesElegidas.show( new Package.View.DuracionesList({ collection : this.selections }));

                this.mostrarRepetidoras(this.duracionActiva);
            },
            initialize:function(options)
            {
                this.app = options.app ;
            },
            _initRepetidoras:function()
            {
                // genero la coleccion para el panel central
                var data = [];

                _.each(this.planificacion, function(seleccion){
                    var repetidoras = seleccion.get('repetidoras') ;

                    repetidoras.each(function(repetidora) {
                        if( repetidora.get('horarios').length > 0 )
                        {
                            var metadata = {};
                            metadata.id         = repetidora.get('id');
                            metadata.repetidora = repetidora;
                            metadata.lugar      = seleccion.get('lugar');
                            metadata.active     = false;
                            metadata.sublist    = repetidora.get('horarios');

                            metadata.sublist.each(function(programa){
                                programa.set('duraciones', new Backbone.Collection());
                            });

                            data.push( metadata );
                        }
                    });
                });

                return data ;
            },
            index:function(planificacion)
            {
                this.duraciones = new Package.Model.Duraciones([
                    { id:"input_duracion_1" , descripcion:'5"' , segundos:'5'  , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_2" , descripcion:'10"', segundos:'10' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_3" , descripcion:'15"', segundos:'15' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_4" , descripcion:'20"', segundos:'20' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_5" , descripcion:'25"', segundos:'25' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_6" , descripcion:'30"', segundos:'30' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_7" , descripcion:'45"', segundos:'45' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_8" , descripcion:'1\'', segundos:'60' , programas:new Backbone.Collection() } ,
                    { id:"input_duracion_9" , descripcion:'3\'', segundos:'180', programas:new Backbone.Collection() } 
                ]);

                // cuando se elige una duración se agrega a la coleccion
                this.selections = new Package.Model.Duraciones();

                this.duracionActual = null;
                // seteo la vista del panel central
                this.planificacion = planificacion ;
                this.repetidoras = {};
                this.repetidoras.collection = new Backbone.Collection( this._initRepetidoras() );
                this.repetidoras.view = new Package.View.RepetidorasList({ collection : this.repetidoras.collection });

                this.layout = new Package.Layout();
                this.app.main.show( this.layout );
                this.layout.duraciones.show( new Package.View.DuracionesCheckbox({ collection: this.duraciones }));
                this.layout.duracionesElegidas.show( new Package.View.DuracionesList({ collection : this.selections }));

                var first = this.duraciones.get("input_duracion_4");
                if( ! first.get('selected') )
                {
                    first.set('selected',true);
                    this.elegirDuracion(first);
                }

                this.mostrarRepetidoras(first);
            },
            setDuracionActiva:function(duracion)
            {
                if( this.duracionActiva != null ){
                    this.duracionActiva.set('active',false);
                }

                this.duracionActiva = duracion;
                duracion.set('active',true);
            },
            elegirDuracion:function(duracion)
            {
                var duraciones = this.selections ;

                duracion.set('checked',false);
                duracion.set('selected',false);
                duracion.set('active',false);

                if( typeof duraciones.get(duracion) == 'undefined' ) // no esta seleccionada
                {
                    if( duraciones.length == 3 ) { // ya hay 3 seleccionadas
                        $('#warning_cantidad_duraciones.modal').modal('show');
                        $('#' + duracion.get('id')).screwDefaultButtons("uncheck");
                        $('#' + duracion.get('id')).toggle();
                        return false;
                    }
                    else {
                        duracion.set('selected',true);
                        duraciones.add(duracion);
                        this.setDuracionActiva(duracion);
                        this.mostrarRepetidoras(duracion);
                    }
                }
                else { // estaba seleccionada
                    duraciones.remove(duracion); // la saco
                    duracion.set('checked',false);
                    duracion.set('selected',false);
                    duracion.set('active',false);

                    var programas = duracion.get('programas');
                    if( programas.length > 0 ) // si tiene programas asignados
                    {
                        programas.each(function(item){ // desasocio todos los programas 
                            if( typeof item.get('programas') != 'undefined' )
                                item.get('programas').get('duraciones').remove(duracion);
                        });

                        programas.remove( programas.models ); // vacio la collection
                    }

                    if( this.duracionActiva == duracion ) // si la duracion activa es la que estoy eliminando
                        this.layout.repetidoras.close(); // cierro el panel
                }
                return true;
            },
            mostrarRepetidoras:function(duracion)
            {
                this.duracionActiva = duracion ;
                var repetidoras = this.repetidoras.collection ;

                this.setDuracionActiva(duracion);

                var id = duracion.get('id');

                repetidoras.each(function(repetidora){
                    _.each(repetidora.get('sublist').where({type:'programa'}),
                        function(programa)
                        {
                            if( typeof programa.get('duraciones').get(id) != 'undefined' )
                                programa.set('active',true);
                            else
                                programa.set('active',false);
                        }
                    );
                });

                this.layout.repetidoras.show( this.repetidoras.view );
            },
            elegirPrograma:function(options)
            {
                var duracion = this.duracionActiva ;
                var repetidora = options.repetidora;
                var programa = options.programa;

                if( typeof programa.get('duraciones').get( this.duracionActiva ) == 'undefined' ) // no esta asignada a esta repetidora
                {
                    programa.get('duraciones').add( duracion );
                    programa.set('active',true);

                    duracion.get('programas').add({id:programa.get('id'), repetidora:repetidora, programa:programa});
                    duracion.set('checked',true);

                } else {
                    programa.get('duraciones').remove( duracion );
                    programa.set('active',false);

                    duracion.get('programas').remove(programa.get('id'));

                    if( duracion.get('programas').length <= 0 )
                        duracion.set('checked',false);
                }
            },
            getSelections : function()
            {
                var completed = false ;

                // verifico que al menos una duracion tengan al menos 1 repetidora asignada
                this.selections.each(function(duracion){
                    if( duracion.get('programas').length > 0 )
                        completed = true;
                });

                if( ! completed )
                    return null;
                return {
                    'duraciones':this.selections,
                    'repetidoras':this.repetidoras.collection
                };
            }
        });

        return Controller ;
    }
);
