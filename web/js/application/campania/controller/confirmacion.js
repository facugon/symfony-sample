define( 'controller/confirmacion',
    ['marionette','underscore','package/paso06','package/registracion'],
    function(Marionette, _, Package, Registracion )
    {
        var Controller = Backbone.Marionette.Controller.extend({
            initialize:function(options)
            {
                this.app = options.app;
            },
            index:function(options)
            {
                var self = this;
                this.totales = options.totales;

                var precio = new Package.Model.Precio(this.totales) ;
                self.layout = new Package.Layout({
                    model : precio
                });

                self.app.main.show( self.layout );
            },
            registrarSolicitud:function(userid,done)
            {
                var solicitud = new Package.Model.Solicitud({ userid:userid });
                solicitud.set('selecciones',[]);

                this.totales.precios.each(function(precio)
                {
                    if( precio.get('duraciones_precios').length > 0 )
                    {
                        var data = {
                            //'horario_id'        : precio.horario,
                            'precio_id'         : precio.get('id'),
                            'repetidora_id'     : precio.get('repetidora'),
                            'duracionesprecios' : []
                        };

                        precio.get('duraciones_precios').each(function(duracionprecio){
                            var programa = duracionprecio.get('programa');
                            var lugar    = programa.get('lugar');

                            data.duracionesprecios.push({
                                'cantidad'        : duracionprecio.get('cantidad'),
                                'segundos'        : duracionprecio.get('duracion').get('segundos'),
                                'programa_nombre' : programa.get('nombre'),
                                'programa_id'     : programa.get('programa').id,
                                'horario_id'      : programa.get('horario').id,
                                'total_calculado' : duracionprecio.get('total'),
                                'valor_duracion'  : duracionprecio.get('valor_duracion'),
                                'lugar'           : lugar.get('id'),
                                'lugar_nombre'    : lugar.get('nombre'),
                                'zona'            : lugar.get('categoria').get('collectionClass')
                            });
                        });

                        solicitud.get('selecciones').push(data);
                    }
                });

                solicitud.save({},{
                    success:function(model, response, options){
                        if(done) done();
                    }
                });
            },
            registrarUsuario:function()
            {
                var self = this;
                if( window.user === null )
                {
                    Registracion.ModalFactory(function(user){
                        self.registrarSolicitud(user.userid);
                    });
                }
                else self.registrarSolicitud(window.user, function(){
                    window.location.href = '/miscampanias'; 
                });
            }
        });

        return Controller ;
    }
);
