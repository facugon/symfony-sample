define('controller/wizard', ['marionette','model/event_dispatcher'],
    function(Marionette, AppEventDispatcher)
    {
        var EventListener = _.extend({}, Backbone.Events);

        var Layout = Backbone.Marionette.Layout.extend({
            template: _.template('<div class="row wizard-nav on"><div id="main-region"></div></div>'),
            regions:{ main:'#main-region' }
        });

        var Wizard = {} ;
        Wizard.Paso1 = Backbone.Marionette.ItemView.extend({
            template : _.template('<span class="uno on"></span>')
        });

        Wizard.Paso2 = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                <a id="paso1" href="#"><span class="uno"><span class="ok"></span></span></a>\
                <span class="dos on"></span>\
            '),
            events : {
                'click a#paso1':'volverPaso1'
            },
            volverPaso1 : function(){
                EventListener.trigger('wizard:volver:paso1');
            }
        });

        Wizard.Paso3 = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                <a id="paso1" href="#"><span class="uno"><span class="ok"></span></span></a>\
                <a id="paso2" href="#"><span class="dos"><span class="ok"></span></span></a>\
                <span class="tres on"></span>\
            '),
            events : {
                'click a#paso1':'volverPaso1',
                'click a#paso2':'volverPaso2'
            },
            volverPaso1 : function(){
                EventListener.trigger('wizard:volver:paso1');
            },
            volverPaso2 : function(){
                EventListener.trigger('wizard:volver:paso2');
            }
        });

        Wizard.Paso4 = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                <a id="paso1" href="#"><span class="uno"><span class="ok"></span></span></a>\
                <a id="paso2" href="#"><span class="dos"><span class="ok"></span></span></a>\
                <a id="paso3" href="#"><span class="tres"><span class="ok"></span></span></a>\
                <span class="cuatro on"></span>\
            '),
            events : {
                'click a#paso1':'volverPaso1',
                'click a#paso2':'volverPaso2',
                'click a#paso3':'volverPaso3'
            },
            volverPaso1 : function(){
                EventListener.trigger('wizard:volver:paso1');
            },
            volverPaso2 : function(){
                EventListener.trigger('wizard:volver:paso2');
            },
            volverPaso3 : function(){
                EventListener.trigger('wizard:volver:paso3');
            }
        });

        Wizard.Paso5 = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                <a id="paso1" href="#"><span class="uno"><span class="ok"></span></span></a>\
                <a id="paso2" href="#"><span class="dos"><span class="ok"></span></span></a>\
                <a id="paso3" href="#"><span class="tres"><span class="ok"></span></span></a>\
                <a id="paso4" href="#"><span class="cuatro"><span class="ok"></span></span></a>\
                <span class="cinco on"></span>\
            '),
            events : {
                'click a#paso1':'volverPaso1',
                'click a#paso2':'volverPaso2',
                'click a#paso3':'volverPaso3',
                'click a#paso4':'volverPaso4'
            },
            volverPaso1 : function(){
                EventListener.trigger('wizard:volver:paso1');
            },
            volverPaso2 : function(){
                EventListener.trigger('wizard:volver:paso2');
            },
            volverPaso3 : function(){
                EventListener.trigger('wizard:volver:paso3');
            },
            volverPaso4 : function(){
                EventListener.trigger('wizard:volver:paso4');
            }
        });

        Wizard.Paso6 = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                <a id="paso1" href="#"><span class="uno"><span class="ok"></span></span></a>\
                <a id="paso2" href="#"><span class="dos"><span class="ok"></span></span></a>\
                <a id="paso3" href="#"><span class="tres"><span class="ok"></span></span></a>\
                <a id="paso4" href="#"><span class="cuatro"><span class="ok"></span></span></a>\
                <a id="paso5" href="#"><span class="cinco"><span class="ok"></span></span></a>\
                <span class="seis on"></span>\
            '),
            events : {
                'click a#paso1':'volverPaso1',
                'click a#paso2':'volverPaso2',
                'click a#paso3':'volverPaso3',
                'click a#paso4':'volverPaso4',
                'click a#paso5':'volverPaso5'
            },
            volverPaso1 : function(){
                EventListener.trigger('wizard:volver:paso1');
            },
            volverPaso2 : function(){
                EventListener.trigger('wizard:volver:paso2');
            },
            volverPaso3 : function(){
                EventListener.trigger('wizard:volver:paso3');
            },
            volverPaso4 : function(){
                EventListener.trigger('wizard:volver:paso4');
            },
            volverPaso5 : function(){
                EventListener.trigger('wizard:volver:paso5');
            }
        });

        var Controller = Backbone.Marionette.Controller.extend({
            initialize : function(options) {
                this.layout = new Layout() ;

                this.app = options.app ;
                this.app.wizard.show( this.layout ) ;

                EventListener.on('wizard:volver:paso1' , this.paso1back , this);
                EventListener.on('wizard:volver:paso2' , this.paso2back , this);
                EventListener.on('wizard:volver:paso3' , this.paso3back , this);
                EventListener.on('wizard:volver:paso4' , this.paso4back , this);
                EventListener.on('wizard:volver:paso5' , this.paso5back , this);
            },
            paso1 : function() {
                this.layout.main.show( new Wizard.Paso1() );
            },
            paso2 : function() {
                this.layout.main.show( new Wizard.Paso2() );
            },
            paso3 : function() {
                this.layout.main.show( new Wizard.Paso3() );
            },
            paso4 : function() {
                this.layout.main.show( new Wizard.Paso4() );
            },
            paso5 : function() {
                this.layout.main.show( new Wizard.Paso5() );
            },
            paso6 : function() {
                this.layout.main.show( new Wizard.Paso6() );
            },
            paso1back : function() {
                AppEventDispatcher.trigger('app:ubicacion:rerender');
                this.layout.main.show( new Wizard.Paso1() );
            },
            paso2back : function() {
                AppEventDispatcher.trigger('app:emisora:rerender');
                this.layout.main.show( new Wizard.Paso2() );
            },
            paso3back : function() {
                AppEventDispatcher.trigger('app:horario:rerender');
                this.layout.main.show( new Wizard.Paso3() );
            },
            paso4back : function() {
                AppEventDispatcher.trigger('app:duracion:rerender');
                this.layout.main.show( new Wizard.Paso4() );
            },
            paso5back : function() {
                AppEventDispatcher.trigger('app:cantidad:rerender');
                this.layout.main.show( new Wizard.Paso5() );
            },
        });

        return Controller ;
    }
);
