define('controller/resumen', ['marionette','model/event_dispatcher'],
    function(Marionette, AppEventDispatcher)
    {
        var EventListener = _.extend({},Backbone.Events);

        var Layout = Backbone.Marionette.Layout.extend({
            template: _.template(' <div class="avatar"><img src="img/avatar-big-ok.png" width="111" height="122"><h3>Tu resumen</h3> <div id="lista-region"></div></div> <a href="#planificacion-ejemplo" data-toggle="modal" class="ver-link">¿Quieres ver cómo será tu plan de medios?</a> '),
            regions:{ lista:'#lista-region' }
        });

        var Resumen = {} ;
        Resumen.ItemView = Backbone.Marionette.ItemView.extend({
            template : _.template('<%= nombre %>: <span><%= valor %></span>'),
            tagName : 'li',
            modelEvents:{ 'change':'render' }
        });

        Resumen.ListaView = Backbone.Marionette.CompositeView.extend({
            template : function(){ return ''; },
            tagName : 'ul',
            itemView : Resumen.ItemView,
            className : 'resumen'
        });

        var Controller = Backbone.Marionette.Controller.extend({
            initialize:function(options){
                this.app = options.app;
            },
            index:function(){
                this.layout = new Layout;
                this.app.resumen.show(this.layout);

                this.items = new Backbone.Collection();

                this.layout.lista.show( new Resumen.ListaView({ collection : this.items }) );
                return this;
            },
            agregarPlan:function(item)
            {
                var match = this.items.where({'nombre':'PLAN'}) ;
                if( match.length != 0 )
                    this.items.remove( match[0] );

                this.items.add( item );
                return this;
            },
            agregarCategoria:function(item)
            {
                var match = this.items.where({'nombre':'DONDE'}) ;
                if( match.length != 0 )
                    this.items.remove( match[0] );

                this.items.add( item );
                return this;
            },
            agregarAnuncios:function(item)
            {
                var match = this.items.where({'nombre':'ANUNCIOS'}) ;
                if( match.length != 0 )
                    this.items.remove( match[0] );

                this.items.add( item );
                return this;
            },
            agregarPrecio:function(item)
            {
                var match = this.items.where({'nombre':'TOTAL'}) ;
                if( match.length != 0 )
                    this.items.remove( match[0] );

                this.items.add( item );
                return this;
            }
        });

        return Controller ;
    }
);

