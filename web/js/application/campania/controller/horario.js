define('controller/horario',
    ['scrollbar', 'marionette','underscore','package/paso03'],
    function( Scrollbar, Marionette, _, Package )
    {
        var Controller = Backbone.Marionette.Controller.extend({
            re_render:function()
            {
                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);
                this.layout.lugares.show(this.lugares_panel);

                var view = this.selecciones[this.lugarActivo].get('repetidoras').meta('view');
                this.layout.repetidoras.show(view);
            },
            initLugares:function()
            {
                var self = this ;
                self.selecciones = [] ;

                _.each( self.lugares, function(data,id){
                    var lugar = data.lugar.clone() ;
                    lugar.set('selected',false);

                    var repetidoras = new Package.Model.Repetidoras(data.repetidoras) ;

                    repetidoras.each(function(repetidora){
                        repetidora.set('horarios', new Backbone.Collection());
                    });
                    repetidoras.meta('view', new Package.View.Repetidoras({ collection : repetidoras }));

                    self.selecciones[ id ] = new Backbone.Model({
                        lugar : lugar,
                        repetidoras : repetidoras
                    });
                })
            },
            initialize:function(options)
            {
                var self = this ;

                this.app = options.app;
                this.selecciones;
                this.lugarActive = null;
            },
            index:function(lugares) // en el index debe caer la primera
            {
                this.lugares = lugares; // lugares|repetidoras seleccionadas del paso anterior
                this.initLugares();

                var lugares_collection = new Backbone.Collection() ;
                _.each(this.selecciones,function(item){
                    lugares_collection.add( item.get('lugar') ); 
                }) ;

                this.lugares_panel = new Package.View.Lugares({ collection : lugares_collection });
                this.lugares_collection = lugares_collection ;

                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);
                this.layout.lugares.show(this.lugares_panel);

                this.cargarRepetidoras( this.lugares_collection.first() );
            },
            setLugarActivo:function(id)
            {
                if( this.lugarActivo != null ) 
                {
                    this.selecciones[ this.lugarActivo ].get('lugar').set('selected',false);
                }

                this.lugarActivo = id ;
                this.selecciones[id].get('lugar').set('selected',true);
            },
            cargarRepetidoras:function(lugarSelected)
            {
                var id = lugarSelected.get('id');

                this.setLugarActivo(id);

                var view = this.selecciones[id].get('repetidoras').meta('view');

                this.layout.repetidoras.show( view );
            },
            cargarProgramas:function(selrep)
            {
                var self = this;

                var seleccion = this.selecciones[ this.lugarActivo ];

                var repetidora = seleccion.get('repetidoras').get( selrep );
                seleccion.set('repetidoraActiva',repetidora);

                var programas = repetidora.get('programas');

                if( typeof programas == 'undefined' ) // yet not accessed
                {
                    var modal = new Package.View.Grilla.Modal({
                        model:new Backbone.Model({nombre:repetidora.get('repetidora')})
                    });
                    repetidora.set('modal',modal);

                    var matriz;
                    matriz = new Package.Model.Grilla.Matriz([]);
                    matriz.meta('repetidora',repetidora);

                    $.blockUI();
                    matriz.fetch({
                        success:function(response,data){
                            $.unblockUI();

                            var view = new Package.View.Grilla.Tabla({ collection : matriz });

                            modal.render(); // render del modal
                            modal.body.show(view); // y render de la tabla

                            repetidora.set('programas',matriz); // guardo el registro de la tabla generada
                            matriz.meta('view',view);
                        }
                    });
                } else {
                    var modal = repetidora.get('modal');

                    if( typeof modal == 'undefined' ) {
                        var modal = new Package.View.Grilla.Modal({
                            model:new Backbone.Model({nombre:repetidora.get('repetidora')})
                        });
                        repetidora.set('modal',modal);
                    }

                    var view = programas.meta('view');

                    modal.render();
                    modal.body.show(view);
                }
            },
            cambiosHorarios:function(celdas)
            {
                var self = this;
                var repetidora ;
                var programas ;

                // el lugar del panel seleccionado
                var seleccion = this.selecciones[ this.lugarActivo ];
                // la repetidora seleccionada
                repetidora = seleccion.get('repetidoraActiva');
                programas = repetidora.get('programas'); // lista/matriz de programas

                _.each(celdas,function(celda,index){
                    // los programas estan ordenados por el id de la celda
                    var horario = programas.get( celda.get('id') ); // obtengo el horario por id de celda

                    if( celda.get('selected') == true ){
                        horario.set('selected',true);
                        repetidora.get('horarios').add( horario );
                    } else {
                        horario.set('selected',undefined); // undefined para que no vuelva a ser tenido en cuenta
                        repetidora.get('horarios').remove( horario );
                    }
                });

                if( self._completoSeleccion(seleccion) )
                    self.lugares_panel.trigger('child:changed',{complete:true, childId:this.lugarActivo});
                else
                    self.lugares_panel.trigger('child:changed',{complete:false, childId:this.lugarActivo});
            },
            _completoSeleccion:function(lugar)
            {
                var completo = false ;
                var repetidoras = lugar.get('repetidoras');

                repetidoras.each(function(repetidora,index){
                    var horarios = repetidora.get('horarios');

                    if( horarios.length > 0 )
                        completo = true;
                });
                return completo;
            },
            getSelections:function()
            {
                var horarios = [] ;
                _.each(this.selecciones,function(lugar) {
                    if( typeof lugar.get('repetidoras') != 'undefined' ) {
                        lugar.get('repetidoras').each(function(repetidora) {
                            if( repetidora.get('horarios').length > 0 )
                                horarios.push( lugar );
                        });
                    }
                });

                if( horarios.length == 0 ) return null;
                return horarios;
            }
        });

        return Controller;
    }
);
