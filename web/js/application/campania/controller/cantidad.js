define( 'controller/cantidad', [ 'scrollbar', 'marionette','underscore','package/paso05'],
    function( Scrollbar, Marionette, _, Package )
    {
        var Controller = Backbone.Marionette.Controller.extend({
            re_render:function()
            {
                var self = this ;
                self.layout = new Package.Layout();
                self.app.main.show( self.layout );
                self.layout.totales.show( self.totalesView );
                self.layout.duracionesElegidas.show( self.duracionesView );
                self.mostrarRepetidoras( self.duracionActiva );
            },
            _completar:function(repetidora)
            {
                var self = this ;
                var programas = repetidora.get('sublist').where({'type':'programa'});
                var lugar = repetidora.get('lugar');

                _.each(programas, function(programa) {

                    programa.set('lugar', lugar);
                    var horario = programa.get('horario');

                    var precio = new Package.Model.Precio({
                        repetidora : repetidora.id,
                        horario    : horario.id,
                    });

                    var duraciones_precios = new Package.Model.DuracionesPrecios() ;
                    programa.get('duraciones').each(function(duracion){
                        duraciones_precios.add(
                            new Package.Model.DuracionPrecio({},{duracion:duracion, programa:programa}) 
                        );
                    });
                    precio.set('duraciones_precios',duraciones_precios);

                    precio.fetch({ // esto es asyncronico
                        success:function(response,options){
                            self.manager.addPrecio( precio );
                            self.fetchHorarios--;
                            self.trigger('fetch:end');
                        }
                    });
                    horario.precio = precio;
                });
            },
            _fetch:function()
            {
                var self = this ;
                self.manager = new Package.Model.PreciosManager();

                $.blockUI();

                var descuentos = new Package.Model.Descuentos();
                descuentos.fetch({
                    success:function(){
                        self.manager.set('descuentos', descuentos);
                        self.planificacion.repetidoras.each( function(repetidora){
                            self._completar( repetidora );
                        });
                    }
                });
            },
            initialize:function(options)
            {
                var self = this ;

                self.fetchHorarios = 0;
                self.app = options.app;
            },
            index:function(planificacion)
            {
                var self = this;
                self.planificacion = planificacion;

                // clono las duraciones elegidas en el paso anterior para poder mantener 
                // el estado actual del paso de seleccion de duraciones y poder volver
                //self.duraciones = new Backbone.Collection() ;
                //self.planificacion.duraciones.each(function(duracion){
                //    var clone = duracion.clone();
                //    //clone.set('checked',false);
                //    clone.set('active',false);
                //    self.duraciones.add(clone);
                //});

                self.planificacion.duraciones.each(function(duracion){
                    duracion.set('active',false);
                });

                self.on('fetch:end',function() {
                    if( self.fetchHorarios == 0 ) {
                        $.unblockUI();

                        var first = self.planificacion.duraciones.first();
                        self.mostrarRepetidoras(first);
                    }
                });

                self.planificacion.repetidoras.each(function(repetidora){
                    repetidora.get('sublist').each(function(horario){
                        self.fetchHorarios++; // cuanto la cantidad de horarios para buscar al servidor
                    });
                });
                self._fetch();

                self.totalesView = new Package.View.Totales({ model:self.manager }) ;
                self.duracionesView = new Package.View.DuracionesList({ collection:self.planificacion.duraciones }) ;

                self.layout = new Package.Layout();
                self.app.main.show( self.layout );
                self.layout.totales.show( self.totalesView );
                self.layout.duracionesElegidas.show( self.duracionesView );
            },
            mostrarRepetidoras:function(duracion)
            {
                var self = this;

                self.planificacion.duraciones.each(function(duracion){
                    duracion.set('active',false);
                });

                duracion.set('active',true);
                this.duracionActiva = duracion ;

                var programas = duracion.get('programas'); // los programas con esta duracion

                //if( typeof programas.view == 'undefined' ){
                //    // creo una nueva vista para esta lista de repetidoras
                programas.view = new Package.View.RepetidorasList({ collection:programas },duracion);
                //}

                self.layout.repetidorasElegidas.show( programas.view ) ; // muestro las repetidoras
            },
            getSelections : function()
            {
                return this.manager.get('precios');
            },
            getTotales : function()
            {
                // lo clono para que no queden referenciados
                // los objetos contenidos(descuento,precio) son copiados por referencia, no clonados
                return _.clone(this.manager.attributes) ;
            }
        });

        return Controller ;
    }
);
