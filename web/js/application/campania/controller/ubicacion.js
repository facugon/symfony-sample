define(
    'controller/ubicacion',[
        'marionette',
        'package/paso01'
    ],function( 
        Marionette,
        Package 
    )
    {
        var UbicacionController = Backbone.Marionette.Controller.extend({
            initialize:function(options)
            {
                this.app = options.app ;

                this.lugares_selected = [] ;
                this.lugares_lista = [] ; ///lista de subcategorias
            },
            re_render: function()
            {
                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);
                this.layout.lugares_categoria.show( this.categorias );
                this.layout.lugares_lista.show( new Package.View.Lugares({ collection : this.lugares_lista }) );
                this.layout.lugares_selected.show( new Package.View.LugaresSelected({ collection : this.lugares_selected }) );
            },
            index:function()
            {
                this.layout = new Package.Layout(); 
                this.app.main.show(this.layout);
                this.categoria = null;

                this.categorias = new Package.View.Categorias({
                    collection : new Package.Model.Categorias([
                        { id : '1' , nombre : 'Nacional'   , url : null                        , collectionClass : null          } ,
                        { id : '2' , nombre : 'CCAA'       , url : BaseURI + '/api/comunidad/' , collectionClass : 'Comunidades' } ,
                        { id : '3' , nombre : 'Provincial' , url : BaseURI + '/api/provincia/' , collectionClass : 'Provincias'  } ,
                        { id : '4' , nombre : 'Local'      , url : BaseURI + '/api/provincia/' , collectionClass : 'SubCategoriasProvincia'  }
                    ])
                });
                this.layout.lugares_categoria.show( this.categorias );
            },
            seleccionarSubCategoria:function(subcategoria)
            {
                var self = this;

                if( typeof subcategoria.get('entities') == 'undefined' ) {
                    $.blockUI();
                    subcategoria.fetch({
                        success:function(responseObject,responseData){
                            $.unblockUI();

                            var list = new Package.Model.Municipios( responseData.entities ) ;
                            self.lugares_lista = list;
                            var view = new Package.View.Lugares({ collection : self.lugares_lista });

                            self.layout.lugares_selected.show(view);

                            subcategoria.set('entities', list);
                        }
                    });
                } else {
                    self.lugares_lista = subcategoria.get('entities') ;
                    var view = new Package.View.Lugares({ collection : self.lugares_lista });
                    self.layout.lugares_selected.show(view);
                }
            },
            seleccionarCategoria:function(categoria)
            {
                var self = this;

                if( self.categoria != null )
                    self.categoria.set('selected',false);
                self.categoria = categoria ;
                categoria.set('selected',true);

                self.layout.lugares_lista.close();

                if( categoria.get('url') != null )
                {
                    if( typeof categoria.get('entities') == 'undefined' )
                    {
                        $.blockUI();
                        categoria.fetch({ // fetch the server
                            success:function(responseObject,responseData){
                                $.unblockUI();

                                self.lugares_lista = new Package.Model[ categoria.get('collectionClass') ]( responseData.entities ) ;

                                if( categoria.get('nombre') == 'Local' ) {
                                    var view = new Package.View.Lugares({ collection : self.lugares_lista });
                                    view.itemView = Package.View.SubCategoria ;
                                } else {
                                    var view = new Package.View.Lugares({ collection : self.lugares_lista });
                                }

                                self.layout.lugares_lista.show(view);
                            }
                        });
                    } else { // already fetched
                        self.lugares_lista = new Package.Model[ categoria.get('collectionClass') ]( categoria.get('entities') ) ;

                                if( categoria.get('nombre') == 'Local' ) {
                                    var view = new Package.View.Lugares({ collection : self.lugares_lista });
                                    view.itemView = Package.View.SubCategoria ;
                                } else {
                                    var view = new Package.View.Lugares({ collection : self.lugares_lista });
                                }

                        self.layout.lugares_lista.show(view);
                    }

                    this.lugares_selected = new Package.Model[ categoria.get('collectionClass') ]() ; // an empty collection

                    var view = new Package.View.LugaresSelected({
                        collection : this.lugares_selected
                    });

                    self.layout.lugares_selected.show(view);
                }
                else if( categoria.get('id') == 1 ) // Nacional
                {
                	//vacio la lista de lugares de la subcategoria
                	this.lugares_lista = new Backbone.Collection(); //empty collection
                    this.lugares_selected = new Backbone.Collection();
                    this.lugares_selected.add({
                        id        : 1          , 
                        nombre    : "Nacional" , 
                        selected  : true       , 
                        categoria : categoria
                    });

                    var view = new Package.View.LugaresSelected({
                        collection : this.lugares_selected
                    });

                    self.layout.lugares_selected.show(view);
                }
            },
            seleccionarLugar:function(lugar)
            {
                var collection = this.lugares_selected ;
                lugar.set('selected',true);
                lugar.set('categoria',this.categoria);
                collection.add(lugar);
            },
            deseleccionarLugar:function(lugar)
            {
                var collection = this.lugares_selected ;
                lugar.set('selected',false);
                collection.remove(lugar);
            },
            getSelections:function()
            {
                var collection = this.lugares_selected ;
                if( collection.length  != 0 )
                {
                    if( collection.length > 0 )
                    {
                        var ubicacion = {};
                        ubicacion.lugares   = collection ;
                        ubicacion.categoria = this.categoria ;

                        return ubicacion ;
                    }
                }

                return null;
            }
        });

        return UbicacionController ;
    }
);
