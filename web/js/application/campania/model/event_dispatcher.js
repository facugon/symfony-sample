define(['backbone','underscore'],function(Backbone, _){
    var dispatcher = _.extend({}, Backbone.Events);
    return dispatcher;
});
