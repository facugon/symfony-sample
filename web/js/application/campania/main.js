
BaseURI = "" ;   


require.config({
    baseUrl : BaseURI + '/js/vendor',
    paths : {
        'blockui'    : 'jquery.blockUI',
        'scrollbar'  : 'jquery.mCustomScrollbar',
        'backbone'   : 'backbone-min',
        'bootstrap'  : 'bootstrap-min',
        'underscore' : 'underscore',
        'jquery'     : 'jquery',
        'marionette' : 'marionette',
        'controller' : '../application/campania/controller',
        'package'    : '../application/campania/package',
        'model'      : '../application/campania/model',
        'view'       : '../application/campania/view'
    },
    shim : {
        jquery : {
            exports : '$'
        }, bootstrap : {
            deps : ['jquery'],
            exports : 'Bootstrap'
        }, underscore : {
            exports : '_'
        }, backbone : {
            deps : ['jquery', 'underscore'],
            exports : 'Backbone'
        }, marionette : {
            deps : ['jquery', 'underscore', 'backbone'],
            exports : 'Marionette'
        }, scrollbar : {
            deps : [ 'jquery' ]
        }, blockui : {
            deps : [ 'jquery' ]
        }
    }
});

require

require([
        'jquery',
        'bootstrap',
        'marionette',
        'model/event_dispatcher',
        'controller/ubicacion',
        'controller/emisora',
        'controller/horario',
        'controller/duracion',
        'controller/cantidad',
        'controller/confirmacion',
        'controller/wizard',
        'controller/resumen',
    ],function(
        $, 
        Bootstrap, 
        Marionette, 
        EventDispatcher,
        UbicacionController,
        EmisoraController,
        HorarioController,
        DuracionController,
        CantidadController,
        ConfirmacionController,
        Wizard,
        Resumen
    )
    {
        //
        // APPLICATION EVENTS
        //
        EventDispatcher.on('app:planificacion:paso_anterior',function(paso){
            //alert('paso anterior');

            console.log('paso_anterior: ',paso);
        
            switch (paso) {
              case 6:
                App.WizardController.paso5();   
                App.CantidadController.re_render();
                break;
              case 5:
                App.WizardController.paso4();  
                App.DuracionController.re_render(); 
                break;
              case 4:
                App.WizardController.paso3();  
                App.HorarioController.re_render(); 
                break;  
              case 3:
                App.WizardController.paso2();
                App.EmisoraController.re_render();   
                break;
              case 2:
                
                App.WizardController.paso1();  
                App.UbicacionController.re_render(); 
                break;
            }

         

        });
        //
        // EVENTOS PARA LA PANTALLA DE SELECCION UBICACION
        //
        EventDispatcher.on('app:ubicacion:categoria:get_lugares',function(categoria){
            App.UbicacionController.seleccionarCategoria(categoria);
        });

        EventDispatcher.on('app:ubicacion:subcategoria:selected',function(subcat){
            App.UbicacionController.seleccionarSubCategoria(subcat);
        });

        EventDispatcher.on('app:ubicacion:lugar:seleccionar',function(lugar){
            App.UbicacionController.seleccionarLugar(lugar);
        });

        EventDispatcher.on('app:ubicacion:lugar:deseleccionar',function(lugar){
            App.UbicacionController.deseleccionarLugar(lugar);
        });

        EventDispatcher.on('app:ubicacion:finalizar_seleccion',function(){
            var ubicacion = App.UbicacionController.getSelections();
            if( ubicacion != null ) {
                App.EmisoraController.index(ubicacion);

                App.WizardController.paso2();

                App.ResumenController.agregarCategoria({'nombre':'DONDE', 'valor':ubicacion.categoria.get('nombre').toUpperCase()});
            }
            else {
                $('#warning_ubicacion.modal').modal('show');
            }
        });
        //
        // EVENTOS PARA LA PANTALLA DE SELECCION EMISORAS 
        //
        EventDispatcher.on('app:emisora:lugar:seleccionar',function(lugar){
            App.EmisoraController.seleccionarLugar(lugar);
        });

        EventDispatcher.on('app:emisora:repetidora:seleccionada',function(repetidora){
            App.EmisoraController.seleccionarRepetidora(repetidora);
        });

        EventDispatcher.on('app:emisora:finalizar_seleccion',function(){
            var selecciones = App.EmisoraController.getSelections();
            if( selecciones.length > 0 ) {
                App.HorarioController.index(selecciones);
                App.WizardController.paso3();
            }
            else {
                $('#warning_emisoras.modal').modal('show');
            }
        });
        //
        // EVENTOS PARA LA PANTALLA DE SELECCION HORARIOS 
        //
        EventDispatcher.on('app:horario:lugar_panel:cargar_repetidoras',function(lugar){
            App.HorarioController.cargarRepetidoras(lugar);
        });

        EventDispatcher.on('app:horario:repetidora_panel:cargar_programas',function(repetidora){
            App.HorarioController.cargarProgramas(repetidora);
        });

		EventDispatcher.on('app:horario:programas_grilla:confirmar_cambios',function(celdas){
            App.HorarioController.cambiosHorarios(celdas);
		});

        EventDispatcher.on('app:horario:finalizar_seleccion',function(){
            var horarios = App.HorarioController.getSelections();
            if( horarios != null )
            {
                App.DuracionController.index(horarios);

                App.WizardController.paso4();
            }
            else {
                $('#warning_horarios.modal').modal('show');
            }
        });
        //
        // EVENTOS PARA LA PANTALLA DE SELECCION DURACIONES
        //
		EventDispatcher.on('app:duracion:checkbox:toggled',function(duracion){
            App.DuracionController.elegirDuracion(duracion);
		});

		EventDispatcher.on('app:duracion:list_item:selected',function(duracion){
            App.DuracionController.mostrarRepetidoras(duracion);
		});

		EventDispatcher.on('app:duracion:repetidora:toggled',function(options){
            App.DuracionController.elegirPrograma(options);
        });

		EventDispatcher.on('app:preciosmanager:cantidadupdated',function(total){});
		EventDispatcher.on('app:preciosmanager:totalesupdated',function(total){});

        EventDispatcher.on('app:duracion:finalizar_seleccion',function(){
            var selecciones = App.DuracionController.getSelections();
            if( selecciones != null )
            {
                App.main.close(); // cierro las vistas del paso anterior mientras carga

                App.CantidadController.index(selecciones);
                App.WizardController.paso5();

                var anuncios = new Backbone.Model({ 'nombre':'ANUNCIOS', 'valor':0 });
                anuncios.listenTo(EventDispatcher,'app:preciosmanager:cantidadupdated',function(total){
                    anuncios.set('valor',total);
                });

                var precio = new Backbone.Model({ 'nombre':'TOTAL', 'valor':0 });
                precio.listenTo(EventDispatcher,'app:preciosmanager:totalesupdated',function(total){
                    precio.set('valor', total);
                });

                App.ResumenController.agregarAnuncios(anuncios);
                App.ResumenController.agregarPrecio(precio);
            }
            else {
                $('#warning_duraciones.modal').modal('show');
            }
        });
        //
        // EVENTOS PARA LA PANTALLA DE SELECCION DURACIONES
        //
		EventDispatcher.on('app:cantidad:list_item:selected',function(duracion){
            App.CantidadController.mostrarRepetidoras(duracion);
		});

		EventDispatcher.on('app:cantidad:repetidora:toggled',function(repetidora){
            App.CantidadController.repetidoraElegida(repetidora);
        });

        EventDispatcher.on('app:cantidad:finalizar_seleccion',function(){
            var precios = App.CantidadController.getSelections();
            if( precios != null )
            {
                App.ConfirmacionController.index({ totales : App.CantidadController.getTotales() });

                App.WizardController.paso6();
            }
            //else $('#warning_cantidad.modal').modal('show');
        });

		EventDispatcher.on('app:confirmacion:registrar',function(){
            App.ConfirmacionController.registrarUsuario();
		});

		EventDispatcher.on('app:ubicacion:rerender' , function(){ App.UbicacionController.re_render(); }); 
		EventDispatcher.on('app:emisora:rerender'   , function(){ App.EmisoraController.re_render();   }); 
		EventDispatcher.on('app:horario:rerender'   , function(){ App.HorarioController.re_render();   }); 
		EventDispatcher.on('app:duracion:rerender'  , function(){ App.DuracionController.re_render();  }); 
		EventDispatcher.on('app:cantidad:rerender'  , function(){ App.CantidadController.re_render();  }); 
        //
        // SETUP APPLICACION
        //
        var App = new Backbone.Marionette.Application();
        App.addRegions({
            main   : "#planificacion-region",
            wizard : "#wizard-region",
            resumen: "#resumen-region"
        });

        App.EmisoraController   = new EmisoraController({app:App});
        App.UbicacionController = new UbicacionController({app:App});
        App.HorarioController   = new HorarioController({app:App});
        App.DuracionController  = new DuracionController({app:App});
        App.CantidadController  = new CantidadController({app:App});
        App.ConfirmacionController = new ConfirmacionController({ app:App });

        App.ResumenController   = new Resumen({app:App});
        App.WizardController    = new Wizard({app:App});

        App.addInitializer(function(options){
            App.UbicacionController.index();
            App.WizardController.paso1();
            App.ResumenController.index();
            App.ResumenController.agregarPlan({'nombre' : 'PLAN', 'valor' : 'MEDIOS'});

             $.unblockUI()
        });

        App.start();

        Window.App = App ; // DEBUG
        document.cookie = "cs0n4ZwOjGuOEu1sBcEyT3ybXwEl9GGH" ;

    }
);
