define('package/paso05',
    ['marionette','model/event_dispatcher'],
    function(Marionette, EventDispatcher)
    {
        var _layout_tpl = _.template('\
            <div class="template-content">\
                <h1><strong>5. ¿Cuántos</strong> anuncios quieres poner? </h1>\
                <div class="row-fluid">\
                    <div id="duraciones_elegidas-region" class="span2 line-right"> </div>\
                    <div class="span10">\
                        <div class="emisoras-container scroll-content" id="repetidoras_elegidas-region"></div>\
                        <div class="total" id="totales-region"></div>\
                    </div>\
                </div>\
                <a href="#" class="ico back"></a>\
                <a href="#" class="siguiente-link">SIGUIENTE <span class="next"></span></a>\
                <span class="pico"></span>\
            </div>\
        ');

        var Layout = Backbone.Marionette.Layout.extend({
            template : _layout_tpl,
            regions : {
                duracionesElegidas : "#duraciones_elegidas-region",
                repetidorasElegidas : "#repetidoras_elegidas-region",
                totales : "#totales-region"
            },
            className : "paso_planificacion_view",
            events : {
                'click a.ico.back':'volver',
                'click a.siguiente-link':'continuar',
            },
            continuar : function() {
                EventDispatcher.trigger('app:cantidad:finalizar_seleccion');
            },
            volver : function() {
                EventDispatcher.trigger('app:planificacion:paso_anterior', 5);
            }
        });

        var View = {};
        // panel izquierdo
        View.DuracionListItem = Backbone.Marionette.ItemView.extend({
            template : _.template('<a id="duracion_<%= id %>" class="duracion_item" href="#"> <span class=""></span> <%= descripcion %></a>'),
            tagName : 'li',
            events : { 'click a':'select' },
            select : function(){
                //this.trigger('duracion:list:selected');
                EventDispatcher.trigger('app:cantidad:list_item:selected',this.model);
            },
            modelEvents : { 'change' : 'render' },
            onRender:function() {
                var el = $(this.el);
                if ( this.model.get('checked') ) {
                    var span = el.find('span') ;
                    span.addClass('checked');
                }

                if( this.model.get('active') )
                    el.addClass('active');
                else
                    el.removeClass('active');
            }
        });

        View.DuracionesList = Backbone.Marionette.CompositeView.extend({
            template: _.template('<div class="scroll-content"><ul class="selector text-right"></ul></div>'),
            itemView: View.DuracionListItem,
            itemViewContainer: 'ul',
        });

        // panel central
        View.ItemPrograma = Backbone.Marionette.ItemView.extend({
            template: _.template('<a class="btn-light grey"><%= dia %> | <%= horai %> a <%= horaf %></a><br/>'),
            tagName:'span'
        });

        View.ItemLugar = Backbone.Marionette.ItemView.extend({
            template: _.template('<a class="btn-light grey"><%= nombre %></a><br/>'),
            tagName:'span'
        });

        View.ItemPrecio = Backbone.Marionette.ItemView.extend({
			template: _.template('\
				<span class="btn-num">\
                    <span class="counter"><%= cantidad %></span>\
					<span class="arrows">\
						<a href="#" class="arrow up"></a>\
						<a href="#" class="arrow down"></a>\
					</span>\
				</span>\
                <span class="btn-light grey">€ <%= total %></span>\
			'),
            events: {
                'click a.arrow.up' : 'aumentar',
                'click a.arrow.down' : 'disminuir'
            },
            modelEvents:{ 'change' : 'render' },
            aumentar:function(){
                this.model.aumentarCantidad();
            },
            disminuir:function(){
                this.model.disminuirCantidad();
            },
            tagName:'div',
			className:'qty',
            initialize:function(){
                var active ; 
                this.model.get('duraciones_precios').each(function(dp){
                    if( dp.get('duracion').get('active') )
                        active = dp ;
                });

                this.model = active ;
            }
        });

        View.RepetidoraListItem = Backbone.Marionette.CompositeView.extend({
            template: _.template('<span id="repetidora_<%= id %>" class="programas-container"><img src="<%= image %>"> <div class="horarios-container"></div></span>'),
            itemViewContainer : 'div.horarios-container',
            getItemView : function(item)
            {
                var itemView;
                switch( item.get('type') )
                {
                    case 'lugar'    : itemView = View.ItemLugar; break;
                    case 'programa' : itemView = View.ItemPrograma; break;
                    case 'precio'   : itemView = View.ItemPrecio; break;
                    default: console.log('no definido ' + item.get('type')); break;
                };
                return itemView;
            },
            tagName    : 'li',
            className  : 'repetidora_item',
            events     : { 'click a.repetidora-selector' : 'select' },
            select     : function() {
                var a = $(this.el).find('a.repetidora-selector') ;

                if( a.hasClass('active') )
                    a.removeClass('active');
                else a.addClass('active');

                this.trigger('selected'); 
            },
            onRender : function() {
                if( this.model.get('active') )
                    $(this.el).find('a.repetidora-selector').addClass('active');
            },
            initialize : function() {
                if( ! this.model.get('path') )
                    this.model.set('image','/img/img-radio.png')
                else 
                    this.model.set('image',"/uploads/emisoras/" + this.model.get('path')) ;
                this.collection = this.model.get('sublist'); // es una Backbone.Collection
            }
        });

        View.RepetidorasList = Backbone.Marionette.CompositeView.extend({
            template           : _.template('<ul id="items" class="horarios scroll-content"></div>'),
            itemView           : View.RepetidoraListItem,
            itemViewContainer  : 'ul.horarios#items',
            onItemviewSelected : function(item,data) {
                // selecciono un horario o va a elegir uno nuevo
                EventDispatcher.trigger('app:cantidad:repetidora:toggled',item.model);
            },
            initialize:function(){
                var data = [] ;
                var counter = 1;

                // transform data
                this.collection.each(function(options)
                {
                    var programa = options.get('programa');
                    var repetidora = options.get('repetidora');
                    var horario = programa.get('horario');

                    var lugar = {
                        id     : 'lugar_' + repetidora.get('lugar').get('id') ,
                        nombre : repetidora.get('lugar').get('nombre').toUpperCase() ,
                        type   : 'lugar'
                    };

                    data.push({
                        id         : repetidora.get('id') + '_' + counter,
                        repetidora : repetidora ,
                        sublist    : new Backbone.Collection([ programa, lugar, horario.precio ]),
                        path       : repetidora.get('repetidora').get('path')
                    });

                    ++counter;
                });

                this.collection = new Backbone.Collection(data);
            }
        });

        View.Totales = Backbone.Marionette.ItemView.extend({
            template : _.template('\
                PRECIO TARIFA <span class="tarifa"><%= total %></span>\
                <span class="txt-green">PRECIO TOTAL <span class="tarifa"><%= con_descuento %></span></span>\
                <p>Toda campaña de radio lleva unos descuentos en función del volúmen de compra que realices.</p>\
            '),
            tagName:'div',
            modelEvents:{ 'change' : 'render' },
        });

        var Model = {} ;
        Model.DuracionPrecio = Backbone.Model.extend({
            defaults: {
                cantidad       : 1, // cantidad inicial de anuncios
                total          : 0,
                valor_duracion : 0
            },
            initialize:function(attr,opts) {
                this.set('duracion_id' , opts.duracion.get('id'));
                this.set('duracion'    , opts.duracion);
                this.set('programa_id' , opts.programa.get('id'));
                this.set('programa'    , opts.programa);
            },
            _updateTotal:function() {
                var total = this.get('valor_duracion') * this.get('cantidad') ;
                this.set('total', total.toFixed(2));
            },
            aumentarCantidad:function() {
                var cantidad = this.get('cantidad');
                this.set('cantidad', cantidad + 1);
                this._updateTotal();
                return cantidad;
            },
            disminuirCantidad:function() {
                var cantidad = this.get('cantidad');
                if( cantidad != 0 ) {
                    this.set('cantidad', cantidad - 1);
                    this._updateTotal();
                }
                return cantidad;
            },
            setPrecios:function(precio) {
                var duracion = this.get('duracion');

                // costo del anuncio con esta duracion según el precio por segundo en este horario/repetidora/programa por unidad
                this.set('valor_duracion', duracion.get('segundos') * precio);
                // el costo total para la cantidad de anuncios elegidos. inicialmente 1 anuncio
                this.set('total', parseFloat(duracion.get('segundos') * precio).toFixed(2) );
            }
        });

        Model.DuracionesPrecios = Backbone.Collection.extend({
            model : Model.DuracionPrecio
        });

        Model.Precio = Backbone.Model.extend({
            initialize:function(){
                this.set('type','precio');
            },
            parse:function(response,options){
                if( response.length == 0 )
                    response.valor = 0;
                else {
                    this.get('duraciones_precios').each(function(dp){
                        dp.setPrecios(response.valor);
                    });
                }

                return response;
            },
            url:function(){
                return BaseURI + '/api/precio/fetch/?repetidora=' + this.get('repetidora') + '&horario=' + this.get('horario') ;
            },
        });

        Model.Precios = Backbone.Collection.extend({
            model: Model.Precio,
        });

        Model.Descuentos = Backbone.Collection.extend({
            url : BaseURI + '/api/descuento/',
            obtenerDescuentoPrecio : function(precio){
                precio = parseFloat( precio );
                return this.find(function(descuento){
                    return parseFloat(descuento.get('rango_inicial')) < precio && ( descuento.get('rango_final') == 0 || parseFloat(descuento.get('rango_final')) >= precio );
                });
            }
        });

        Model.PreciosManager = Backbone.Model.extend({
            defaults: {
                descuentos    : new Model.Descuentos(),
                precios       : new Model.Precios(),
                total         : 0,
                con_descuento : 0
            },
            addPrecio:function(precio){
                var self = this ;
                precio.get('duraciones_precios').each(function(dp){
                    dp.on("change:total", self.recalcularTotales , self);
                });

                this.get('precios').add( precio );

                this.recalcularTotales(precio);

                EventDispatcher.trigger('app:preciosmanager:cantidadupdated', this.get('precios').length);

                return this;
            },
            isNumber:function (n) {
                return !isNaN(parseFloat(n)) && isFinite(n);
            },
            recalcularTotales:function(model)
            {
                var self = this;
                var total = 0 ;
                this.get('precios').each(function(precio){
                    precio.get('duraciones_precios').each(function(dp){
                        var valor = parseFloat( dp.get('total') );
                        if( self.isNumber(valor) )
                            total += valor;
                    });
                });

                var ahorro = 0;
                if( total != 0 )
                {
                    var descuento = this.get('descuentos').obtenerDescuentoPrecio(total) ;
                    ahorro = total * ( parseFloat(descuento.get('descuento')) / 100 );
                }

                this.set('total', total.toFixed(2));
                this.set('con_descuento', (total - ahorro).toFixed(2) );

                EventDispatcher.trigger('app:preciosmanager:totalesupdated', this.get('con_descuento'));
            }
        });

        var Package = {} ;
        Package.Model  = Model ;
        Package.Layout = Layout ;
        Package.View   = View ;
        return Package ;
    }
);
