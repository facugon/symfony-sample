define('package/paso01',
    ['scrollbar','marionette','model/event_dispatcher'],
    function(Scrollbar, Marionette,EventDispatcher){
        // VIEWS
        var View = {};
        View.Categoria = Backbone.Marionette.ItemView.extend({
            template:_.template('<a class="categoria_item" id="categoria_<%= id %>" href="#"><%= nombre %></a>'), 
            tagName:'li', 
            className:'categoria_item',
            events:{
                'click a.categoria_item':'selectCategoria'
            },
            selectCategoria:function(){
                this.trigger('categoria:selected'); // informa a la collection del evento
            },
            onRender:function(){
                if( this.model.get('selected') )
                    $(this.el).addClass('active');
            }
        });

        View.Categorias = Backbone.Marionette.CompositeView.extend({
            template:_.template('<div class="scroll-content"> <ul id="items" class="selector"> </ul> </div>'),
            itemView:View.Categoria,
            itemViewContainer:'ul#items',
            onItemviewCategoriaSelected:function(item, data){
                this.children.each(function(view){
                    if( $(view.el).hasClass('active') )
                        $(view.el).removeClass('active');
                });
                $(item.el).addClass('active');
                EventDispatcher.trigger('app:ubicacion:categoria:get_lugares',item.model);
            },
            onRender:function(){
                //$(this.el).find('.scroll-content').mCustomScrollbar();
            }
        });

        View.SubCategoria = Backbone.Marionette.ItemView.extend({ 
            template:_.template('<a class="subcategoria_item" id="subcategoria_<%= id %>" href="#"><%= nombre %></a>'), 
            tagName:'li', 
            className:'subcategoria_item' ,
            events:{
                'click a.subcategoria_item':'selectSubcategoria' 
            },
            selectSubcategoria:function() {
                EventDispatcher.trigger('app:ubicacion:subcategoria:selected',this.model);
                this.trigger('subcategoria:selected');
            },
            onRender:function() {
                var li = $(this.el);
                if( this.model.get('selected') == true ) {
                    li.addClass('active');
                } else {
                    li.removeClass('active');
                }
            },
            modelEvents:{'change':'render'}
        });

        View.Lugar = Backbone.Marionette.ItemView.extend({ 
            template:_.template('<a class="lugar_item" id="lugar_<%= id %>" href="#"><%= nombre %></a>'), 
            tagName:'li', 
            className:'lugar_item' ,
            events:{ 'click a.lugar_item':'selectLugar' },
            selectLugar:function() {
                if( this.model.get('selected') == true )
                    this.trigger('deselected'); // informa a la collection del evento
                else
                    this.trigger('selected'); // informa a la collection del evento
            },
            onRender:function(){
                var li = $(this.el);
                if( this.model.get('selected') == true ) {
                    li.addClass('active');
                } else {
                    li.removeClass('active');
                }
            },
            modelEvents:{ 'change':'render' }
        });

        View.Lugares = Backbone.Marionette.CompositeView.extend({
            template           : _.template('<div class="scroll-content"> <ul id="items" class="selector dos"> </ul> </div>'),
            itemView           : View.Lugar,
            itemViewContainer  : 'ul#items',
            onItemviewSelected : function(item,data){
                EventDispatcher.trigger('app:ubicacion:lugar:seleccionar',item.model);
            },
            onItemviewDeselected : function(item,data){
                EventDispatcher.trigger('app:ubicacion:lugar:deseleccionar',item.model);
            },
            onRender:function(){
                //$(this.el).find('.scroll-content').mCustomScrollbar();
            }
        });

        View.LugarSelected = Backbone.Marionette.ItemView.extend({ 
            template:_.template('<a class="lugar_item" id="lugar_<%= id %>" href="#"><%= nombre %></a>'), 
            tagName:'li', 
            className:'lugar_item' ,
            events:{
                'click a.lugar_item':'selectLugar',
            },
			selectLugar:function() {
				if( this.model.get('nombre') != 'Nacional' )
				{
					// dejo que la collection manipule el evento
					this.trigger('selected'); // informa a la collection del evento
				}
			}
        });

        View.LugaresSelected = Backbone.Marionette.CompositeView.extend({
            template:_.template('<div class="scroll-content"> <h3>Tu selección</h3> <ul id="items" class="selector dos"> </ul> </div>'),
            itemView: View.LugarSelected,
            itemViewContainer:'ul#items',
            onItemviewSelected:function(item,data) {
                var li = $(item.el) ;
                EventDispatcher.trigger('app:ubicacion:lugar:deseleccionar',item.model);

                var a = li.children('a');

                $('div.paso_planificacion_view div#lugares_lista-region li.active a#' + a.attr('id')).parent('li').removeClass('active');
            },
            onRender:function() {
                //$(this.el).find('.scroll-content').mCustomScrollbar('update');
            }
        });

        var _layout_tpl = _.template('\
            <div class="template-content">\
                <h1><strong>1. ¿Dónde</strong> quieres que salga tu campaña?</h1>\
                <div class="row-fluid">\
                    <div id="lugares_categoria-region" class="span3 line-right">\
                    </div>\
                    <div id="lugares_lista-region" class="span4 line-right">\
                    </div>\
                    <div id="lugares_selected-region" class="span5">\
                    </div>\
                </div>\
                <a href="#" id="paso_2" class="siguiente-link">SIGUIENTE <span class="next"></span></a>\
                <span class="pico"></span>\
            </div>\
        ');

        var Layout = Backbone.Marionette.Layout.extend({
            template : _layout_tpl,
            regions : {
                lugares_categoria : "#lugares_categoria-region",
                lugares_lista     : "#lugares_lista-region",
                lugares_selected  : "#lugares_selected-region"
            },
            className : "paso_planificacion_view", 
            events : {
                'click a.siguiente-link#paso_2':'continuar'
            },
            continuar : function() {
                EventDispatcher.trigger('app:ubicacion:finalizar_seleccion');
            }
        });

        //
        // MODELS AND COLLECTIONS
        //
        var Model = {} ;

        Model.SubCategoriaProvincia = Backbone.Model.extend({
            url:function() {
                return BaseURI + '/api/municipio?provincia=' + this.get('id') ;
            } 
        });
        Model.Categoria   = Backbone.Model.extend({
            url:function() {
                return this.attributes.url ; 
            } 
        });
        Model.SubCategoriasProvincia = Backbone.Collection.extend({ model:Model.SubCategoriaProvincia });
        Model.Categorias  = Backbone.Collection.extend({ model:Model.Categoria });
        Model.Provincia   = Backbone.Model.extend({});
        Model.Provincias  = Backbone.Collection.extend({ model:Model.Provincia });
        Model.Comunidad   = Backbone.Model.extend({});
        Model.Comunidades = Backbone.Collection.extend({ model:Model.Comunidad });
        Model.Municipio   = Backbone.Model.extend({});
        Model.Municipios  = Backbone.Collection.extend({ model:Model.Municipio });

        Package = {} ;
        Package.Model = Model ;
        Package.View = View ;
        Package.Layout = Layout ;

        return Package ;
    }
);
