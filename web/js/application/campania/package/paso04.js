define('package/paso04',
    ['marionette','model/event_dispatcher','jquery.screwdefaultbuttonsV2'],
    function(Marionette, EventDispatcher, ScrewDefaultButtons){

        var _layout_tpl = _.template('\
            <div class="template-content">\
                <h1><strong>4. ¿Cuánto</strong> duran tus anuncios?</h1>\
                <div id="duraciones-region"> </div>\
                <div class="row-fluid">\
                    <div id="duraciones_elegidas-region" class="span2 line-right"> </div>\
                    <div id="repetidoras-region" class="span10"> </div>\
                </div>\
                <a href="#" class="ico back"></a>\
                <a href="#" class="siguiente-link">SIGUIENTE <span class="next"></span></a>\
                <span class="pico"></span>\
            </div>\
        ');

        var Layout = Backbone.Marionette.Layout.extend({
            template : _layout_tpl,
            regions : {
                duraciones         : "#duraciones-region",
                duracionesElegidas : "#duraciones_elegidas-region",
                repetidoras        : "#repetidoras-region"
            },
            className : "paso_planificacion_view",
            events : { 
                'click a.ico.back':'volver',
                'click a.siguiente-link':'continuar' 
            },
            continuar : function() {
                EventDispatcher.trigger('app:duracion:finalizar_seleccion');
            },
            volver : function() {
                EventDispatcher.trigger('app:planificacion:paso_anterior', 4);
            }
        });

        var View = {};
        // panel superior
        View.DuracionCheckbox = Backbone.Marionette.ItemView.extend({
            template : _.template('<input type="checkbox" id="input_duracion_<%= id %>"><span id="duracion_<%= id %>"> <%= descripcion %> </span>'),
            events: {'click div':'select'},
            tagName:'span',
            select:function(){
                $(this.el).find('input').screwDefaultButtons("uncheck") ;
                this.trigger('duracion:checkbox:toggled');  
            },
            onRender:function(){
                try {
                    $(this.el).find('input:radio').screwDefaultButtons({ image: 'url("js/images/radioSmall.jpg")', width: 25, height: 25 });

                    $(this.el).find('input:checkbox').screwDefaultButtons({ image: 'url("js/images/checkboxSmall.jpg")', width: 35, height: 30 });

                    if( this.model.get('selected') ){ // cuando se selecciona el checkbox de la duracion
                        $(this.el).find('input').screwDefaultButtons("check") ;
                    }
                    else{
                        $(this.el).find('input').screwDefaultButtons("uncheck") ;
                    }

                } catch(e) {
                    if( console && console.log )
                        console.log('Error en los botones');
                }
            },
            modelEvents:{ 'change':'render' }
        });

        View.DuracionesCheckbox = Backbone.Marionette.CollectionView.extend({
            itemView: View.DuracionCheckbox,
            tagName:'div',
            className:'checkbox-container',
            onItemviewDuracionCheckboxToggled:function(item,data){
                EventDispatcher.trigger('app:duracion:checkbox:toggled',item.model);
            },
        });

        // panel izquierdo
        View.DuracionListItem = Backbone.Marionette.ItemView.extend({
            template : _.template('<a id="duracion_<%= id %>" class="duracion_item" href="#"> <span class=""></span> <%= descripcion %></a>'),
            tagName : 'li',
            events : { 'click a':'select' },
            select : function(){
                this.trigger('duracion:list:selected');
            },
            modelEvents : { 'change' : 'render' },
            onRender:function()
            {
                var el = $(this.el);
                if ( this.model.get('checked') ) { // cuando se selecciona al menos un horario para esta duracion
                    var span = el.find('span') ;
                    span.addClass('checked');
                }

                if( this.model.get('active') ) // cuando se encuentra activo en el panel lateral
                   el.addClass('active'); 
            },
        });

        View.DuracionesList = Backbone.Marionette.CompositeView.extend({
            template: _.template('<div class="scroll-content"><ul class="selector text-right"></ul></div>'),
            itemView: View.DuracionListItem,
            itemViewContainer: 'ul',
            onItemviewRender:function(item){
                this.children.each(function(view){ // barra lateral verde
                    if( $(view.el).hasClass('active') )
                        $(view.el).removeClass('active');
                });
            },
            onItemviewDuracionListSelected:function(item,data){
                EventDispatcher.trigger('app:duracion:list_item:selected',item.model);
            },
        });

        // panel central
        View.ItemPrograma = Backbone.Marionette.ItemView.extend({
            template: _.template('<a class="btn-light grey"><%= dia %> | <%= horai %> a <%= horaf %></a><br/>'),
            tagName:'span',
            modelEvents:{ 'change':'cascade' },
            cascade:function(){
                this.trigger('model:changed');
            }
        });

        View.ItemLugar = Backbone.Marionette.ItemView.extend({
            template: _.template('<a class="btn-light grey"><%= nombre %></a><br/>'),
            tagName:'span'
        });

        View.RepetidoraListItem = Backbone.Marionette.CompositeView.extend({
            template: _.template(' <a class="repetidora-selector" href="#"> <span id="repetidora_<%= id %>" class="programas-container"><img src="<%= image %>"> <div class="horarios-container"></div></span></a>'),
            itemViewContainer : 'div.horarios-container',
            getItemView : function(item)
            {
                var itemView;
                switch( item.get('type') )
                {
                    case 'lugar' : itemView = View.ItemLugar; break;
                    case 'programa' : itemView = View.ItemPrograma; break;
                    default: console.log('no definido ' + item.get('type')); break;
                };
                return itemView;
            },
            tagName    : 'li',
            className  : 'repetidora_item',
            events     : { 'click a.repetidora-selector' : 'select' },
            select:function() {
                this.trigger('selected'); 
            },
            initialize : function() {
                if( ! this.model.get('path') )
                    this.model.set('image','/img/img-radio.png')
                else 
                    this.model.set('image',"/uploads/emisoras/" + this.model.get('path')) ;
                this.collection = this.model.get('sublist'); // es una Backbone.Collection
            },
            onRender : function() {
                var programa = this.model.get('sublist').findWhere({type:'programa'})  ;
                if( programa.get('active') )
                    $(this.el).find('a.repetidora-selector').addClass('active');
            },
            onItemviewModelChanged:function(item){
                this.render();
            }
        });

        View.RepetidorasList = Backbone.Marionette.CompositeView.extend({
            template           : _.template('<div class="emisoras-container scroll-content"><ul id="items" class="horarios scroll-content"></div></div>'),
            itemView           : View.RepetidoraListItem,
            itemViewContainer  : 'ul.horarios#items',
            onItemviewSelected : function(item,data)
            {
                var repetidora = item.model.get('repetidora');
                var programa = item.model.get('sublist').findWhere({type:'programa'});

                EventDispatcher.trigger('app:duracion:repetidora:toggled',{repetidora:repetidora, programa:programa});
            },
            initialize : function() {
                var data = [] ;
                var counter = 1;

                // transform data
                this.collection.each(function(repetidora){
                    var programas = repetidora.get('sublist').where({'type':'programa'});
                    _.each(programas,function(programa){

                        var lugar = {
                            id     : 'lugar_' + repetidora.get('lugar').get('id') ,
                            nombre : repetidora.get('lugar').get('nombre').toUpperCase() ,
                            type   : 'lugar'
                        };

                        data.push({
                            id         : repetidora.get('id') + '_' + counter,
                            repetidora : repetidora ,
                            sublist    : new Backbone.Collection([ programa, new Backbone.Model(lugar) ]),
                            path       : repetidora.get('repetidora').get('path')
                        });

                        ++counter;
                    });
                });

                this.collection = new Backbone.Collection(data);
            }
        });

        var Model = {};
        Model.Duracion = Backbone.Model.extend();
        Model.Duraciones = Backbone.Collection.extend({ 
			model : Model.Duracion,
            initialize:function(){
                this._meta = {};
            },
            meta: function(prop, value) {
                if (typeof value === "undefined") {
                    return this._meta[prop]
                } else {
                    this._meta[prop] = value;
                }
            }
		});

        var Package = {} ;
        Package.Model = Model ;
        Package.Layout = Layout ;
        Package.View = View ;
        return Package ;
    }
);
