define('package/registracion', ['backbone','marionette','underscore','jquery'],
    function(Backbone, Marionette, _, $)
    {
        var usercreatedcallback ; // es cabeza pero rapido

        var FormLayout = Backbone.Marionette.Layout.extend({
            template:function(){ return ''; },
            className:'registracion-form',
            tagName:'div',
            regions:{captcha:'#captcha-region'},
            events:{
                'click input.btn#register'    : 'register',
                'change input[type=text]'     : 'dataChanged',
                'change input[type=email]'    : 'dataChanged',
                'change input[type=password]' : 'dataChanged',
                'change input[type=checkbox]' : 'condicionesChanged',
            },
            dataChanged:function(e)
            {
                var input = $(e.target);
                var name = input.attr('name');
                var val = input.val();

                this.model.set(name,val);
            },
            recaptcha:function(){
                var img = $(this.el).find('img');
                img.src = '/_gcb/generate-captcha/gcb_captcha?n=' + (new Date()).getTime();
            },
            register:function(e){
                e.preventDefault();

                var condiciones = $('input#condiciones');
                if( condiciones.is(':checked') )
                {
                    var form = this.serializeData();
                    var self = this;

                    $.ajax({
                        method:'POST',
                        data:form,
                        url:this.model.url,
                        success:function(response,txt,xhr){
                            $('div.modal.registracion').modal('hide');

                            $('div.modal.gracias#registracion-ok').on('hidden.bs.modal',function(){
                                window.location.href = "/" ;
                            });

                            $('div.modal.gracias#registracion-ok').modal('show');

                            usercreatedcallback(response.data);
                        },
                        error:function(xhr,txt,err){
                            // informar los errores
                            self.recaptcha();
                        }
                    });
                }
                else {
                    $('div#warning-condiciones').modal('show');
                }
            },
            render:function(){
                var self = this;
                $.get("/register_ajax", function(tpl){
                    var html = _.template(tpl);
                    self.$el.html(html);
                });
                return this;
            }
        });

        var ModalLayout = Backbone.Marionette.Layout.extend({
            template : _.template('<button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button> <div id="body-region" class="modal-body clearfix"> </div>'),
            tagName : 'div',
            className : 'modal ms registracion hide fade',
            regions : { body : "#body-region" },
            events:{ 'click a#login-btn':'login' },
            login:function(){
                $(this.el).modal('hide');
            },
            onRender : function(){
                $(this.el).modal('show');
            }
        });

        var Registracion = Backbone.Model.extend({
            url: BaseURI + '/register_ajax'
        });

        var Factory = function(procesarsolicitudcallback)
        {
            var modal = new ModalLayout();
            var form = new FormLayout({ model:new Registracion() });

            usercreatedcallback = procesarsolicitudcallback ;

            modal.render();
            modal.body.show(form);
        }

        return { ModalFactory : Factory };
    }
);
