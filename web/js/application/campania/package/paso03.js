define('package/paso03',
    ['scrollbar','marionette','model/event_dispatcher'],
    function(Scrollbar, Marionette, EventDispatcher){

        var _layout_tpl = _.template('\
            <div class="template-content">\
                <h1><strong>3.</strong> ¿En qué <strong>horarios?</strong></h1>\
                <div class="row-fluid">\
                    <div id="lugares-region" class="span3 line-right"> </div>\
                    <div id="repetidoras-region" class="span9"> </div>\
                    <div id="programas-region"  class="modal emisora hide fade"> </div>\
                </div>\
                <a href="#" class="ico back"></a>\
                <a href="#" id="paso_4" class="siguiente-link">SIGUIENTE <span class="next"></span></a>\
                <span class="pico"></span>\
            </div>'
        );

        var Layout = Backbone.Marionette.Layout.extend({
            template : _layout_tpl,
            regions : {
                lugares  : "#lugares-region",
                repetidoras : "#repetidoras-region"
            },
            className : "paso_planificacion_view",
            events : { 
                'click a.ico.back':'volver',
                'click a.siguiente-link#paso_4':'continuar' 
            },
            continuar : function() {
                EventDispatcher.trigger('app:horario:finalizar_seleccion');
            },
            volver : function() {
                EventDispatcher.trigger('app:planificacion:paso_anterior', 3);
            }
        });

        var View = {} ;
        View.Lugar = Backbone.Marionette.ItemView.extend({
            template: _.template('<a id="lugar_<%= id %>" class="lugar_item" href="#"><span></span><%= nombre %></a>'),
            tagName: 'li', 
            events: { 'click a.lugar_item':'select' },
            select: function() {
                this.trigger('lugar:selected'); // asi informa a la collection del evento
            },
            modelEvents:{ 'change':'render' },
            onRender:function(){
                if( this.model.get('selected') )
                    $(this.el).addClass('active');
                else
                    $(this.el).removeClass('active');
            }
        });

        View.Lugares = Backbone.Marionette.CompositeView.extend({
			initialize:function()
			{
				this.on('child:changed',function(data) {
					var child = $(this.el).find('li a#lugar_' + data.childId + ' span');
					if( data.complete ) child.addClass('checked');
					else child.removeClass('checked');
				});
			},
            template: _.template('<div class="scroll-content"><ul id="items" class="selector text-right"></ul></div>'),
            itemView:View.Lugar,
            itemViewContainer:'ul#items',
            onItemviewLugarSelected: function(item, data) {
                EventDispatcher.trigger('app:horario:lugar_panel:cargar_repetidoras',item.model);
            },
            onRender:function() {
//                $(this.el).find('.scroll-content').mCustomScrollbar();
            }
        });

        View.Horario = Backbone.Marionette.ItemView.extend({
            template: _.template('<a id="celda_<%= id %>" class="btn-light grey"><%= dia %> | <%= horai %> a <%= horaf %></a><br/>'),
            tagName:'span'
        });

        View.Repetidora = Backbone.Marionette.CompositeView.extend({
            initialize:function(){
                if( ! this.model.get('path') )
                    this.model.set('image','/img/img-radio.png')
                else 
                    this.model.set('image',"/uploads/emisoras/" + this.model.get('path')) ;
            },
            template: _.template('\
                <span id="repetidora_<%= id %>" class="programas-container">\
                    <a class="imagen" href="#" data-toggle="modal"><img src="<%= image %>"></a>\
                    <div>\
                        <div id="horarios" class="horarios-container"></div>\
                        <a id="programas" href="#" data-toggle="modal" class="btn-light">seleccionar</a>\
                    </div>\
                </span>\
                '),
            tagName    : 'li',
            className  : 'repetidora_item',
            events     : { 
                            'click .imagen' : 'select',
                            'click a#programas.btn-light' : 'select',
                         },
            select     : function() { this.trigger('selected'); },
            itemView   : View.Horario,
            itemViewContainer : 'div#horarios.horarios-container',
            initialize : function() {
                this.collection = this.model.get('horarios');
            }
        });

        View.Repetidoras = Backbone.Marionette.CompositeView.extend({
            template           : _.template('<div class="emisoras-container scroll-content"><ul id="items" class="horarios scroll-content"></div></div>'),
            itemView           : View.Repetidora,
            itemViewContainer  : 'ul.horarios#items',
            onItemviewSelected : function(item,data)
            {
                // selecciono un horario o va a elegir uno nuevo
                EventDispatcher.trigger('app:horario:repetidora_panel:cargar_programas',item.model);
            },
            onRender:function()
            {
//                $(this.el).find('.scroll-content').mCustomScrollbar();
            }
        });

        /*
         *
         * Templates
         *
         */
        var _modal_tpl = _.template('\
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"> </button>\
            <h2> <span id="emisora-nombre" class="nombre"><%= nombre %></span> </h2>\
            <div id="body-region" class="modal-body">\
            </div>\
            <a id="confirm-selections" href="#" class="ok-big"></a>\
        ');

        var _grilla_tpl = _.template('\
            <table witdh="100%" border="0" cellspacing="0" cellpadding="0" class="table">\
            <tbody id="body-container"> </tbody>\
            </table>\
        ');

        /*
         *
         * Vistas
         *
         */
        View.Grilla = {};
        View.Grilla.Modal = Backbone.Marionette.Layout.extend({
            template : _modal_tpl,
            tagName : 'div',
            className : 'modal emisora hide fade',
            regions : { body : "#body-region" },
            onRender : function(){
                $(this.el).modal('show');
            },
            events:{ 'click a#confirm-selections':'confirmar' },
            confirmar:function() {
                var tabla = this.body.currentView; // la tabla
                var changes = [];

                tabla.children.each(function(fila){
                    fila.children.each(function(celda){
                        if( typeof celda.model.get('selected') != 'undefined' ) {
                            // traigo todos los que tengan el atributo true/false
                            changes.push( celda.model );
                        }
                    });
                });

                EventDispatcher.trigger('app:horario:programas_grilla:confirmar_cambios',changes); // asi registro los horarios seleccionados

                $(this.el).modal('hide');
            },
            modelEvents:{
                'change' : 'updateName'
            },
            updateName:function(){
                $(this.el).find("span#emisora-nombre").html( this.model.get('nombre') );
            }
        });

        View.Grilla.CeldaPrograma = Backbone.Marionette.ItemView.extend({
            template: _.template('<a class="programa_cell-item col<%= columna %>" href="#" id="programa_<%= id %>" ><%= nombre %></a>'),
            tagName: 'td',
            className: 'programa_cell-container',
            events:{ 'click':'select' },
            select:function()
            {
                var el = $(this.el) ;
                if( el.hasClass('active') ) {
                    el.removeClass('active');
                    this.model.set('selected',false); // este atributo se mantiene solo en el model de la vista. cuando la vista se destruye se pierde
                } else {
                    el.addClass('active');
                    this.model.set('selected',true);
                }
                //this.trigger('horario:selected');
            },
            onRender:function(){
                if( this.model.get('selected') )
                    $(this.el).addClass('active') ;
                else
                    this.trigger('celda:rendered');
            }
        });

        View.Grilla.CeldaDia = Backbone.Marionette.ItemView.extend({
            template: _.template('<span id="programa_<%= id %>" ><%= nombre %></span>'),
            tagName: 'th',
            className: 'dia_cell-container',
            onRender:function()
            {
                var el = $(this.el);
                if( this.model.get('id') == 0 ) el.addClass('dark');
                else if( (this.model.get('id') % 2) == 0 ) el.addClass('light');
            }
        });

        View.Grilla.CeldaHorario = Backbone.Marionette.ItemView.extend({
            template: _.template('<span id="programa_<%= id %>" ><%= nombre %></span>'),
            tagName: 'th',
            className: 'horario_cell-container'
        });

        View.Grilla.Fila = Backbone.Marionette.CompositeView.extend({
            template  : function(){ return ''; },
            tagName   : 'tr',
            className : 'programas_row-container',
            getItemView : function(item)
            {
                var itemView;
                switch( item.get('type') )
                {
                    case 'programa' : itemView = View.Grilla.CeldaPrograma ; break ; 
                    case 'horario'  : itemView = View.Grilla.CeldaHorario  ; break ; 
                    case 'dia'      : 
                        itemView = View.Grilla.CeldaDia;
                        $(this.el).addClass('main-th');
                        break ; 
                };
                return itemView;
            },
            initialize: function() {
                this.collection = new Backbone.Collection( _.toArray(this.model.attributes) );
            },
            onItemviewCeldaRendered:function(args){
                this.trigger('celda:rendered',args);
            }
        });

        View.Grilla.Tabla = Backbone.Marionette.CompositeView.extend({
            template : _grilla_tpl,
            itemView : View.Grilla.Fila,
            itemViewContainer : 'tbody',
            initialize:function()
            {
                this.renderedCount = 0;
                var grid = this.collection.groupBy(function(list,iterator) {
                    return Math.floor(iterator / 4); // 4 == number of columns
                });
                this.collection = new Backbone.Collection( _.toArray(grid) );
            },
            onRender:function(){
                this.group();
            },
            group:function()
            {
                var group = function(col1)
                {
                    var i = 0;
                    var groups = [];
                    while( i < col1.length ){
                        var item = col1[i];
                        var nombre = item.html();
                        var count = 0;
                        var items = [];

                        while( i < col1.length && nombre == item.html() ){
                            items.push( item );
                            i++;
                            item = col1[i];
                        }

                        groups.push( items );
                    }

                    var ble = 0;
                    for( var i in groups ) {
                        ble++;
                        var items = groups[i];

                        var cnt = items.length ;
                        if( cnt % 2 == 0 ) pos = Math.floor( cnt / 2 ) ; 
                        else pos = Math.floor( cnt / 2 ) + 1;

                        for( var j=1; j<=items.length; j++ ) {
                            var item = items[j-1];
                            if( ble % 2 == 0 ) {
                                item.parent('td').addClass('osc');
                            }

                            if( j != pos ) {
                                item.html('&nbsp;');
                            }
                        }
                    }
                };

                var col1 = [];
                var col2 = [];
                var col3 = [];

                this.children.each(function(row){
                    row.children.each(function(cell){
                        var a = $(cell.el).find('a') ;
                        if( typeof a != 'undefined' )
                        {
                            if( a.hasClass('col1') ){
                                col1.push( a );
                            }
                            else if( a.hasClass('col2') ){
                                col2.push( a );
                            }
                            else if( a.hasClass('col3') ){
                                col3.push( a );
                            }
                        }
                    });
                });

                group(col1);
                group(col2);
                group(col3);
            }
        });

        // el indice de cada elemento corresponde 
        // al numero de comienzo del rango
        var horarios = [ '0',
            '01:00-02:00' , '02:00-03:00' , '03:00-04:00' , '04:00-05:00' , 
            '05:00-06:00' , '06:00-07:00' , '07:00-08:00' , '08:00-09:00' , 
            '09:00-10:00' , '10:00-11:00' , '11:00-12:00' , '12:00-13:00' , 
            '13:00-14:00' , '14:00-15:00' , '15:00-16:00' , '16:00-17:00' , 
            '17:00-18:00' , '18:00-19:00' , '19:00-20:00' , '20:00-21:00' , 
            '21:00-22:00' , '22:00-23:00' , '23:00-24:00' , '24:00-01:00' , 
        ];

        // el indice de cada elemento del array
        // corresponde al número de día, excepto los rangos
        var dias = [ '', 
            'DOMINGO'   , 'LUNES'           , 'MARTES'         , 
            'MIERCOLES' , 'JUEVES'          , 'VIERNES'        , 
            'SABADO'    , 'LUNES A VIERNES' , 'LUNES A JUEVES' ,
        ];

        var Model = {} ;
        Model.Grilla = {};
        Model.Grilla.Celda = Backbone.Model.extend();

        /* 
         * La matriz es una Lista ordenado de Programas
         * que se utiliza para generar la tabla
         */
        Model.Grilla.Matriz = Backbone.Collection.extend({
            model:Model.Grilla.Celda,
            url:function()
            {
                return BaseURI + '/api/programa/fetch/?repetidora=' + this.meta('repetidora').get('id') ;
            },
            initialize:function(options)
            {
                this._meta = {};
                this.meta('rowoffset', 5); // el offset es para mostrar las hs desde la hora 6
                this.meta('colcount', 4);
                this.meta('rowcount', 24);
                this.meta('matriz', this.initMatriz());
            },
            parse:function(data)
            {
                this.meta('programas',data.programas);
                this.completeMatriz();

                return this.meta('matriz') ;
            },
            meta: function(prop, value)
            {
                if (typeof value === "undefined") {
                    return this._meta[prop]
                } else {
                    this._meta[prop] = value;
                }
            },
            getCelda:function(dia,hora)
            {
                var rowoffset = this.meta('rowoffset');
                var colcount = this.meta('colcount');
                /* 
                 * Las horas por cada fila
                 * 1) 06:00
                 * 2) 07:00
                 * ...
                 * 18) 20:00
                 * 19) 21:00
                 * ...
                 * 23) 04:00
                 * 24) 05:00
                 *
                 * determino la fila según la hora.
                 * la file 0 corresponde a los dias
                 *
                 */
                var row = hora - rowoffset;

                if ( row < 1 ) // 1 corresponde a la hora 6 , primer fila/hora de cada columna
                    row = 24 + row ;

                /* 
                 * Los días los agrupo por rango
                 * 1) LUN a VIE : columna 1
                 * 2) SAB       : columna 2
                 * 3) DOM       : columan 3
                 */
                var col ;
                // determino la columna según el día.
                // la columna 0 corresponde a las horas
                switch(dia)
                {
                    // LUNES:MARTES:MIERCOLES:JUEVES:VIERNES:
                    case 2: case 3: case 4: case 5: case 6: col = 1; break;
                    // SABADO
                    case 7 : col = 2; break;
                    // DOMINGO
                    case 1 : col = 3; break;
                    // COLUMNAS HORA
                    case 99: col = 0; break;
                }

                return row * colcount + col ; // la celda buscada
            },
            initMatriz:function() // no es una estructura matriz (array de dos dimensiones) en realidad, pero los indices estan mapeados a cada celda
            {
                //
                // La matriz se representa con un array de 25 * 4 = 100 elementos (incluido el header)
                // Se recorre por horizontalmente x fila para poder generar la tabla html
                //
                var matriz = [];
                for(var i=0; i<100; i++) 
                    matriz[i] = null ;

                // completo las celdas correspondientes a las horas
                for(var hora=1; hora<=24; hora++)
                {
                    celda = this.getCelda(99,hora);

                    matriz[celda] = {
                        id       : celda,
                        nombre   : horarios[hora],
                        type : 'horario'
                    };
                }

                // completo las celdas para el header de cada columna
                matriz[0] = { id:0 , nombre:''      , type:'dia' };
                matriz[1] = { id:1 , nombre:dias[8] , type:'dia' };
                matriz[2] = { id:2 , nombre:dias[7] , type:'dia' };
                matriz[3] = { id:3 , nombre:dias[1] , type:'dia' };

                return matriz;
            },
            completeMatriz:function()
            {
                var matriz = this.meta('matriz');
                var self = this;

                var dia, hora ;
                var celda;
                _.each( this.meta('programas'), function(programa) {
                    _.each( programa.horarios, function(horario) {
                        dia  = horario.dia.numeroDia;
                        hora = horario.hora.numero;

                        if( dia >= 2 && dia <= 6 ) // LUN A VIER
                        {
                            nombredia = 'L a V';
                            columna = 1 ;
                        } else {
                            nombredia = horario.dia.nombreDia.substr(0,3).toUpperCase() ;
                            if( dia == 1 ) columna = 3 ;
                            else columna = 2 ;
                        }

                        celda = self.getCelda(dia,hora);
                        matriz[celda] = {
                            id       : celda,
							horario  : horario,
                            dia      : nombredia,
                            horai    : horario.hora.numero,
                            horaf    : horario.hora.numero == 24 ? 1 : horario.hora.numero + 1 ,
                            programa : programa,
                            nombre   : programa.nombre,
                            columna  : columna ,
                            type     : 'programa'
                        }
                    });
                });
            }
        });

        Model.Lugar = Backbone.Model.extend({});
        Model.Repetidora = Backbone.Model.extend({});
        Model.Repetidoras = Backbone.Collection.extend({
            initialize:function(){
                this._meta = {};
            },
            model:Model.Repetidora,
            meta: function(prop, value) {
                if (typeof value === "undefined") {
                    return this._meta[prop]
                } else {
                    this._meta[prop] = value;
                }
            }
        });

        Package = {};
        Package.Model = Model;
        Package.View  = View;
        Package.Layout = Layout;

        return Package;
    }
);
