define('package/paso06',
    ['marionette','model/event_dispatcher'],
    function(Marionette, EventDispatcher)
    {
        var _layout_tpl = _.template('\
            <div class="campaign paypal">\
                <h1><strong>6.</strong> Quieres confirmar <strong>tu descuento?</strong></h1>\
                <div class="total"> PRECIO TARIFA <span class="tarifa"><%= total %></span> PRECIO TOTAL <span class="tarifa underline"><%= con_descuento %></span> </div>\
                <div class="row-fluid">\
                    <div class="span7">\
                        <div class="txt-container">\
                            <p class="txt-big">Con el pago de tan solo € <%= costo_servicio %><br>(1% del total) te confirmaremos el coste real de tu compra, que podría ser másbajo que el precio final mostrado.</p>\
                            <p class="txt-info">Este pago incluye, negociación definitiva con las emisoras, calendario emisión, envio de ordenes, reserva de espacios, control y seguimiento de campaña.</p>\
                            <a id="registracion-modal" href="#" class="btn btn-green btn-large">OBTENER DESCUENTO!</a>\
                        </div>\
                    </div>\
                    <div class="span5"> <img src="img/img-paypal.gif" width="232" height="194" alt="Paypal"> </div>\
                </div>\
                <span class="pico"></span>\
            </div>\
        ');

        var Layout = Backbone.Marionette.ItemView.extend({
            template : _layout_tpl,
            className : "paso_planificacion_view",
            events : { 'click a#registracion-modal':'registrar' },
            registrar : function() {
                EventDispatcher.trigger('app:confirmacion:registrar');
            }
        });

        var View = {};

        var Model = {};
        Model.Precio = Backbone.Model.extend({
            initialize:function(){
                var con_descuento = parseFloat(this.get('con_descuento'));

                var costo = con_descuento * (1/100) ; // el 1%
                this.set('costo_campania', parseFloat(con_descuento - costo));
                this.set('costo_servicio', parseFloat(costo).toFixed(2));
            }
        });

        Model.Solicitud = Backbone.Model.extend({
            initialize:function(options) {
                this.url += options.userid;
            },
            url:BaseURI + '/api/solicitud/',
        });

        var Package    = {};
        Package.Model  = Model;
        Package.Layout = Layout;
        Package.View   = View;
        return Package;
    }
);
