define('package/paso02',
    ['scrollbar','marionette','underscore','model/event_dispatcher'],
    function(Scrollbar, Marionette, _, EventDispatcher){

        var _layout_tpl = _.template('\
            <div class="template-content">\
                <h1><strong>2.</strong> ¿En que <strong>emisoras?</strong></h1>\
                <div class="row-fluid">\
                    <div id="lugares-region" class="span3 line-right"> </div>\
                    <div id="emisoras-region" class="span9"> </div>\
                </div>\
                <a href="#" class="ico back"></a>\
                <a href="#" id="paso_3" class="siguiente-link">SIGUIENTE <span class="next"></span></a>\
                <span class="pico"></span>\
            </div>');

        var Layout = Backbone.Marionette.Layout.extend({
            template : _layout_tpl,
            regions : {
                lugares  : "#lugares-region",
                emisoras : "#emisoras-region"
            },
            className : "paso_planificacion_view",
            events : { 
                'click a.ico.back':'volver',
                'click a.siguiente-link#paso_3':'continuar' 
            },
            continuar : function() {
                EventDispatcher.trigger('app:emisora:finalizar_seleccion');
            },
            volver : function() {
                EventDispatcher.trigger('app:planificacion:paso_anterior', 2);
            }
        });


        var View = {} ;
        View.Lugar = Backbone.Marionette.ItemView.extend({
            template: _.template('<a id="lugar_<%= id %>" class="lugar_item" href="#"><span></span><%= nombre %></a>'),
            tagName:'li', 
            events:{ 'click a.lugar_item':'select' },
            select:function(){
                // dejo que la collection manipule el evento
                this.trigger('lugar:selected'); // informa a la collection del evento
            },
            modelEvents:{ 'change':'render' },
            onRender:function(){
                if( this.model.get('active') ) $(this.el).addClass('active');
                else $(this.el).removeClass('active');

                var span = $(this.el).find('span');
                if( this.model.get('checked') ) span.addClass('checked');
                else span.removeClass('checked');
            }
        });

        View.Lugares = Backbone.Marionette.CompositeView.extend({
            template: _.template('<div class="scroll-content"><ul id="items" class="selector text-right"></ul></div>'),
            itemView:View.Lugar,
            itemViewContainer:'ul#items',
            onItemviewLugarSelected: function(item, data) {
                EventDispatcher.trigger('app:emisora:lugar:seleccionar',item.model);
            },
            onRender:function() {
//                $(this.el).find('.scroll-content').mCustomScrollbar("update");
            }
        });

        View.Repetidora = Backbone.Marionette.ItemView.extend({
            initialize:function(){
                if( ! this.model.get('path') )
                    this.model.set('image','/img/img-radio.png')
                else 
                    this.model.set('image',"/uploads/emisoras/" + this.model.get('path')) ;
            },
            template: _.template('<span id="repetidora_<%= id %>" class="img-container"><img src="<%= image %>" style="width:87px; heigth:87px;"></span>'),
            tagName: 'a', 
            className: 'emisora_item',
            events:{
                'click span.img-container':'select'
            },
            select:function(){
                this.trigger('selected');
            },
            onRender:function(){
                if( this.model.get('selected') )
                    $(this.el).addClass('active') ;
                else 
                    $(this.el).removeClass('active') ;
            },
            modelEvents:{ 'change':'render' }
        });

        View.Repetidoras = Backbone.Marionette.CompositeView.extend({
            template           : _.template('<div id="items" class="emisoras-container scroll-content"></div>'),
            itemView           : View.Repetidora,
            itemViewContainer  : 'div.emisoras-container#items',
            onItemviewSelected : function(item,data) {
                EventDispatcher.trigger('app:emisora:repetidora:seleccionada',item.model);
            },
            onRender:function() {
//                $(this.el).find('.scroll-content').mCustomScrollbar();
            }
        });

        var Model = {} ;
        Model.Lugar = Backbone.Model.extend({
            check:function(count){
                this.set('checked', count > 0);
            }
        });
        Model.Repetidora = Backbone.Model.extend({ });
        Model.Repetidoras = Backbone.Collection.extend({
            url:function() {
                return BaseURI + '/api/repetidora/' + this.meta('categoria').get('id') + '/' + this.meta('lugar').get('id');
            },
            parse:function(response,options){
                return response.repetidoras ;
            },
            initialize:function(){
                this._meta = {};
            },
            model:Model.Repetidora,
            meta: function(prop, value) {
                if (typeof value === "undefined") {
                    return this._meta[prop]
                } else {
                    this._meta[prop] = value;
                }
            }
        });

        Package = {} ;
        Package.Model = Model ;
        Package.View = View ;
        Package.Layout = Layout ;

        return Package ;
    }
);
