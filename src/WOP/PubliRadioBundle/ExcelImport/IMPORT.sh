#
#
# Importo los fixtures por separado para evitar tener que importar todo desde cero
# en caso de un error en la importacion.
#
# También aumento la velocidad de ejecucion de los scripts importando en procesos diferentes
# proceso en etapas , libero recursos , reinicio cache , etc
#
#

ROOT_DIR=$1

cd $ROOT_DIR

php app/console doctrine:fixtures:load

php app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/Emisoras/ --append

#php app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/EmisorasLocales/ --append

php app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaNacional/ --append

php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaRegional/ --append

php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaProvincial/ --append

#php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal/ --append
