<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use WOP\PubliRadioBundle\Entity\Repetidora;
use WOP\PubliRadioBundle\Entity\Precio;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTarifaNacionalData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer() 
    {
        return $this->container ;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $reader = $this->getXLSXReader();

        $logger = $this->container->get('logger');

        $worksheet = $reader->getActiveSheet();

        echo "  >> Processing data\n";

        $lastRow = $worksheet->getHighestDataRow();
        $lastColumn = $worksheet->getHighestDataColumn();

        $column = 'A'; // la columna que tiene los días y horarios
        $dias_horarios = array();
        for($row=1; $row<=$lastRow; $row++)
        {
            $dias_horarios[ $row ] = $worksheet->getCell( $column . $row )->getValue();
        }

        // recorro cada columna y creo las repetidoras nacionales para cada día y horario
        for( $column = 'B'; $column != $lastColumn; $column++ )
        {
            $logger->debug("Procesando Columna {$column}");

            $row = 1; // la repetidora
            $nombre = $worksheet->getCell($column . $row)->getValue();

            $logger->debug("Prepetidora {$nombre}");

            $emisora = $manager->getRepository('WOPPubliRadioBundle:Emisora')->findOneBy(array('nombre'=> $nombre));

            // if( empty($emisora) ) ??
            $logger->debug("Emisora {$emisora}");

            $repetidora = new Repetidora();
            $repetidora->setNombre($nombre);
            $repetidora->setNacional(true);
            $repetidora->setEmisora($emisora);

            $logger->debug("Guardando Repetidora {$repetidora}");
            $manager->persist($repetidora);
            $manager->flush();

            $rangoDia = $dias_horarios[ $row ]; // el primer rango de dias esta en la celda A1

            $insert_count = 20; // bulk inserts

            do {
                $row++; // primera vez que entra al bucle row = 2
                if( isset($dias_horarios[$row]) )
                {
                    $colA = $dias_horarios[$row];

                    if( !empty($colA) )
                    {
                        if( $colA == 'L-V' || $colA == 'SÁBADO' || $colA == 'DOMINGO' )
                        {
                            $rangoDia = $colA; // el rango de dias

                            $row++;
                            $colA = $dias_horarios[ $row ]; // el rango horario
                        }

                        $logger->debug("Utilizando rango dia {$rangoDia}");
                        $rangoHora = $colA;
                        $logger->debug("Utilizando rango horario {$rangoHora}"); // los rango hora de 1 hora.

                        $data = $worksheet->getCell($column . $row)->getValue();
                        $valor = str_replace(' €','',$data);

                        $horarioManager = $this->container->get('horario_manager');

                        $logger->debug("Procesando horarios...");
                        $horarios = $horarioManager->getWithEmisoraDiasHoras(
                            $emisora,
                            $rangoDia,
                            $rangoHora
                        );

                        if(!empty($horarios)) {
                            foreach($horarios as $horario) {
                                $logger->debug("Horario {$horario}");
                                $precio = new Precio();
                                $precio->setValor($valor/20); // los valores son x 20 seg de publicidad
                                $precio->setRepetidora($repetidora);
                                $precio->setHorario($horario);

                                $logger->debug("Guardando Precio {$precio}");
                                $manager->persist($precio);

                                if( $insert_count == 0 )
                                {
                                    $manager->flush();
                                    $manager->clear('WOPPubliRadioBundle:Precio');
                                    $insert_count = 20;
                                }
                                else --$insert_count;
                            }
                            $manager->clear('WOPPubliRadioBundle:Horario');
                        } else $logger->error("No se encontraron horarios");
                    }
                }
            } while($row <= $lastRow);
            $manager->clear('WOPPubliRadioBundle:Repetidora');
        }
    }

    private function getXLSXReader()
    {
        echo "  >> Reading xls file tarifas_nacional.xls\n";

        $input = __DIR__ . '/../data/xls/tarifas_nacional.xls';

        $reader = \PHPExcel_IOFactory::createReader( \PHPExcel_IOFactory::identify($input) );
        $reader->setReadDataOnly(true);
        return $reader->load($input);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8; // the order in which fixtures will be loaded
    }
}
