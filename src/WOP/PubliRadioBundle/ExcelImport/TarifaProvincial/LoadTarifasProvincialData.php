<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use WOP\PubliRadioBundle\Entity\Repetidora;
use WOP\PubliRadioBundle\Entity\Precio;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTarifaProvincialData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer() 
    {
        return $this->container ;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $reader = $this->getXLSXReader();
        $logger = $this->container->get('logger');
        $emisoraManager = $this->container->get('emisora_manager');

        $manager->getConnection()->getConfiguration()->setSQLLogger(null);

        $index = 0;
        foreach($reader->getWorksheetIterator() as $worksheet)
        {
            $provinciaNombre = $worksheet->getTitle() ;
            $logger->debug("Procesando PROVINCIA {$provinciaNombre}");
            echo "Procesando PROVINCIA {$provinciaNombre}\n";

            $provincia = $manager
                ->getRepository('WOPPubliRadioBundle:Provincia')
                ->findOneByNombre($provinciaNombre);

            if( empty($provincia) )
            {
                $logger->error("No se encontro la PROVINCIA {$provinciaNombre}");
                //echo "No se encontro la PROVINCIA {$provinciaNombre}\n";
            }
            else 
            {
                /**
                 *
                 * indexo la columna que tiene los días y horarios.
                 *
                 */
                $lastRow    = $worksheet->getHighestDataRow();
                $lastColumn = $worksheet->getHighestDataColumn();
                $column = 'A';
                $dias_horarios = array();
                for($row=2; $row<=$lastRow; $row++) {
                    $dias_horarios[ $row ] = $worksheet->getCell($column . $row)->getValue();
                }

                /**
                 *
                 *  recorro cada columna y creo las repetidoras de la provincia
                 *  para cada día y horario.
                 *
                 */
                for( $column = 'B'; $column != $lastColumn; $column++ )
                {
                    /**
                     *
                     * obtengo el nombre de la repetidora y creo la instancia.
                     *
                     */
                    $logger->debug("Columna {$column}");
                    $row = 2;
                    $nombreRepetidora = $worksheet->getCell($column . $row)->getValue();

                    $logger->debug("Procesando REPETIDORA {$nombreRepetidora}");
                    echo "Procesando REPETIDORA {$nombreRepetidora}\n";

                    /**
                     *
                     * busco una emisora que machee con la repetidora
                     * creo la repetidora y proceso los horarios
                     *
                     */
                    $emisora = $emisoraManager->findMatchByName($nombreRepetidora);

                    if( empty($emisora) )
                    {
                        $logger->error("No se encontro EMISORA para la REPETIDORA {$nombreRepetidora}");
                        //echo "No se encontro EMISORA para la REPETIDORA {$nombreRepetidora}\n";
                    } 
                    else
                    {
                        $logger->debug("EMISORA {$emisora}");

                        $repetidora = new Repetidora();
                        $repetidora->setNombre($nombreRepetidora);
                        $repetidora->setNacional(false);
                        $repetidora->setProvincia($provincia);
                        $repetidora->setEmisora($emisora);

                        $logger->debug("Creando REPETIDORA {$repetidora}");
                        //echo "Creando REPETIDORA {$repetidora}\n";

                        $manager->persist($repetidora);
                        $manager->flush();

                        $rangoDia = $dias_horarios[ $row ]; // el primer rango de dias esta en la celda A1

                        $insert_count = 20; // bulk inserts

                        do {
                            $row++; // primera vez que entra al bucle row = 2
                            if( isset($dias_horarios[$row]) )
                            {
                                $colA = $dias_horarios[$row];

                                if( !empty($colA) )
                                {
                                    if( $colA == 'L-V' || $colA == 'SÁBADO' || $colA == 'DOMINGO' )
                                    {
                                        $rangoDia = $colA; // el rango de dias

                                        $row++;
                                        $colA = $dias_horarios[ $row ]; // el rango horario
                                    }

                                    $logger->debug("Utilizando rango dia {$rangoDia}");
                                    //echo "Utilizando rango dia {$rangoDia}\n";
                                    $rangoHora = $colA;

                                    $logger->debug("Utilizando rango horario {$rangoHora}"); // los rango hora de 1 hora.
                                    //echo "Utilizando rango horario {$rangoHora}\n";

                                    $data = $worksheet->getCell($column . $row)->getValue();
                                    $valor = str_replace(' €','',$data);

                                    if( !empty($valor) && is_numeric($valor) )
                                    {
                                        $horarioManager = $this->container->get('horario_manager');

                                        $logger->debug("Procesando horarios...");

                                        $horarios = $horarioManager->getWithEmisoraDiasHoras(
                                            $emisora,
                                            $rangoDia,
                                            $rangoHora
                                        );

                                        if( !empty($horarios) )
                                        {
                                            foreach($horarios as $horario)
                                            {
                                                $logger->debug("Horario {$horario}");
                                                //echo "Horario {$horario}\n";

                                                $precio = new Precio();
                                                $precio->setValor($valor/20); // los valores son x 20 seg, obtengo el valor del segundo
                                                $precio->setRepetidora($repetidora);
                                                $precio->setHorario($horario);

                                                $logger->debug("Guardando Precio {$precio}");
                                                //echo "Guardando Precio {$precio}\n";

                                                $manager->persist($precio);

                                                if( $insert_count == 0 )
                                                {
                                                    $manager->flush();
                                                    $manager->clear('WOPPubliRadioBundle:Precio');
                                                    $insert_count = 20;
                                                }
                                                else --$insert_count;

                                                $precio = null;
                                                $horario = null;
                                            }

                                            $horarios = null;
                                            $manager->clear('WOPPubliRadioBundle:Horario');
                                        }
                                        else {
                                            $logger->error("No se encontraron horarios para Emisora {$emisora} {$rangoDia}/{$rangoHora}");
                                            echo "No se encontraron horarios para Emisora {$emisora} {$rangoDia}/{$rangoHora}\n";
                                        }
                                    }
                                    else {
                                        $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no contiene un precio valido [{$valor}]");
                                        echo "La CELDA {$worksheet->getTitle()} {$column}:{$row} no contiene un precio valido [{$valor}]\n";
                                    }
                                }
                                else {
                                    $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no contiene datos [{$colA}]");
                                    echo "La CELDA {$worksheet->getTitle()} {$column}:{$row} no contiene datos [{$colA}]\n";
                                }
                            }
                            else {
                                $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no contiene informacion de horario");
                                echo "La CELDA {$worksheet->getTitle()} {$column}:{$row} no contiene informacion de horario\n";
                            }
                        }
                        while($row <= $lastRow);
                    } // end else no emisora

                    $repetidora = null;
                    $manager->flush();
                    $manager->clear('WOPPubliRadioBundle:Repetidora');

                } // end for column
            } // end else no provincia
            $provincia = null;
            $manager->flush();
            $manager->clear();
        } // end foreach
    }

    private function getXLSXReader()
    {
        echo "  >> Reading xls file tarifas_provincial.xls \n";

        $input = __DIR__ . '/../data/xls/tarifas_provincial.xls';

        $reader = \PHPExcel_IOFactory::createReader( \PHPExcel_IOFactory::identify($input) );
        $reader->setReadDataOnly(true);
        return $reader->load($input);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9; // the order in which fixtures will be loaded
    }
}
