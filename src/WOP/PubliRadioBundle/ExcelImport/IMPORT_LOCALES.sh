#
#
# Importo los fixtures por separado para evitar tener que importar todo desde cero
# en caso de un error en la importacion.
#
# También aumento la velocidad de ejecucion de los scripts importando en procesos diferentes
# proceso en etapas , libero recursos , reinicio cache , etc
#
#

ROOT_DIR=$1

cd $ROOT_DIR

php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal1/ --append
php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal2/ --append
php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal3/ --append
php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal4/ --append
php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal5/ --append
php -dmemory_limit=1G app/console doctrine:fixtures:load --fixtures=src/WOP/PubliRadioBundle/ExcelImport/TarifaLocal6/ --append
