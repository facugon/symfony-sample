<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use WOP\PubliRadioBundle\Entity\Repetidora;
use WOP\PubliRadioBundle\Entity\Precio;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadTarifaLocalData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer() 
    {
        return $this->container ;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $reader = $this->getXLSXReader();
        $logger = $this->container->get('logger');
        $emisoraManager = $this->container->get('emisora_manager');

        foreach($reader->getWorksheetIterator() as $worksheet)
        {
            $nombre = $worksheet->getTitle() ;
            $logger->debug("Procesando MUNICIPIO {$nombre}");

            $municipio = $manager
                ->getRepository('WOPPubliRadioBundle:Municipio')
                ->findOneByNombre($nombre);

            if( empty($municipio) )
            {
                $logger->error("No se encontro el MUNICIPIO {$nombre}");
            }
            else 
            {
                /**
                 *
                 * indexo la columna que tiene los días y horarios.
                 *
                 */
                $lastRow    = $worksheet->getHighestDataRow();
                $lastColumn = $worksheet->getHighestDataColumn();
                $column = 'A';
                $dias_horarios = array();
                for($row=2; $row<=$lastRow; $row++) {
                    $dias_horarios[ $row ] = $worksheet->getCell($column . $row)->getValue();
                }

                /**
                 *
                 *  recorro cada columna y creo las repetidoras de la comunidad autonoma
                 *  para cada día y horario.
                 *
                 */
                for( $column = 'B'; $column != $lastColumn; $column++ )
                {
                    /**
                     *
                     * obtengo el nombre de la repetidora y creo la instancia.
                     *
                     */
                    $logger->debug("Columna {$column}");
                    $row = 2;
                    $nombreRepetidora = $worksheet->getCell($column . $row)->getValue();

                    $logger->debug("Procesando REPETIDORA {$nombreRepetidora}");
                    echo "Procesando REPETIDORA {$nombreRepetidora}\n";

                    /**
                     *
                     * busco una emisora que machee con la repetidora
                     * creo la repetidora y proceso los horarios
                     *
                     */
                    $emisora = $emisoraManager->findMatchByName($nombreRepetidora);

                    if( empty($emisora) )
                    {
                        $logger->error("No se encontro EMISORA para la REPETIDORA {$nombreRepetidora}");
                    } 
                    else
                    {
                        $logger->debug("EMISORA {$emisora}");

                        $repetidora = new Repetidora();
                        $repetidora->setNombre($nombreRepetidora);
                        $repetidora->setNacional(false);
                        $repetidora->setMunicipio($municipio);
                        $repetidora->setEmisora($emisora);

                        $logger->debug("Guardando Repetidora {$repetidora}");
                        $manager->persist($repetidora);
                        $manager->flush();

                        $rangoDia = $dias_horarios[ $row ]; // el primer rango de dias esta en la celda A1

                        $insert_count = 20; // bulk inserts
                        do {
                            $row++; // primera vez que entra al bucle row = 2
                            if( isset($dias_horarios[$row]) )
                            {
                                $colA = $dias_horarios[$row];

                                if( !empty($colA) )
                                {
                                    if( $colA == 'L-V' || $colA == 'SÁBADO' || $colA == 'DOMINGO' )
                                    {
                                        $rangoDia = $colA; // el rango de dias

                                        $row++;
                                        $colA = $dias_horarios[ $row ]; // el rango horario
                                    }

                                    $logger->debug("Utilizando rango dia {$rangoDia}");
                                    $rangoHora = $colA;
                                    $logger->debug("Utilizando rango horario {$rangoHora}"); // los rango hora de 1 hora.

                                    $data = $worksheet->getCell($column . $row)->getValue();
                                    $valor = str_replace(' €','',$data);

                                    if( !empty($valor) && is_numeric($valor) )
                                    {
                                        $horarioManager = $this->container->get('horario_manager');

                                        $logger->debug("Procesando horarios...");
                                        $horarios = $horarioManager->getWithEmisoraDiasHoras(
                                            $emisora,
                                            $rangoDia,
                                            $rangoHora
                                        );

                                        if( !empty($horarios) )
                                        {
                                            foreach($horarios as $horario)
                                            {
                                                $logger->debug("Horario {$horario}");
                                                $precio = new Precio();
                                                $precio->setValor($valor/20); // obtengo el valor del seg de publicidad, viene el valor de 20"
                                                $precio->setRepetidora($repetidora);
                                                $precio->setHorario($horario);

                                                $logger->debug("Guardando Precio {$precio}");
                                                $manager->persist($precio);

                                                if( $insert_count == 0 )
                                                {
                                                    $manager->flush();
                                                    $manager->clear('WOPPubliRadioBundle:Precio');
                                                    $insert_count = 20;
                                                }
                                                else --$insert_count;

                                                $precio = null;
                                                $horario = null;
                                            }

                                            $horarios = null;
                                            $manager->clear('WOPPubliRadioBundle:Horario');
                                        }
                                        else $logger->error("No se encontraron horarios para Emisora {$emisora} {$rangoDia}/{$rangoHora}");
                                    }
                                    else $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no contiene un precio valido");
                                }
                                else $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no contiene datos");
                            }
                            else $logger->warning("La CELDA {$worksheet->getTitle()}{$column}:{$row} no es una celda valida");
                        }
                        while($row <= $lastRow);
                    } // end else no emisora

                    $repetidora = null;
                    $manager->flush();
                    $manager->clear('WOPPubliRadioBundle:Repetidora');

                } // end for column
            } // end else no municipio
            $municipio = null;
            $manager->flush();
            $manager->clear();
        } // end foreach
    }

    private function getXLSXReader()
    {
        echo "  >> Reading xls file tarifas_local.xls \n";

        $input = __DIR__ . '/../data/xls/tarifas_local.xls';

        $reader = \PHPExcel_IOFactory::createReader( \PHPExcel_IOFactory::identify($input) );
        $reader->setReadDataOnly(true);
        return $reader->load($input);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 9; // the order in which fixtures will be loaded
    }
}
