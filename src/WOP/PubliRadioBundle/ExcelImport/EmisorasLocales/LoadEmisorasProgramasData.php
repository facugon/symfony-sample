<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use WOP\PubliRadioBundle\Entity\Emisora as Emisora;
use WOP\PubliRadioBundle\Entity\Programa as Programa;
//use WOP\PubliRadioBundle\Entity\Horario as Horario;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadEmisorasProgramasData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function getContainer() 
    {
        return $this->container ;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $reader = $this->getXLSXReader();

        $logger = $this->container->get('logger');
        $horarioManager = $this->container->get('horario_manager');
        $rangoHorarioManager = $this->container->get('rangohorario_manager');

        echo "  >> Processing data\n";
        foreach($reader->getWorksheetIterator() as $worksheet)
        {
            $emisoraNombre = str_replace('PARRILLA ', '', $worksheet->getTitle()) ;

            $logger->info("Creando Emisora {$emisoraNombre}");

            $emisora = new Emisora();
            $emisora->setNombre( $emisoraNombre );

            $manager->persist($emisora);

            $lastRow = $worksheet->getHighestDataRow();

            /**
             *
             * de la primer columna(A) obtengo rangos de hora para los programas
             * asignados a la fila. genero un array con la informacion indexado
             * por fila para obtener los rangos horarios de cada programa en
             * su creacion
             *
             */
            $column = 'A';
            $horas = array();
            for( $row=2; $row <= $lastRow; $row++ )
            {
                $horas[$row] = $worksheet->getCell($column.$row)->getValue();
            }

            /**
             *
             * a partir de la columna B comienza la info de los programas
             *
             */
            $lastColumn = $worksheet->getHighestDataColumn();
            ++$lastColumn;
            $logger->info("Ultima Columna '{$lastColumn}'");
            for($column='B'; $column!=$lastColumn; $column++)
            {
                /**
                 *
                 * la Primer fila contiene el rango de dias al cual aplica cada programa.
                 * construyo los horarios base que se usara en los distintos rangos de hora
                 * por cada dia. obtengo un array de horarios por dia, con indice x dia
                 *
                 */
                $row = 1;
                $rangoDia = $worksheet->getCell($column.$row)->getValue();

                if( ! empty($rangoDia) )
                {
                    $horariosBase = $horarioManager->buildInRange($rangoDia);
                    $logger->info("Columna '{$column}', rango dia '{$rangoDia}'");

                    /**
                     *
                     * a partir de la fila 2 se encuentra el nombre del programa, y tantas filas
                     * vacias como horas ocupa el programa hasta encontrar otro programa.
                     * \TODO verificar que pasa al llegar al final de la columna, cuando un programa ocupa las ultimas celdas quedan vacias hasta el final. el ultimo horario se encuentra definido por el rango de horas de la columna A
                     *
                     * recorro todas las celdas de la columna creando tantos programas encontrados
                     * sus horarios (1 horario x dia y hora) y asocio la emisora.
                     *
                     */
                    $programa = null ;
                    $programaNombre = '';

                    $insert_count=20;
                    for( $row=2; $row <= $lastRow; $row++ )
                    {
                        $data = $worksheet->getCell($column.$row)->getValue();
                        if( ! empty($data) && $data != $programaNombre )
                        {
                            $programaNombre = $data;

                            $programa = new Programa();
                            $programa->setNombre($programaNombre);
                            $programa->setEmisora($emisora);
                        }

                        /**
                         *
                         * obtengo las horas y los dias en los rangos definidos por la primer fila
                         * y la primer columna de la matriz.
                         *
                         * recorro los dos rangos. utiliza los horarios base para x dia
                         * para asignar los horarios.
                         * se agregan los horarios al programa y se persiste le programa
                         * para guardar los datos en la base.
                         *
                         */
                        $horasEnRango = $rangoHorarioManager->buildInRange($horas[$row]);
                        $diasEnRango  = $horarioManager->getNumberOfDaysByRange($rangoDia);
                        foreach( $diasEnRango as $dia )
                        {
                            foreach( $horasEnRango as $hora )
                            {
                                $horario = clone $horariosBase[$dia];
                                $horario->setHora($hora);
                                $horario->setPrograma($programa);

                                $programa->addHorario($horario);

                                $logger->info($horario);
                            }
                        }

                        $logger->info("Creando Programa {$programa}");
                        $manager->persist($programa);

                        if( $insert_count == 20 ) {
                            $manager->flush();
                            $manager->clear('WOPPubliRadioBundle:Horario');
                        }
                        else ++$insert_count;
                    }
                    $manager->clear('WOPPubliRadioBundle:Programa');
                }
            }
            $manager->clear('WOPPubliRadioBundle:Emisora');
        }
    }

    private function getXLSXReader()
    {
        echo "  >> Reading xls file emisoras_programas.xls\n";

        $input = __DIR__ . '/../data/xls/emisoras_programas_locales.xls';

        $reader = \PHPExcel_IOFactory::createReader( \PHPExcel_IOFactory::identify($input) );
        $reader->setReadDataOnly(true);
        return $reader->load($input);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8; // the order in which fixtures will be loaded
    }
}
