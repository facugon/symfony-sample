<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Municipio
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Municipio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="codigo", type="integer")
     */
    private $codigo;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=100)
     */
    private $nombre;

    /**
     * @var integer
     *
     * @ORM\Column(name="dc", type="integer")
     */
    private $dc;

    /**
     * @var integer
     *
     * @ORM\Column(name="provincia_id", type="integer")
     */
    private $provincia;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codigo
     *
     * @param integer $codigo
     * @return Municipio
     */
    public function setCodigo($codigo)
    {
        $this->codigo = $codigo;
    
        return $this;
    }

    /**
     * Get codigo
     *
     * @return integer 
     */
    public function getCodigo()
    {
        return $this->codigo;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Municipio
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set dc
     *
     * @param integer $dc
     * @return Municipio
     */
    public function setDc($dc)
    {
        $this->dc = $dc;
    
        return $this;
    }

    /**
     * Get dc
     *
     * @return integer 
     */
    public function getDc()
    {
        return $this->dc;
    }

    /**
     * Set Provincia
     *
     * @param WOP\PubliRadioBundle\Entity\Provincia $Provincia
     * @return Municipio
     */
    public function setProvincia(Provincia $provincia)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get Provincia
     *
     * @return WOP\PubliRadioBundle\Entity\Provincia
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Get string to display in the combo box
     *
     * return string
     */
    public function __toString()
    {
        return $this->nombre ;
    }
}