<?php

namespace WOP\PubliRadioBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping as ResultSetMapping;

class SolicitudRepository extends EntityRepository
{
    public function findAllByUserId($userid)
    {
        return $this->_em->createQuery('
            SELECT s FROM WOPPubliRadioBundle:Solicitud s
            WHERE s.userId = :userid
            GROUP BY s.queryHash
            ORDER BY s.datetime DESC
            ')
            ->setParameter('userid',$userid)
            ->getResult();
    }

    public function findAll()
    {
        return $this->_em->createQuery('
            SELECT s FROM WOPPubliRadioBundle:Solicitud s
            GROUP BY s.queryHash
            ORDER BY s.datetime DESC
            ')
            ->getResult();
    }

    public function findByHash($hash)
    {
        $return = $this->_em->createQuery('
            SELECT s AS campania, SUM(s.precioTotal) as preciototal, SUM(s.precioDescuento) as preciototalcondescuento, s.estado
            FROM WOPPubliRadioBundle:Solicitud s
            WHERE s.queryHash = :hash
            GROUP BY s.queryHash
            ORDER BY s.datetime ASC
            ')
            ->setParameter('hash', $hash) 
            ->getOneOrNullResult();

        return $return;
    }

    public function findAllItemsByHash($hash)
    {
        return $this->_em->createQuery('
            SELECT s FROM WOPPubliRadioBundle:Solicitud s
            WHERE s.queryHash = :hash
            ORDER BY s.datetime ASC
            ')
            ->setParameter('hash', $hash) 
            ->getResult();
    }

    public function getInfoEspaciosByHash($hash)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('WOP\PubliRadioBundle\Entity\Solicitud','s');
        $rsm->addFieldResult('s' , 'id'                , 'id'               ); 
        $rsm->addFieldResult('s' , 'zona'              , 'zona'             ); 
        $rsm->addFieldResult('s' , 'cantidad_anuncios' , 'cantidadAnuncios' ); 
        $rsm->addFieldResult('s' , 'duracion_segundos' , 'duracionSegundos' ); 
        $rsm->addFieldResult('s' , 'precio_total'      , 'precioTotal'      ); 
        $rsm->addFieldResult('s' , 'precio_descuento'  , 'precioDescuento'  ); 
        $rsm->addFieldResult('s' , 'programa_id'  , 'programaId'  ); 
        $rsm->addFieldResult('s' , 'horario_id'  , 'horarioId'  ); 
        $rsm->addFieldResult('s' , 'repetidora_id'  , 'repetidoraId'  ); 

        $campania = $this->_em->createNativeQuery("
            SELECT 
                s.id, s.zona, s.cantidad_anuncios, s.duracion_segundos,
                s.precio_total, s.precio_descuento,
                s.programa_id, s.horario_id, s.repetidora_id
            FROM Solicitud s
            WHERE s.query_hash = ? ", $rsm)
            ->setParameter(1, $hash)
            ->getResult();

        foreach( $campania as $item )
        {
            $horario = $this->_em
                ->getRepository('WOP\PubliRadioBundle\Entity\Horario')
                ->find( $item->getHorarioId() );
            $item->setHorario($horario);

            $programa = $this->_em
                ->getRepository('WOP\PubliRadioBundle\Entity\Programa')
                ->find( $item->getProgramaId() );
            $item->setPrograma($programa);

            $repetidora = $this->_em
                ->getRepository('WOP\PubliRadioBundle\Entity\Repetidora')
                ->find( $item->getRepetidoraId() );
            $item->setRepetidoraImagen($repetidora->getEmisora()->getWebPath());
        }

        return $campania;
    }
} 
