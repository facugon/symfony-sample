<?php

namespace WOP\PubliRadioBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 */
class MensajeriaRepository extends EntityRepository
{
    public function findByHash($hash)
    {
        return $this->createQueryBuilder('m')
            ->orderBy('m.datetime','asc')
            ->where('m.solicitudHash = ?1')
            ->getQuery()
            ->setParameter(1, $hash)
            ->getResult();
    }
}

