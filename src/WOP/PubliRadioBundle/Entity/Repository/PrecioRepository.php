<?php

namespace WOP\PubliRadioBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMapping as ResultSetMapping;
use Doctrine\ORM\Query as Query ;

/**
 * PrecioRepository
 */
class PrecioRepository extends EntityRepository
{
    public function findAllOrderedByRepetidora($idRepetidora)
    {
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('WOP\PubliRadioBundle\Entity\Precio','p');
        $rsm->addFieldResult('p','id','id');
        $rsm->addFieldResult('p','precio','valor');
        $rsm->addJoinedEntityResult('WOP\PubliRadioBundle\Entity\Horario','h','p','horario');
        $rsm->addFieldResult('h','horario_id','id');
        $rsm->addJoinedEntityResult('WOP\PubliRadioBundle\Entity\Repetidora','r','p','repetidora');
        $rsm->addFieldResult('r','repetidora_id','id');
        $rsm->addFieldResult('r','repetidora','nombre');
        $rsm->addJoinedEntityResult('WOP\PubliRadioBundle\Entity\Programa','pr','h','programa');
        $rsm->addFieldResult('pr','programa_id','id');
        $rsm->addFieldResult('pr','programa','nombre');
        $rsm->addJoinedEntityResult('WOP\PubliRadioBundle\Entity\Hora','hs','h','hora');
        $rsm->addFieldResult('hs','hora_id','id');
        $rsm->addFieldResult('hs','hora','hora');
        $rsm->addJoinedEntityResult('WOP\PubliRadioBundle\Entity\Dia','d','h','dia');
        $rsm->addFieldResult('d','dia_id','id');
        $rsm->addFieldResult('d','nombre_dia','nombreDia');
        $rsm->addFieldResult('d','numero_dia','numeroDia');

        return $this->_em->createNativeQuery("
            SELECT 
                p.id                  , p.precio               , p.horario_id , p.repetidora_id , 
                r.id as repetidora_id , r.nombre as repetidora , 
                h.id horario_id       , h.dia_id               , h.hora_id    , h.programa_id   , 
                pr.id as programa_id  , pr.nombre as programa  , 
                hs.id as hora_id      , hs.hora                , 
                d.id as dias_id       , d.nombre_dia

            FROM Precio p
            INNER JOIN Repetidora r ON r.id  = p.repetidora_id
            INNER JOIN Horario h    ON h.id  = p.horario_id
            INNER JOIN Programa pr  ON pr.id = h.programa_id
            INNER JOIN Hora hs      ON hs.id = h.hora_id
            INNER JOIN Dia d        ON d.id  = h.dia_id
            WHERE p.repetidora_id = ?
            ORDER BY p.horario_id ASC, p.repetidora_id ASC
            ", $rsm)
            ->setParameter(1,$idRepetidora)
            ->getResult();
    }
}
