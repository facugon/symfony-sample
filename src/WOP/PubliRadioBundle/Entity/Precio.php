<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Precio
 *
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(name="precio_unique", columns={"horario_id", "repetidora_id"})})
 * @ORM\Entity()
 * @ORM\Entity(repositoryClass="WOP\PubliRadioBundle\Entity\Repository\PrecioRepository")
 */
class Precio
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Horario
     *
     * @ORM\ManyToOne(targetEntity="Horario", inversedBy="precios")
     * @ORM\JoinColumn(name="horario_id", referencedColumnName="id", nullable=false)
     */
    private $horario;

    /**
     * @var Repetidora
     *
     * @ORM\ManyToOne(targetEntity="Repetidora", inversedBy="precios")
     * @ORM\JoinColumn(name="repetidora_id", referencedColumnName="id", nullable=false)
     */
    private $repetidora;

    /**
     * @var decimal
     *
     * @ORM\Column(name="precio",type="decimal", precision=10, scale=2, nullable=false)
     */
    private $valor;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set valor
     *
     * @param float $valor
     * @return Precio
     */
    public function setValor($valor)
    {
        $this->valor = $valor;
    
        return $this;
    }

    /**
     * Get valor
     *
     * @return float 
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * Set horario
     *
     * @param \WOP\PubliRadioBundle\Entity\Horario $horario
     * @return Precio
     */
    public function setHorario(\WOP\PubliRadioBundle\Entity\Horario $horario = null)
    {
        $this->horario = $horario;
    
        return $this;
    }

    /**
     * Get horario
     *
     * @return \WOP\PubliRadioBundle\Entity\Horario 
     */
    public function getHorario()
    {
        return $this->horario;
    }

    /**
     * Set repetidora
     *
     * @param \WOP\PubliRadioBundle\Entity\Repetidora $repetidora
     * @return Precio
     */
    public function setRepetidora(\WOP\PubliRadioBundle\Entity\Repetidora $repetidora)
    {
        $this->repetidora = $repetidora;
    
        return $this;
    }

    /**
     * Get repetidora
     *
     * @return \WOP\PubliRadioBundle\Entity\Repetidora 
     */
    public function getRepetidora()
    {
        return $this->repetidora;
    }

    public function __toString()
    {
        return "{$this->valor}" ;
    }
}
