<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Programa
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="WOP\PubliRadioBundle\Entity\Repository\ProgramaRepository")
 */
class Programa
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var Emisora
     *
     * @ORM\ManyToOne(targetEntity="Emisora", inversedBy="programas")
     * @ORM\JoinColumn(name="emisora_id", referencedColumnName="id", nullable=false)
     */
    private $emisora;

    /**
     * @var WOP\PubliRadioBundle\Entity\Horario
     *
     * @ORM\OneToMany(targetEntity="Horario", mappedBy="programa", cascade={"persist"})
     */
    private $horarios;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->horarios = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Programa
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set emisora
     *
     * @param \WOP\PubliRadioBundle\Entity\Emisora $emisora
     * @return Programa
     */
    public function setEmisora(\WOP\PubliRadioBundle\Entity\Emisora $emisora)
    {
        $this->emisora = $emisora;
    
        return $this;
    }

    /**
     * Get emisora
     *
     * @return \WOP\PubliRadioBundle\Entity\Emisora 
     */
    public function getEmisora()
    {
        return $this->emisora;
    }

    /**
     * Add horarios
     *
     * @param \WOP\PubliRadioBundle\Entity\Horario $horarios
     * @return Programa
     */
    public function addHorario(\WOP\PubliRadioBundle\Entity\Horario $horarios)
    {
        $this->horarios[] = $horarios;
    
        return $this;
    }

    /**
     * Remove horarios
     *
     * @param \WOP\PubliRadioBundle\Entity\Horario $horarios
     */
    public function removeHorario(\WOP\PubliRadioBundle\Entity\Horario $horarios)
    {
        $this->horarios->removeElement($horarios);
    }

    /**
     * Get horarios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getHorarios()
    {
        return $this->horarios;
    }

    public function __toString(){
        return $this->nombre ;
    }
}
