<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Mensajeria
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="WOP\PubliRadioBundle\Entity\Repository\MensajeriaRepository")
 */
class Mensajeria
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mensaje", type="text")
     */
    private $mensaje;

    /**
     * @var string
     *
     * @ORM\Column(name="solicitud_hash", type="string", length=255)
     */
    private $solicitudHash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @ORM\ManyToOne(targetEntity="IAR\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mensaje
     *
     * @param string $mensaje
     * @return Mensajeria
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    
        return $this;
    }

    /**
     * Get mensaje
     *
     * @return string 
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * Set solicitudHash
     *
     * @param string $solicitudHash
     * @return Mensajeria
     */
    public function setSolicitudHash($solicitudHash)
    {
        $this->solicitudHash = $solicitudHash;
    
        return $this;
    }

    /**
     * Get solicitudHash
     *
     * @return string 
     */
    public function getSolicitudHash()
    {
        return $this->solicitudHash;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Mensajeria
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * Set user
     *
     * @param \IAR\UserBundle\Entity\User $user
     * @return Mensajeria
     */
    public function setUser(\IAR\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \WOP\PubliRadioBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
