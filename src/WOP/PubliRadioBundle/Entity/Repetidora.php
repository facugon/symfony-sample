<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Repetidora
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Repetidora
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var boolean
     *
     * @ORM\Column(name="alcance_nacional", type="boolean", nullable=true)
     */
    private $nacional=false;

    /**
     * @var Comunidad
     *
     * @ORM\ManyToOne(targetEntity="Comunidad")
     * @ORM\JoinColumn(name="comunidad_id", referencedColumnName="id", nullable=true)
     */
    private $comunidad=null;

    /**
     * @var Provincia
     *
     * @ORM\ManyToOne(targetEntity="Provincia")
     * @ORM\JoinColumn(name="provincia_id", referencedColumnName="id", nullable=true)
     */
    private $provincia=null;

    /**
     * @var Municipio
     *
     * @ORM\ManyToOne(targetEntity="Municipio")
     * @ORM\JoinColumn(name="municipio_id", referencedColumnName="id", nullable=true)
     */
    private $municipio=null;

    /**
     * @var Emisora
     *
     * @ORM\ManyToOne(targetEntity="Emisora", inversedBy="repetidoras")
     * @ORM\JoinColumn(name="emisora_id", referencedColumnName="id", nullable=false)
     */
    private $emisora;

    /**
     * @var Recargo
     *
     * @ORM\ManyToOne(targetEntity="Recargo")
     * @ORM\JoinColumn(name="recargo_id", referencedColumnName="id")
     */
    private $recargo;

    /**
     * @var WOP\PubliRadioBundle\Entity\Precio
     *
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="repetidora")
     */
    private $precios;

    /**
     * @var string
     *
     * @ORM\Column(name="tipo_modulacion",type="string",length=2, nullable=true)
     */
    private $tipoModulacion;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->precios = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Repetidora
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set nacional
     *
     * @param boolean $nacional
     * @return Repetidora
     */
    public function setNacional($nacional)
    {
        $this->nacional = $nacional;
    
        return $this;
    }

    /**
     * Get nacional
     *
     * @return boolean 
     */
    public function getNacional()
    {
        return $this->nacional;
    }

    /**
     * Set provincia
     *
     * @param \WOP\PubliRadioBundle\Entity\Provincia $provincia
     * @return Repetidora
     */
    public function setProvincia(\WOP\PubliRadioBundle\Entity\Provincia $provincia = null)
    {
        $this->provincia = $provincia;
    
        return $this;
    }

    /**
     * Get provincia
     *
     * @return \WOP\PubliRadioBundle\Entity\Provincia 
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set comunidad
     *
     * @param \WOP\PubliRadioBundle\Entity\Comunidad $comunidad
     * @return Repetidora
     */
    public function setComunidad(\WOP\PubliRadioBundle\Entity\Comunidad $comunidad = null)
    {
        $this->comunidad = $comunidad;
    
        return $this;
    }

    /**
     * Get comunidad
     *
     * @return \WOP\PubliRadioBundle\Entity\Comunidad 
     */
    public function getComunidad()
    {
        return $this->comunidad;
    }

    /**
     * Set municipio
     *
     * @param \WOP\PubliRadioBundle\Entity\Municipio $municipio
     * @return Repetidora
     */
    public function setMunicipio(\WOP\PubliRadioBundle\Entity\Municipio $municipio = null)
    {
        $this->municipio = $municipio;
    
        return $this;
    }

    /**
     * Get municipio
     *
     * @return \WOP\PubliRadioBundle\Entity\Municipio 
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set emisora
     *
     * @param \WOP\PubliRadioBundle\Entity\Emisora $emisora
     * @return Repetidora
     */
    public function setEmisora(\WOP\PubliRadioBundle\Entity\Emisora $emisora)
    {
        $this->emisora = $emisora;
    
        return $this;
    }

    /**
     * Get emisora
     *
     * @return \WOP\PubliRadioBundle\Entity\Emisora 
     */
    public function getEmisora()
    {
        return $this->emisora;
    }

    /**
     * Set recargo
     *
     * @param \WOP\PubliRadioBundle\Entity\Recargo $recargo
     * @return Repetidora
     */
    public function setRecargo(\WOP\PubliRadioBundle\Entity\Recargo $recargo = null)
    {
        $this->recargo = $recargo;
    
        return $this;
    }

    /**
     * Get recargo
     *
     * @return \WOP\PubliRadioBundle\Entity\Recargo 
     */
    public function getRecargo()
    {
        return $this->recargo;
    }

    /**
     * Add precios
     *
     * @param \WOP\PubliRadioBundle\Entity\Precio $precios
     * @return Repetidora
     */
    public function addPrecio(\WOP\PubliRadioBundle\Entity\Precio $precios)
    {
        $this->precios[] = $precios;
    
        return $this;
    }

    /**
     * Remove precios
     *
     * @param \WOP\PubliRadioBundle\Entity\Precio $precios
     */
    public function removePrecio(\WOP\PubliRadioBundle\Entity\Precio $precios)
    {
        $this->precios->removeElement($precios);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios()
    {
        return $this->precios;
    }

    /**
     * Set tipoModulacion
     *
     * @param string $tipoModulacion
     * @return Repetidora
     */
    public function setTipoModulacion($tipoModulacion)
    {
        $this->tipoModulacion = $tipoModulacion;
    
        return $this;
    }

    /**
     * Get tipoModulacion
     *
     * @return string 
     */
    public function getTipoModulacion()
    {
        return $this->tipoModulacion;
    }

    public function __toString()
    {
        return "{$this->nombre}";
    }
}
