<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dia
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Dia
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="numero_dia", type="integer")
     */
    private $numeroDia;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre_dia", type="string", length=10)
     */
    private $nombreDia;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numeroDia
     *
     * @param integer $numeroDia
     * @return Dia
     */
    public function setNumeroDia($numeroDia)
    {
        $this->numeroDia = $numeroDia;
    
        return $this;
    }

    /**
     * Get numeroDia
     *
     * @return integer 
     */
    public function getNumeroDia()
    {
        return $this->numeroDia;
    }

    /**
     * Set nombreDia
     *
     * @param string $nombreDia
     * @return Dia
     */
    public function setNombreDia($nombreDia)
    {
        $this->nombreDia = $nombreDia;
    
        return $this;
    }

    /**
     * Get nombreDia
     *
     * @return string 
     */
    public function getNombreDia()
    {
        return $this->nombreDia;
    }

    public function __toString()
    {
        return "{$this->nombreDia}";
    }

    /**
     * Set id
     *
     * @param integer $id
     * @return Dia
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }
}
