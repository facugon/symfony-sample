<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Recargo
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Recargo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="descripcion", type="string", length=50)
     */
    private $descripcion;

    /**
     * @var float
     *
     * @ORM\Column(name="porcentaje", type="decimal")
     */
    private $porcentaje;

    /**
     * @var integer
     *
     * @ORM\Column(name="segundos_duracion", type="integer")
     */
    private $segundosDuracion;

    /**
     * @var string
     *
     * @ORM\Column(name="regla", type="string", length=20)
     */
    private $regla;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set descripcion
     *
     * @param string $descripcion
     * @return Recargo
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    
        return $this;
    }

    /**
     * Get descripcion
     *
     * @return string 
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * Set porcentaje
     *
     * @param float $porcentaje
     * @return Recargo
     */
    public function setPorcentaje($porcentaje)
    {
        $this->porcentaje = $porcentaje;
    
        return $this;
    }

    /**
     * Get porcentaje
     *
     * @return float 
     */
    public function getPorcentaje()
    {
        return $this->porcentaje;
    }

    /**
     * Set segundosDuracion
     *
     * @param integer $segundosDuracion
     * @return Recargo
     */
    public function setSegundosDuracion($segundosDuracion)
    {
        $this->segundosDuracion = $segundosDuracion;
    
        return $this;
    }

    /**
     * Get segundosDuracion
     *
     * @return integer 
     */
    public function getSegundosDuracion()
    {
        return $this->segundosDuracion;
    }

    /**
     * Set regla
     *
     * @param string $regla
     * @return Recargo
     */
    public function setRegla($regla)
    {
        $this->regla = $regla;
    
        return $this;
    }

    /**
     * Get regla
     *
     * @return string 
     */
    public function getRegla()
    {
        return $this->regla;
    }

    public function __toString()
    {
        return $this->descripcion ;
    }
}
