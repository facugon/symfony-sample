<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Solicitud
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="WOP\PubliRadioBundle\Entity\Repository\SolicitudRepository")
 */
class Solicitud
{
    const SinPagar              = 1 ;
    const Paga                  = 2 ;
    const EsperandoConfirmacion = 3 ;
    const Confirmada            = 4 ;

    static public $Estados = array(
        1 => 'Sin Pagar',
        2 => 'Pagada',
        3 => 'Esperando Confirmación',
        4 => 'Confirmada',
    );

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="zona", type="string", length=255)
     */
    private $zona;

    /**
     * @var integer
     * @ORM\Column(name="user_id", type="integer")
     */
    private $userId;

    /**
     * @var integer
     * @ORM\Column(name="lugar_id", type="integer")
     */
    private $lugarId;

    /**
     * @var string
     * @ORM\Column(name="lugar_nombre", type="string", length=255)
     */
    private $lugarNombre;

    /**
     * @var integer
     * @ORM\Column(name="repetidora_id", type="integer")
     */
    private $repetidoraId;

    /**
     * @var string
     * @ORM\Column(name="repetidora_nombre", type="string", length=255)
     */
    private $repetidoraNombre;

    /**
     * @var integer
     * @ORM\Column(name="programa_id", type="integer")
     */
    private $programaId;

    /**
     * @var string
     * @ORM\Column(name="programa_nombre", type="string", length=255)
     */
    private $programaNombre;

    /**
     * @var integer
     * @ORM\Column(name="horario_id", type="integer")
     */
    private $horarioId;

    /**
     * @var integer
     * @ORM\Column(name="cantidad_anuncios", type="integer")
     */
    private $cantidadAnuncios;

    /**
     * El precio para esta duracion calculado
     * @var integer
     * @ORM\Column(name="duracion_segundos", type="integer")
     */
    private $duracionSegundos;

    /**
     * @var integer
     * @ORM\Column(name="precio_id", type="integer")
     */
    private $precioId;

    /**
     * DuracionSegundos x PrecioDuracion
     * @var float
     * @ORM\Column(name="precio_total", type="float")
     */
    private $precioTotal;

    /**
     * @var float
     * @ORM\Column(name="precio_descuento", type="float")
     */
    private $precioDescuento;

    /**
     * Precio para la cantidad de segundos de duracion del anuncios
     * @var float
     * @ORM\Column(name="precio_duracion", type="float")
     */
    private $precioDuracion;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * @var string
     * @ORM\Column(name="query_hash", type="string", length=255)
     */
    private $queryHash;


    /**
     * @var integer
     * @ORM\Column(name="estado", type="integer")
     */
    private $estado;

    private $programa;
    private $horario;
    private $repetidoraimagen;
    private $lugar;

    public function setPrograma($programa){
        $this->programa = $programa;
        return $this;
    }

    public function getPrograma(){
        return $this->programa;
    }

    public function setHorario($horario){
        $this->horario = $horario;
        return $this;
    }

    public function getHorario() {
        return $this->horario;
    }

    public function setRepetidoraImagen($img){
        $this->repetidoraimgen = $img;
        return $this;
    }

    public function getRepetidoraImagen(){
        return $this->repetidoraimgen;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set zona
     *
     * @param string $zona
     * @return Solicitud
     */
    public function setZona($zona)
    {
        $this->zona = $zona;
    
        return $this;
    }

    /**
     * Get zona
     *
     * @return string 
     */
    public function getZona()
    {
        return $this->zona;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     * @return Solicitud
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    
        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set lugarId
     *
     * @param integer $lugarId
     * @return Solicitud
     */
    public function setLugarId($lugarId)
    {
        $this->lugarId = $lugarId;
    
        return $this;
    }

    /**
     * Get lugarId
     *
     * @return integer 
     */
    public function getLugarId()
    {
        return $this->lugarId;
    }

    /**
     * Set lugarNombre
     *
     * @param string $lugarNombre
     * @return Solicitud
     */
    public function setLugarNombre($lugarNombre)
    {
        $this->lugarNombre = $lugarNombre;
    
        return $this;
    }

    /**
     * Get lugarNombre
     *
     * @return string 
     */
    public function getLugarNombre()
    {
        return $this->lugarNombre;
    }

    /**
     * Set repetidoraId
     *
     * @param integer $repetidoraId
     * @return Solicitud
     */
    public function setRepetidoraId($repetidoraId)
    {
        $this->repetidoraId = $repetidoraId;
    
        return $this;
    }

    /**
     * Get repetidoraId
     *
     * @return integer 
     */
    public function getRepetidoraId()
    {
        return $this->repetidoraId;
    }

    /**
     * Set repetidoraNombre
     *
     * @param string $repetidoraNombre
     * @return Solicitud
     */
    public function setRepetidoraNombre($repetidoraNombre)
    {
        $this->repetidoraNombre = $repetidoraNombre;
    
        return $this;
    }

    /**
     * Get repetidoraNombre
     *
     * @return string 
     */
    public function getRepetidoraNombre()
    {
        return $this->repetidoraNombre;
    }

    /**
     * Set programaId
     *
     * @param integer $programaId
     * @return Solicitud
     */
    public function setProgramaId($programaId)
    {
        $this->programaId = $programaId;
    
        return $this;
    }

    /**
     * Get programaId
     *
     * @return integer 
     */
    public function getProgramaId()
    {
        return $this->programaId;
    }

    /**
     * Set programaNombre
     *
     * @param string $programaNombre
     * @return Solicitud
     */
    public function setProgramaNombre($programaNombre)
    {
        $this->programaNombre = $programaNombre;
    
        return $this;
    }

    /**
     * Get programaNombre
     *
     * @return string 
     */
    public function getProgramaNombre()
    {
        return $this->programaNombre;
    }

    /**
     * Set horarioId
     *
     * @param integer $horarioId
     * @return Solicitud
     */
    public function setHorarioId($horarioId)
    {
        $this->horarioId = $horarioId;
    
        return $this;
    }

    /**
     * Get horarioId
     *
     * @return integer 
     */
    public function getHorarioId()
    {
        return $this->horarioId;
    }

    /**
     * Set cantidadAnuncios
     *
     * @param integer $cantidadAnuncios
     * @return Solicitud
     */
    public function setCantidadAnuncios($cantidadAnuncios)
    {
        $this->cantidadAnuncios = $cantidadAnuncios;
    
        return $this;
    }

    /**
     * Get cantidadAnuncios
     *
     * @return integer 
     */
    public function getCantidadAnuncios()
    {
        return $this->cantidadAnuncios;
    }

    /**
     * Set duracionSegundos
     *
     * @param integer $duracionSegundos
     * @return Solicitud
     */
    public function setDuracionSegundos($duracionSegundos)
    {
        $this->duracionSegundos = $duracionSegundos;
    
        return $this;
    }

    /**
     * Get duracionSegundos
     *
     * @return integer 
     */
    public function getDuracionSegundos()
    {
        return $this->duracionSegundos;
    }

    /**
     * Set precioTotal
     *
     * @param string $precioTotal
     * @return Solicitud
     */
    public function setPrecioTotal($precioTotal)
    {
        $this->precioTotal = $precioTotal;
    
        return $this;
    }

    /**
     * Get precioTotal
     *
     * @return string 
     */
    public function getPrecioTotal()
    {
        return $this->precioTotal;
    }

    public function aplicarDescuento(\WOP\PubliRadioBundle\Entity\Descuento $descuento)
    {
        $this->precioDescuento = $this->precioTotal * ( $descuento->getDescuento() / 100 ) ;
        return $this;
    }

    /**
     * Set precioDescuento
     *
     * @param string $precioDescuento
     * @return Solicitud
     */
    public function setPrecioDescuento($precioDescuento)
    {
        $this->precioDescuento = $precioDescuento;
    
        return $this;
    }

    /**
     * Get precioDescuento
     *
     * @return string 
     */
    public function getPrecioDescuento()
    {
        return $this->precioDescuento;
    }

    /**
     * Set precioDuracion
     *
     * @param string $precioDuracion
     * @return Solicitud
     */
    public function setPrecioDuracion($precioDuracion)
    {
        $this->precioDuracion = $precioDuracion;
    
        return $this;
    }

    /**
     * Get precioDuracion
     *
     * @return string 
     */
    public function getPrecioDuracion()
    {
        return $this->precioDuracion;
    }

    /**
     * Set datetime
     *
     * @param \DateTime $datetime
     * @return Solicitud
     */
    public function setDateTime($datetime)
    {
        $this->datetime = $datetime;
    
        return $this;
    }

    /**
     * Get datetime
     *
     * @return \DateTime 
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

    /**
     * Set precioId
     *
     * @param integer $precioId
     * @return Solicitud
     */
    public function setPrecioId($precioId)
    {
        $this->precioId = $precioId;
    
        return $this;
    }

    /**
     * Get precioId
     *
     * @return integer 
     */
    public function getPrecioId()
    {
        return $this->precioId;
    }

    /**
     * Set queryHash
     *
     * @param string $queryHash
     * @return Solicitud
     */
    public function setQueryHash($queryHash)
    {
        $this->queryHash = $queryHash;
    
        return $this;
    }

    /**
     * Get queryHash
     *
     * @return string 
     */
    public function getQueryHash()
    {
        return $this->queryHash;
    }

    public function setEstado($estado)
    {
        $this->estado = $estado;
    
        return $this;
    }

    public function getEstado()
    {
        return $this->estado ;
    }

    public function getEstadoText($id)
    {
        return self::$Estados[$id];
    }
}
