<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Descuento
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Descuento
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="rango_inicial", type="bigint")
     */
    private $rangoInicial;

    /**
     * @var integer
     *
     * @ORM\Column(name="rango_final", type="bigint")
     */
    private $rangoFinal;

    /**
     * @var string
     *
     * @ORM\Column(name="descuento", type="integer")
     */
    private $descuento;


    /**
     * Set id
     *
     * @param integer $id
     * @return Descuento
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rangoInicio
     *
     * @param integer $rangoInicio
     * @return Descuento
     */
    public function setRangoInicial($rangoInicial)
    {
        $this->rangoInicial = $rangoInicial;
    
        return $this;
    }

    /**
     * Get rangoInicio
     *
     * @return integer 
     */
    public function getRangoInicial()
    {
        return $this->rangoInicial;
    }

    /**
     * Set rangoFinal
     *
     * @param integer $rangoFinal
     * @return Descuento
     */
    public function setRangoFinal($rangoFinal)
    {
        $this->rangoFinal = $rangoFinal;
    
        return $this;
    }

    /**
     * Get rangoFinal
     *
     * @return integer 
     */
    public function getRangoFinal()
    {
        return $this->rangoFinal;
    }

    /**
     * Set descuento
     *
     * @param string $descuento
     * @return Descuento
     */
    public function setDescuento($descuento)
    {
        $this->descuento = $descuento;
    
        return $this;
    }

    /**
     * Get descuento
     *
     * @return string 
     */
    public function getDescuento()
    {
        return $this->descuento;
    }

    public function __toString()
    {
        return "{$this->rangoInicial} {$this->rangoFinal} : {$this->descuento}";
    }
}
