<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facturacion
 *
 * @ORM\Table(indexes={@ORM\Index(name="campania_hash_idx", columns={"campania_hash"})})
 * @ORM\Entity
 */
class Facturacion
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="campania_hash", type="string", length=255)
     */
    private $campaniaHash;

    /**
     * @var string
     * @ORM\Column(name="razon_social", type="string", length=255)
     */
    private $razonSocial='';

    /**
     * @var string
     * @ORM\Column(name="cif", type="string", length=255)
     */
    private $cif='';

    /**
     * @var string
     * @ORM\Column(name="direccion", type="string", length=255)
     */
    private $direccion='';

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_inicio_campania", type="datetime")
     */
    private $fechaInicioCampania;

    /**
     * @var \DateTime
     * @ORM\Column(name="fecha_fin_campania", type="datetime")
     */
    private $fechaFinCampania;

    /**
     * @var string
     * @ORM\Column(name="comentarios", type="text")
     */
    private $comentarios='';

    /**
     * @var boolean
     * @ORM\Column(name="preferencia_dia_semana", type="boolean")
     */
    private $preferenciaDiaSemana=false;

    /**
     * @var \DateTime
     * @ORM\Column(name="timestamp", type="datetime")
     */
    private $timestamp;

    public function __construct()
    {
        $this->fechaFinCampania = new \DateTime();
        $this->fechaInicioCampania = new \DateTime();
        $this->timestamp = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set razonSocial
     *
     * @param string $razonSocial
     * @return Facturacion
     */
    public function setRazonSocial($razonSocial)
    {
        $this->razonSocial = $razonSocial;
    
        return $this;
    }

    /**
     * Get razonSocial
     *
     * @return string 
     */
    public function getRazonSocial()
    {
        return $this->razonSocial;
    }

    /**
     * Set cif
     *
     * @param string $cif
     * @return Facturacion
     */
    public function setCif($cif)
    {
        $this->cif = $cif;
    
        return $this;
    }

    /**
     * Get cif
     *
     * @return string 
     */
    public function getCif()
    {
        return $this->cif;
    }

    /**
     * Set direccion
     *
     * @param string $direccion
     * @return Facturacion
     */
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    
        return $this;
    }

    /**
     * Get direccion
     *
     * @return string 
     */
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set fechaInicioCampania
     *
     * @param \DateTime $fechaInicioCampania
     * @return Facturacion
     */
    public function setFechaInicioCampania($fechaInicioCampania)
    {
        $this->fechaInicioCampania = $fechaInicioCampania;
    
        return $this;
    }

    /**
     * Get fechaInicioCampania
     *
     * @return \DateTime 
     */
    public function getFechaInicioCampania()
    {
        return $this->fechaInicioCampania;
    }

    /**
     * Set fechaFinCampania
     *
     * @param \DateTime $fechaFinCampania
     * @return Facturacion
     */
    public function setFechaFinCampania($fechaFinCampania)
    {
        $this->fechaFinCampania = $fechaFinCampania;
    
        return $this;
    }

    /**
     * Get fechaFinCampania
     *
     * @return \DateTime 
     */
    public function getFechaFinCampania()
    {
        return $this->fechaFinCampania;
    }

    /**
     * Set comentarios
     *
     * @param string $comentarios
     * @return Facturacion
     */
    public function setComentarios($comentarios)
    {
        $this->comentarios = $comentarios;
    
        return $this;
    }

    /**
     * Get comentarios
     *
     * @return string 
     */
    public function getComentarios()
    {
        return $this->comentarios;
    }

    /**
     * Set preferenciaDiaSemana
     *
     * @param boolean $preferenciaDiaSemana
     * @return Facturacion
     */
    public function setPreferenciaDiaSemana($preferenciaDiaSemana)
    {
        $this->preferenciaDiaSemana = $preferenciaDiaSemana;
    
        return $this;
    }

    /**
     * Get preferenciaDiaSemana
     *
     * @return boolean 
     */
    public function getPreferenciaDiaSemana()
    {
        return $this->preferenciaDiaSemana;
    }

    /**
     * Set timestamp
     *
     * @param \DateTime $timestamp
     * @return Facturacion
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    
        return $this;
    }

    /**
     * Get timestamp
     *
     * @return \DateTime 
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * Set campaniaHash
     *
     * @param string $campaniaHash
     * @return Facturacion
     */
    public function setCampaniaHash($campaniaHash)
    {
        $this->campaniaHash = $campaniaHash;
    
        return $this;
    }

    /**
     * Get campaniaHash
     *
     * @return string 
     */
    public function getCampaniaHash()
    {
        return $this->campaniaHash;
    }

    public function isCompleted()
    {
        return ! empty($this->razonSocial) 
            && ! empty($this->cif) 
            && ! empty($this->direccion )
            ;
    }
}
