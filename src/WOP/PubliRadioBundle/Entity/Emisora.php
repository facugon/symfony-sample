<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Emisora
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Emisora
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=50)
     */
    private $nombre;

    /**
     * @var WOP\PubliRadioBundle\Entity\Programa
     *
     * @ORM\OneToMany(targetEntity="Programa", mappedBy="emisora")
     */
    private $programas;

    /**
     * @var WOP\PubliRadioBundle\Entity\Repetidora
     *
     * @ORM\OneToMany(targetEntity="Repetidora", mappedBy="emisora")
     */
    private $repetidoras;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @Assert\Image(maxSize="6000000")
     */
    private $file;

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir() . '/' . $this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir() . '/' . $this->path;
    }

    protected function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/' . $this->getUploadDir();
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return '/uploads/emisoras';
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->path = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->file = null;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->programas = new \Doctrine\Common\Collections\ArrayCollection();
        $this->repetidoras = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     * @return Emisora
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    
        return $this;
    }

    /**
     * Get nombre
     *
     * @return string 
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add programas
     *
     * @param \WOP\PubliRadioBundle\Entity\Programa $programas
     * @return Emisora
     */
    public function addPrograma(\WOP\PubliRadioBundle\Entity\Programa $programas)
    {
        $this->programas[] = $programas;
    
        return $this;
    }

    /**
     * Remove programas
     *
     * @param \WOP\PubliRadioBundle\Entity\Programa $programas
     */
    public function removePrograma(\WOP\PubliRadioBundle\Entity\Programa $programas)
    {
        $this->programas->removeElement($programas);
    }

    /**
     * Get programas
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProgramas()
    {
        return $this->programas;
    }

    /**
     * Add repetidoras
     *
     * @param \WOP\PubliRadioBundle\Entity\Repetidora $repetidoras
     * @return Emisora
     */
    public function addRepetidora(\WOP\PubliRadioBundle\Entity\Repetidora $repetidoras)
    {
        $this->repetidoras[] = $repetidoras;
    
        return $this;
    }

    /**
     * Remove repetidoras
     *
     * @param \WOP\PubliRadioBundle\Entity\Repetidora $repetidoras
     */
    public function removeRepetidora(\WOP\PubliRadioBundle\Entity\Repetidora $repetidoras)
    {
        $this->repetidoras->removeElement($repetidoras);
    }

    /**
     * Get repetidoras
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRepetidoras()
    {
        return $this->repetidoras;
    }

    public function __toString(){
        return $this->nombre ;
    }
}
