<?php

namespace WOP\PubliRadioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Horario
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="WOP\PubliRadioBundle\Entity\Repository\HorarioRepository")
 */
class Horario
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \WOP\PubliRadioBundle\Entity\Programa
     *
     * @ORM\ManyToOne(targetEntity="Programa", inversedBy="horarios")
     * @ORM\JoinColumn(name="programa_id", referencedColumnName="id", nullable=false)
     */
    private $programa;

    /**
     * @var Hora
     *
     * @ORM\ManyToOne(targetEntity="Hora")
     * @ORM\JoinColumn(name="hora_id",referencedColumnName="id", nullable=false)
     */
    private $hora;

    /**
     * @var Dia
     *
     * @ORM\ManyToOne(targetEntity="Dia")
     * @ORM\JoinColumn(name="dia_id",referencedColumnName="id", nullable=false)
     */
    private $dia;

    /**
     * @var Precios
     *
     * @ORM\OneToMany(targetEntity="Precio", mappedBy="horario")
     */
    private $precios;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set programa
     *
     * @param \WOP\PubliRadioBundle\Entity\Programa $programa
     * @return Horario
     */
    public function setPrograma(\WOP\PubliRadioBundle\Entity\Programa $programa)
    {
        $this->programa = $programa;
    
        return $this;
    }

    /**
     * Get programa
     *
     * @return \WOP\PubliRadioBundle\Entity\Programa 
     */
    public function getPrograma()
    {
        return $this->programa;
    }

    /**
     * Set hora
     *
     * @param \WOP\PubliRadioBundle\Entity\Hora $hora
     * @return Horario
     */
    public function setHora(\WOP\PubliRadioBundle\Entity\Hora $hora = null)
    {
        $this->hora = $hora;

        return $this;
    }

    /**
     * Get hora
     *
     * @return \WOP\PubliRadioBundle\Entity\Hora 
     */
    public function getHora()
    {
        return $this->hora;
    }

    /**
     * Set dia
     *
     * @param \WOP\PubliRadioBundle\Entity\Dia $dia
     * @return Horario
     */
    public function setDia(\WOP\PubliRadioBundle\Entity\Dia $dia)
    {
        $this->dia = $dia;
    
        return $this;
    }

    /**
     * Get dia
     *
     * @return \WOP\PubliRadioBundle\Entity\Dia 
     */
    public function getDia()
    {
        return $this->dia;
    }

    public function __toString()
    {
        return "{$this->dia} {$this->hora}" ;
    }

    public function __clone()
    {
        $this->id = null ;
    }

    public function getHoraFin()
    {
        $hora = new \DateTime($this->hora);

        return $hora->add( new \DateInterval('PT1H') );
    }

    /**
     * Add precio
     *
     * @param \WOP\PubliRadioBundle\Entity\Precio $precio
     * @return Horario
     */
    public function addPrecio(\WOP\PubliRadioBundle\Entity\Precio $precio)
    {
        $this->precios[] = $precio;
    
        return $this;
    }

    /**
     * Remove precio
     *
     * @param \WOP\PubliRadioBundle\Entity\Precio $precio
     */
    public function removePrecio(\WOP\PubliRadioBundle\Entity\Precio $precio)
    {
        $this->precios->removeElement($precio);
    }

    /**
     * Get precios
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPrecios()
    {
        return $this->precios;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->precios = new \Doctrine\Common\Collections\ArrayCollection();
    }
}
