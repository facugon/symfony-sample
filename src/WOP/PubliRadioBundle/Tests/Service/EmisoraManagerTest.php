<?php

namespace WOP\PubliRadioBundle\Tests\Service;

use WOP\PubliRadioBundle\Service\EmisoraManager;
use IAR\CommonsBundle\Tests\ContainerAwareUnitTestCase;

class EmisoraManagerTest extends ContainerAwareUnitTestCase
{
    public function testMatchByName()
    {
        echo "Test Started\n\n";
        $emisoramanager = $this->get('emisora_manager');

        $emisora = $emisoramanager->findMatchByName('40 ANDALUCÍA');
        $this->assertInstanceOf('WOP\PubliRadioBundle\Entity\Emisora',$emisora);
        echo $emisora . "\n";

        $emisora = $emisoramanager->findMatchByName('RADIO VOZ GALICIA');
        $this->assertNull($emisora);
        echo "Emisora not found\n";

        $emisora = $emisoramanager->findMatchByName('MAXIMA FM CASTILLA LEÓN');
        $this->assertInstanceOf('WOP\PubliRadioBundle\Entity\Emisora',$emisora);
        echo $emisora . "\n";
    }
}
