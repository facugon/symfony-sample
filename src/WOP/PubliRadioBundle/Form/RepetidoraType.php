<?php

namespace WOP\PubliRadioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RepetidoraType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('nacional')
            ->add('provincia')
            ->add('comunidad')
            ->add('municipio')
            ->add('emisora')
            ->add('recargo')
            ->add('tipoModulacion', 'choice', array(
                'choices'   => array('FM' => 'FM', 'OM' => 'OM'),
                'required'  => true,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WOP\PubliRadioBundle\Entity\Repetidora'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wop_publiradiobundle_repetidora';
    }
}
