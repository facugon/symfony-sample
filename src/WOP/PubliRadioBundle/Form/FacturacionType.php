<?php

namespace WOP\PubliRadioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FacturacionType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('razonSocial')
            ->add('cif')
            ->add('direccion')
            ->add('fechaInicioCampania','date',array('widget' => 'text','required'=> true))
            ->add('fechaFinCampania','date',array('widget' => 'text','required'=> true))
            ->add('comentarios')
            ->add('preferenciaDiaSemana','choice',array(
                'choices'  => array('1' => 'Si','2' => 'No'),
                'expanded' => true ,
                'label'    => false,
                'multiple' => false
            ))
            ->add('campaniaHash','hidden')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WOP\PubliRadioBundle\Entity\Facturacion'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wop_publiradiobundle_facturacion';
    }
}
