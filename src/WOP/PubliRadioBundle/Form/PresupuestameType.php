<?php

namespace WOP\PubliRadioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PresupuestameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, array('error_bubbling' => true))
            ->add('apellido', null, array('error_bubbling' => true))
            ->add('email','repeated',array(
                'type'           => 'email',
                'required'       => true,
                'first_options'  => array('label' => 'Email'),
                'second_options' => array('label' => 'Repite Email'),
                'error_bubbling' => true
            ))
            ->add('detalles')
            ->add('captcha','captcha', array('error_bubbling' => true,'as_url' => true, 'reload' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'WOP\PubliRadioBundle\Entity\Presupuesto',
            'csrf_protection' => false,
            'intention'       => 'presupuesto'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wop_publiradiobundle_presupuestame';
    }
}
