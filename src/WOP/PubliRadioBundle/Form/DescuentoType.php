<?php

namespace WOP\PubliRadioBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DescuentoType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rangoInicial')
            ->add('rangoFinal')
            ->add('descuento')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WOP\PubliRadioBundle\Entity\Descuento'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'wop_publiradiobundle_descuento';
    }
}
