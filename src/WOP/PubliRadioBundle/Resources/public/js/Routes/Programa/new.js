
require.config({
    // By default load any module IDs from js/lib
    // mached by filename
    baseUrl: '/js/lib',
    // exceptions, load as defined below.
    // config is relative to the baseUrl.
    // never includes a ".js" extension since
    // the paths config could be for a directory.
    paths: {
        Model       : '/bundles/woppubliradio/js/Models',
        Controller  : '/bundles/woppubliradio/js/Controllers',
        View        : '/bundles/woppubliradio/js/Views',
        Template    : '/bundles/woppubliradio/templates'
    }
});

// Start the main app logic.
require(['jquery','Controller/Programa','Controller/Horario'],
    function($, ProgramaController, HorarioController) {

        $(function(){
            $("a.btn#horariocontroller_newitemaction").click(function(){
                HorarioController.newItemAction();
            });

            $("a.btn#programacontroller_saveaction").click(function(){
                ProgramaController.saveAction();
            });

            // this works with elements dinamically created 
            $("div#horario-list div#horarios").on("click","a.btn#horariocontroller_removeitemaction", function(){
                var indexHorario = $(this).closest("tr.horario").attr("id");
                HorarioController.removeItemAction(indexHorario);
            });
        });
    }
);
