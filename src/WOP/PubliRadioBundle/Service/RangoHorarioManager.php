<?php

namespace WOP\PubliRadioBundle\Service;

use WOP\PubliRadioBundle\Entity\Hora;

use Doctrine\ORM\EntityManager;

class RangoHorarioManager
{
    private $em ;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager ;
    }

    /**
     * Obtiene las horas en el Rango proporcionado con el formato 00:00-24:00
     *
     * @param string $rango
     * @return array<\WOP\PubliRadioBundle\Entity\Hora>
     */
    public function buildInRange($range)
    {
        $hours = explode('-', $range);

        $ini = $hours[0];
        $end = $hours[1];

        // obtengo los numeros de hora de inicio/fin
        $ininum = explode(':', $ini);
        $ininum = (int) $ininum[0];

        $endnum = explode(':', $end);
        $endnum = (int) $endnum[0];

        // obtengo las instancias pertenecientes al rango
        $horas = array();
        $index = $ininum;

        while( $index != $endnum ) // distinto porque cuando es 24 arranca de nuevo en 1
        {
            $instance = $this->em->getRepository('WOPPubliRadioBundle:Hora')->find($index);
            $horas[] = $instance;

            if( $index == 24 )
                $index = 1;
            else $index++;
        }

        return $horas;
    }
}
