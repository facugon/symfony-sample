<?php

namespace WOP\PubliRadioBundle\Service;

use WOP\PubliRadioBundle\Entity\Horario as Horario;

use Doctrine\ORM\EntityManager;

class HorarioManager
{
    private $em ;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager ;
    }

    /**
     * Obtiene los horarios en un Rango proporcionado con el formato L-V , L-J , S , D
     *
     * @param string $rango
     * @return Array<WOP\PubliRadioBundle\Entity\Horario>
     */
    public function buildInRange($range)
    {
		$id = $this->getNumberOfDaysByRange($range);

		$horarios = array();

		foreach( $id as $numdia )
		{
			$dia = $this->em->getRepository('WOPPubliRadioBundle:Dia')->find($numdia);

			$horario = new Horario();
			$horario->setDia($dia);

			$horarios[$numdia] = $horario ;
		}

        return $horarios;
    }

	public function getNumberOfDaysByRange($range)
	{
        $days = array ( // estos son los rangos por defecto, se podría hacer mas generico
            'L-J' => array(2, 3, 4, 5),
            'L-V' => array(2, 3, 4, 5, 6),
            'V'   => array(6),
            'S'   => array(7),
            'D'   => array(1)
        );

        return $days[ $range ] ;
	}

    /**
     * obtengo un horario de la emisora, en el horario y dias especificados
     *
     * @param \WOP\PubliRadioBundle\Entity\Emisora $emisora
     * @param string $rangoDia valores aceptados [L-V,SÁBADO,DOMINGO]
     * @param string $rangoHora valores aceptados [combinaciones en el rango 01:00-24:00]
     * @return \WOP\PubliRadioBundle\Entity\Horario
     */
    public function getWithEmisoraDiasHoras(\WOP\PubliRadioBundle\Entity\Emisora $emisora, $rangoDia, $rangoHora)
    {
        $hours = explode('-', $rangoHora);
        $ini = $hours[0];
        $ininum = explode(':', $ini);
        $hora = (int) $ininum[0];

        $structDia = array(
            'L-V' => array(2, 3, 4, 5, 6),
            'SÁBADO' => array(7),
            'DOMINGO' => array(1)
        );

        $horarios = array();

        try {

            $dias = $structDia[ $rangoDia ];
            foreach($dias as $dia)
            {
                $horarios[] = $this->em
                    ->getRepository('WOPPubliRadioBundle:Horario')
                    ->findOneByEmisoraDiaHora(
                        $emisora->getId(),
                        $dia,
                        $hora
                    );
            }

		} 
		catch (\Doctrine\ORM\NoResultException $e)
		{
            echo "Exception NoResult {$e->getMessage()}\n";
            echo "params [{$emisora->getId()}, {$dia}, {$hora}]\n";
		}
		catch (\Doctrine\ORM\NonUniqueResultException $e)
		{
            echo "Exception NonUniqueResult {$e->getMessage()}\n";
            echo "params [{$emisora->getId()}, {$dia}, {$hora}]\n";
        }

        return $horarios ;
    }
}
