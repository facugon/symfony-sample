<?php

namespace WOP\PubliRadioBundle\Service;

//use WOP\PubliRadioBundle\Entity\Emisora;

use Doctrine\ORM\EntityManager;

class EmisoraManager
{
    private $em ;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager ;
    }

    public function findMatchByName($sname)
    {
        $emisoras = $this->em->getRepository('WOPPubliRadioBundle:Emisora')->findAll();

        $match = null;
        $count = count($emisoras);
        $index = 0;

        while( is_null($match) && $index < $count )
        {
            $emisora = $emisoras[ $index ];
            if( stristr($sname, $emisora->getNombre()) != false )
                $match = $emisora;
            $index++;
        }

        return $match;
    }

    public function findMunicipioMatchByNames(array $names, \WOP\PubliRadioBundle\Entity\Provincia $provincia)
    {
        $municipio=null;
        try{
            $municipio = $this->em
                ->createQuery("SELECT m FROM \WOP\PubliRadioBundle\Entity\Municipio m WHERE m.nombre IN (:names) AND m.provincia = :provincia")
                ->setParameter('provincia',$provincia->getId())
                ->setParameter('names',$names)
                ->getOneOrNullResult();
        }
        catch (\Doctrine\ORM\NoResultException $e) 
        {
            echo "Exception NoResult {$e->getMessage()}\n";
            $names = implode(',',$names);
            echo "params [{$names}, {$provincia}]\n";
        }
        catch (\Doctrine\ORM\NonUniqueResultException $e) 
        {
            echo "Exception NonUniqueResult {$e->getMessage()}\n";
            $names = implode(',',$names);
            echo "params [{$names}, {$provincia}]\n";
        }

        return $municipio;
    }
}
