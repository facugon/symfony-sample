<?php

namespace WOP\PubliRadioBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\SecurityContext;

use WOP\PubliRadioBundle\Entity\Solicitud;
use WOP\PubliRadioBundle\Entity\Campania;

class CampaniaManager
{
    private $em ;

    public function __construct(EntityManager $entityManager, SecurityContext $securityContext)
    {
        $this->entitymanager = $entityManager ;
        $this->securitycontext = $securityContext ;
    }

    public function getMisCampanias()
    {
        $user = $this->securitycontext->getToken()->getUser();

        $em = $this->entitymanager->getRepository('WOPPubliRadioBundle:Solicitud');

        $campanias = $em->findAllByUserId( $user->getId() );

        return $campanias ;
    }

    public function getCampanias()
    {
        $user = $this->securitycontext->getToken()->getUser();

        $em = $this->entitymanager->getRepository('WOPPubliRadioBundle:Solicitud');

        $campanias = $em->findAll();

        return $campanias ;
    }

    public function getCampania($hash)
    {
        $em = $this->entitymanager->getRepository('WOPPubliRadioBundle:Solicitud');

        $campania = $em->findByHash($hash);

        return $campania ;
    }

    public function getSolicitudCompleta($hash)
    {
        $em = $this->entitymanager->getRepository('WOPPubliRadioBundle:Solicitud');

        $campania = $em->findAllItemsByHash($hash);

        return $campania ;
    }


    public function pagarCampania($hash)
    {
        $campania = $this->getSolicitudCompleta($hash);
        $em = $this->entitymanager;

        foreach( $campania as $item )
        {
            $item->setEstado( Solicitud::Paga );
            $em->persist($item);
        }
        $em->flush();
    }

    public function getCampaniaEspacios($hash)
    {
        $em = $this->entitymanager->getRepository('WOPPubliRadioBundle:Solicitud');

        $items = $em->getInfoEspaciosByHash($hash);

        return $items;
    }

    public function crearpresupuestofinalcampania($hash,$preciofinal)
    {
        $solicitud = $this->getSolicitudCompleta($hash);
        $em = $this->entitymanager;

        $campania = new Campania();
        $campania->setHash($hash);
        $campania->setPresupuesto($preciofinal);
        $campania->setEstado(Solicitud::EsperandoConfirmacion);
        $campania->setUserid($solicitud[0]->getUserid());
        $em->persist($campania);

        foreach( $solicitud as $item )
        {
            $item->setEstado( Solicitud::EsperandoConfirmacion );
            $em->persist($item);
        }
        $em->flush();
    }

    public function actualizarpreciocondescuento(\WOP\PubliRadioBundle\Entity\Descuento $descuento,$hash)
    {
        $em = $this->entitymanager;
        $solicitud = $this->getSolicitudCompleta($hash);
        foreach( $solicitud as $item )
        {
            $item->aplicarDescuento($descuento);
            $em->persist($item);
        }
        $em->flush();
    }

    public function confirmarpresupuestofinal($hash)
    {
        $solicitud = $this->getSolicitudCompleta($hash);
        $em = $this->entitymanager;

        $campania = $em
            ->getRepository('WOPPubliRadioBundle:Campania')
            ->findOneByHash($hash);

        $campania->setEstado(Solicitud::Confirmada);
        $em->persist($campania);

        foreach( $solicitud as $item )
        {
            $item->setEstado( Solicitud::Confirmada );
            $em->persist($item);
        }
        $em->flush();
    }

    public function gettotalapagar($hash)
    {
        $solicitud = $this->getCampania($hash);

        return $solicitud['preciototalcondescuento'] ;
    }

    public function getcomision($hash)
    {
        return $this->gettotalapagar($hash) * ( 1 / 100 ) ; // el 1%
    }

    public function getusercampania($hash)
    {
        $campania = $this->getCampania($hash);

        $user = $this->entitymanager
            ->getRepository('IARUserBundle:User')->find( $campania['campania']->getUserId() );

        return $user;
    }
}
