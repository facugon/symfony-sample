<?php

/*
 * ***********************************************************
 * Comunidades Autónomas.
 *
 * Albert Lombarte
 * Twitter: @alombarte
 *
 * https://github.com/alombarte/utilities/blob/master/sql/spain_comunidades_autonomas.sql
 * ------------------------------------------------------------
 */

return "
INSERT INTO `Comunidad` (`id`, `nombre`)
VALUES
        (1,'Andalucía'),
        (2,'Aragón'),
        (3,'Asturias'),
        (4,'Baleares'),
        (5,'Canarias'),
        (6,'Cantabria'),
        (7,'Castilla León'),
        (8,'Castilla La Mancha'),
        (9,'Cataluña'),
        (10,'Com. Valenciana'),
        (11,'Extremadura'),
        (12,'Galicia'),
        (13,'Madrid'),
        (14,'Murcia'),
        (15,'Navarra'),
        (16,'País Vasco'),
        (17,'La Rioja'),
        (18,'Ceuta'),
        (19,'Melilla'),
        (20,'Andorra') ;
";
