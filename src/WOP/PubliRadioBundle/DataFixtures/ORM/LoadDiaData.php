<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use WOP\PubliRadioBundle\Entity\Dia;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadDiaData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->getData();

        foreach($data as $prop)
        {
            $d = new Dia();
            $d->setId($prop->id);
            $d->setNumeroDia($prop->numero_dia);
            $d->setNombreDia($prop->nombre_dia);

            $manager->persist($d);
            $manager->flush();
        }
    }

    private function getData()
    {
        return array(
            (object) array('id' => '1', 'numero_dia' => '1', 'nombre_dia' => 'Domingo' ),
            (object) array('id' => '2', 'numero_dia' => '2', 'nombre_dia' => 'Lunes' ),
            (object) array('id' => '3', 'numero_dia' => '3', 'nombre_dia' => 'Martes' ),
            (object) array('id' => '4', 'numero_dia' => '4', 'nombre_dia' => 'Miercoles' ),
            (object) array('id' => '5', 'numero_dia' => '5', 'nombre_dia' => 'Jueves' ),
            (object) array('id' => '6', 'numero_dia' => '6', 'nombre_dia' => 'Viernes' ),
            (object) array('id' => '7', 'numero_dia' => '7', 'nombre_dia' => 'Sabado' )
        );
    }

    /**
     * Despues de cargar las frecuencias
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 6;
    }
}
