<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\Persistence\ObjectManager;
use WOP\PubliRadioBundle\Entity\Recargo;

class LoadRecargoData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $stmt = $this->loadStmt();

        $manager
            ->getConnection()
            ->prepare($stmt)
            ->execute();
    }

    private function loadStmt()
    {
        return "
            INSERT INTO `Recargo` (`id`, `descripcion`, `porcentaje`, `segundos_duracion`, `regla`) VALUES
            (1, 'Recargo 10% duración inferior/igual a 10\"', 10, 10, 'menor igual'),
            (2, 'Recargo 15% duración inferior/igual a 15\"', 15, 15, 'menor igual'),
            (3, 'Recargo 20% duración inferior/igual a 10\"', 20, 10, 'menor igual');
        ";
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5; // the order in which fixtures will be loaded
    }
}
