<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use WOP\PubliRadioBundle\Entity\Descuento;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadDescuentoData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->getData();

        foreach($data as $prop)
        {
            $d = new Descuento();
            $d->setId($prop->id);
            $d->setRangoInicial($prop->rangoinicial);
            $d->setRangoFinal($prop->rangofinal);
            $d->setDescuento($prop->descuento);

            $manager->persist($d);
            $manager->flush();
        }
    }

    private function getData()
    {
        return array(
            (object) array('id' => '1' , 'descuento' => 20, 'rangoinicial' => 0     , 'rangofinal' => 2000  ),
            (object) array('id' => '2' , 'descuento' => 30, 'rangoinicial' => 2000  , 'rangofinal' => 5000  ),
            (object) array('id' => '3' , 'descuento' => 40, 'rangoinicial' => 5000  , 'rangofinal' => 10000 ),
            (object) array('id' => '4' , 'descuento' => 50, 'rangoinicial' => 10000 , 'rangofinal' => 30000 ),
            (object) array('id' => '5' , 'descuento' => 60, 'rangoinicial' => 30000 , 'rangofinal' => 50000 ),
            (object) array('id' => '6' , 'descuento' => 70, 'rangoinicial' => 50000 , 'rangofinal' => 0     )
        );
    }

    /**
     * Despues de cargar las frecuencias
     *
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
