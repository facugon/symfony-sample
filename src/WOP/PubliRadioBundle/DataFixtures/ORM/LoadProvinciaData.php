<?php

namespace WOP\PubliRadioBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use WOP\PubliRadioBundle\Entity\Provincia;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

class LoadProvinciaData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $data = $this->loadData();

        foreach($data as $dataProvincia) {

            $unaProvincia = new Provincia();
            $unaProvincia->setId    ( $dataProvincia[0] );
            $unaProvincia->setNombre( $dataProvincia[1] );

            $manager->persist($unaProvincia);
            $manager->flush();
            unset($unaProvincia);
        }
    }

    private function loadData()
    {
        $data = include 'data/provincias.php';

        return $data ;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2; // the order in which fixtures will be loaded
    }
}

