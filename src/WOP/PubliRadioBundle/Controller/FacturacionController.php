<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Facturacion;
use WOP\PubliRadioBundle\Form\FacturacionType;

/**
 * Facturacion controller.
 *
 * @Route("/admin/facturacion")
 */
class FacturacionController extends Controller
{
    /**
     * Lists all Facturacion entities.
     *
     * @Route("/", name="admin_facturacion")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Facturacion')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Facturacion entity.
     *
     * @Route("/", name="admin_facturacion_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Facturacion:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Facturacion();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_facturacion_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Facturacion entity.
    *
    * @param Facturacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Facturacion $entity)
    {
        $form = $this->createForm(new FacturacionType(), $entity, array(
            'action' => $this->generateUrl('admin_facturacion_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Facturacion entity.
     *
     * @Route("/new", name="admin_facturacion_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Facturacion();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Facturacion entity.
     *
     * @Route("/{id}", name="admin_facturacion_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Facturacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Facturacion entity.
     *
     * @Route("/{id}/edit", name="admin_facturacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Facturacion entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Facturacion entity.
    *
    * @param Facturacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Facturacion $entity)
    {
        $form = $this->createForm(new FacturacionType(), $entity, array(
            'action' => $this->generateUrl('admin_facturacion_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Facturacion entity.
     *
     * @Route("/{id}", name="admin_facturacion_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Facturacion:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Facturacion entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_facturacion_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Facturacion entity.
     *
     * @Route("/{id}", name="admin_facturacion_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Facturacion entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_facturacion'));
    }

    /**
     * Creates a form to delete a Facturacion entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_facturacion_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
