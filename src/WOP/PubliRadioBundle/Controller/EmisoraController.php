<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Emisora;
use WOP\PubliRadioBundle\Form\EmisoraType;

/**
 * Emisora controller.
 *
 * @Route("/admin/emisora")
 */
class EmisoraController extends Controller
{

    /**
     * Lists all Emisora entities.
     *
     * @Route("/", name="admin_emisora")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Emisora')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Emisora entity.
     *
     * @Route("/", name="admin_emisora_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Emisora:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Emisora();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();

            $entity->upload();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_emisora_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Emisora entity.
    *
    * @param Emisora $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Emisora $entity)
    {
        $form = $this->createForm(new EmisoraType(), $entity, array(
            'action' => $this->generateUrl('admin_emisora_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Emisora entity.
     *
     * @Route("/new", name="admin_emisora_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Emisora();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Emisora entity.
     *
     * @Route("/{id}", name="admin_emisora_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Emisora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Emisora entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Emisora entity.
     *
     * @Route("/{id}/edit", name="admin_emisora_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Emisora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Emisora entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'emisora'     => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Emisora entity.
    *
    * @param Emisora $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Emisora $entity)
    {
        $form = $this->createForm(new EmisoraType(), $entity, array(
            'action' => $this->generateUrl('admin_emisora_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Emisora entity.
     *
     * @Route("/{id}", name="admin_emisora_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Emisora:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Emisora')->find($id);

        if(!$entity) {
            throw $this->createNotFoundException('No se pudo encontrar la Emisora');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid())
        {
            $entity->upload();
            $em->flush();

            return $this->redirect($this->generateUrl('admin_emisora_edit', array('id' => $id)));
        }

        $deleteForm = $this->createDeleteForm($id);
        return array(
            'emisora'     => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView()
        );
    }
    /**
     * Deletes a Emisora entity.
     *
     * @Route("/{id}", name="admin_emisora_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Emisora')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Emisora entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_emisora'));
    }

    /**
     * Creates a form to delete a Emisora entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_emisora_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
