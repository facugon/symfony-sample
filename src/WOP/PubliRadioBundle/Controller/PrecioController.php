<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use WOP\PubliRadioBundle\Entity\Precio;
use WOP\PubliRadioBundle\Form\PrecioType;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Precio controller.
 *
 * @Route("/admin/precio")
 */
class PrecioController extends Controller
{
    /**
     * Fetch precios por Repetidora.
     *
     * @Route("/fetch/{repetidora}", name="admin_precio_fetch", requirements={"repetidora" = "\d+"}, defaults={"repetidora" = 0}))
     * @Method("GET")
     * @Template("WOPPubliRadioBundle:Precio:index.html.twig")
     */
    public function fetchAction($repetidora)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Precio')->findAllOrderedByRepetidora($repetidora);

        return array( 'entities' => $entities, 'repetidora' => $repetidora );
    }

    /**
     * Creates a new Precio entity.
     *
     * @Route("/", name="admin_precio_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Precio:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Precio();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_precio_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Precio entity.
    *
    * @param Precio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Precio $entity)
    {
        $form = $this->createForm(new PrecioType(), $entity, array(
            'action' => $this->generateUrl('admin_precio_create'),
            'method' => 'POST',
            'attr' => array(
                'id' => 'new_precio'
            )
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Precio entity.
     *
     * @Route("/new/{repetidora}", name="admin_precio_new", requirements={"repetidora" = "\d+"}, defaults={"repetidora" = 0})
     * @Method("GET")
     * @Template()
     */
    public function newAction($repetidora)
    {
        $entity = new Precio();
        $form   = $this->createCreateForm($entity);

        return array(
            'repetidora' => $repetidora,
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Precio entity.
     *
     * @Route("/{id}", name="admin_precio_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Precio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Precio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Precio entity.
     *
     * @Route("/{id}/edit", name="admin_precio_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Precio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Precio entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Precio entity.
    *
    * @param Precio $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Precio $entity)
    {
        $form = $this->createForm(new PrecioType(), $entity, array(
            'action' => $this->generateUrl('admin_precio_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Precio entity.
     *
     * @Route("/{id}", name="admin_precio_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Precio:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Precio')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Precio entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_precio_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Precio entity.
     *
     * @Route("/{id}", name="admin_precio_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Precio')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Precio entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_precio'));
    }

    /**
     * Creates a form to delete a Precio entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_precio_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
