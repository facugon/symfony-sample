<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Programa;
use WOP\PubliRadioBundle\Form\ProgramaType;

/**
 * Programa controller.
 *
 * @Route("/admin/programa")
 */
class ProgramaController extends Controller
{

    /**
     * Lists all Programa entities.
     *
     * @Route("/", name="admin_programa")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Programa')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Programa entity.
     *
     * @Route("/", name="admin_programa_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Programa:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Programa();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_programa_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Programa entity.
    *
    * @param Programa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Programa $entity)
    {
        $form = $this->createForm(new ProgramaType(), $entity, array(
            'action' => $this->generateUrl('admin_programa_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Programa entity.
     *
     * @Route("/new", name="admin_programa_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Programa();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Programa entity.
     *
     * @Route("/{id}", name="admin_programa_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Programa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Programa entity.
     *
     * @Route("/{id}/edit", name="admin_programa_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Programa entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Programa entity.
    *
    * @param Programa $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Programa $entity)
    {
        $form = $this->createForm(new ProgramaType(), $entity, array(
            'action' => $this->generateUrl('admin_programa_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Programa entity.
     *
     * @Route("/{id}", name="admin_programa_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Programa:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Programa')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Programa entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_programa_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Programa entity.
     *
     * @Route("/{id}", name="admin_programa_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Programa')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Programa entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_programa'));
    }

    /**
     * Creates a form to delete a Programa entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_programa_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
