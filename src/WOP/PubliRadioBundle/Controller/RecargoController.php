<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Recargo;
use WOP\PubliRadioBundle\Form\RecargoType;

/**
 * Recargo controller.
 *
 * @Route("/admin/recargo")
 */
class RecargoController extends Controller
{

    /**
     * Lists all Recargo entities.
     *
     * @Route("/", name="admin_recargo")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Recargo')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Recargo entity.
     *
     * @Route("/", name="admin_recargo_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Recargo:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Recargo();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_recargo_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Recargo entity.
    *
    * @param Recargo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Recargo $entity)
    {
        $form = $this->createForm(new RecargoType(), $entity, array(
            'action' => $this->generateUrl('admin_recargo_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Recargo entity.
     *
     * @Route("/new", name="admin_recargo_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Recargo();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Recargo entity.
     *
     * @Route("/{id}", name="admin_recargo_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Recargo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Recargo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Recargo entity.
     *
     * @Route("/{id}/edit", name="admin_recargo_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Recargo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Recargo entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Recargo entity.
    *
    * @param Recargo $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Recargo $entity)
    {
        $form = $this->createForm(new RecargoType(), $entity, array(
            'action' => $this->generateUrl('admin_recargo_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Recargo entity.
     *
     * @Route("/{id}", name="admin_recargo_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Recargo:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Recargo')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Recargo entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_recargo_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Recargo entity.
     *
     * @Route("/{id}", name="admin_recargo_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Recargo')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Recargo entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_recargo'));
    }

    /**
     * Creates a form to delete a Recargo entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_recargo_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
