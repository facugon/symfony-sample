<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Mensajeria;
use WOP\PubliRadioBundle\Form\MensajeriaType;

/**
 * Mensajeria controller.
 *
 * @Route("/admin/mensajeria")
 */
class MensajeriaController extends Controller
{

    /**
     * Lists all Mensajeria entities.
     *
     * @Route("/", name="admin_mensajeria")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Mensajeria')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Mensajeria entity.
     *
     * @Route("/", name="admin_mensajeria_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Mensajeria:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Mensajeria();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_mensajeria_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Mensajeria entity.
    *
    * @param Mensajeria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Mensajeria $entity)
    {
        $form = $this->createForm(new MensajeriaType(), $entity, array(
            'action' => $this->generateUrl('admin_mensajeria_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Mensajeria entity.
     *
     * @Route("/new", name="admin_mensajeria_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Mensajeria();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Mensajeria entity.
     *
     * @Route("/{id}", name="admin_mensajeria_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Mensajeria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajeria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Mensajeria entity.
     *
     * @Route("/{id}/edit", name="admin_mensajeria_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Mensajeria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajeria entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Mensajeria entity.
    *
    * @param Mensajeria $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Mensajeria $entity)
    {
        $form = $this->createForm(new MensajeriaType(), $entity, array(
            'action' => $this->generateUrl('admin_mensajeria_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Mensajeria entity.
     *
     * @Route("/{id}", name="admin_mensajeria_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Mensajeria:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Mensajeria')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Mensajeria entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_mensajeria_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Mensajeria entity.
     *
     * @Route("/{id}", name="admin_mensajeria_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Mensajeria')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Mensajeria entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_mensajeria'));
    }

    /**
     * Creates a form to delete a Mensajeria entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_mensajeria_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
