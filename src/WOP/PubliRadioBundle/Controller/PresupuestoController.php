<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Presupuesto;
use WOP\PubliRadioBundle\Form\PresupuestameType;

/**
 * Presupuesto controller.
 *
 * @Route("/admin/presupuesto")
 */
class PresupuestoController extends Controller
{
    /**
     * Lists all Presupuesto entities.
     *
     * @Route("/", name="admin_presupuesto")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Presupuesto')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Finds and displays a Presupuesto entity.
     *
     * @Route("/{id}", name="admin_presupuesto_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Presupuesto')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Presupuesto entity.');
        }

        return array( 'entity' => $entity );
    }
}
