<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;

// LA API DE PAYPAL QUE NO ANDA !
//use PayPal\Types\AP\PayRequest;
//use PayPal\Types\AP\Receiver;
//use PayPal\Types\AP\ReceiverList;
//use PayPal\Types\Common\RequestEnvelope;
//use PayPal\Service\AdaptivePaymentsService;

/**
 * PayPal controller.
 *
 * @Route("/paypal")
 */
class PayPalController extends Controller
{
    /**
     * @Route("/payment/{hash}", name="b2c_paypal_payment")
     * @Method("GET")
     */
    public function paymentAction($hash)
    {
        /*
        $config = array(
            'mode' => 'sandbox',
            'acct1.UserName' => 'facundo.siquot-facilitator_api1.gmail.com',
            'acct1.Password' => '1402165520',
            'acct1.Signature' => 'AFcWxV21C7fd0v3bYYYRCpSSRl31A63c0KY7zppjcNEwQgBpsRK7X94S',
            'acct1.AppId' => 'APP-80W284485P519543T'
        );

        $payRequest = new PayRequest();
        $payRequest->actionType = 'PAY' ;
        $payRequest->currencyCode = 'USD' ;
        $payRequest->cancelUrl = 'http://stagingpubli.com' . $this->generateUrl('b2c_paypal_cancel',array('hash' => $hash)) ;
        $payRequest->returnUrl = 'http://stagingpubli.com' . $this->generateUrl('b2c_paypal_return',array('hash' => $hash)) ;

        $payRequest->requestEnvelope = new RequestEnvelope();
        $payRequest->requestEnvelope->errorLanguage = 'en_US';
        $payRequest->requestEnvelope->detailLevel = 'ReturnAll';

        $receiver = new Receiver();
        $receiver->email   = 'facundo.siquot-facilitator_api1@gmail.com';
        $receiver->amount  = '1.00';

        $payRequest->receiverList = new ReceiverList(array($receiver));

        $service = new AdaptivePaymentsService($config);
        $response = $service->Pay($payRequest);

        return new Response( json_encode($response->error) );
        //return new Response( json_encode($payRequest) );
        */
        
        $comision = $this->get('campania_manager')->getcomision($hash);
        $r = curl_init();

        $payload = '{
               "actionType":"PAY",
               "currencyCode":"USD",
               "receiverList":{
                  "receiver":[
                     {
                        "amount":"' . round( (float)$comision, 2, PHP_ROUND_HALF_UP ) . '",
                        "email":"martin.alfonsi@gmail.com"
                     }
                  ]
               },
               "returnUrl":"http://stagingpubli.com' . $this->generateUrl('b2c_paypal_return',array('hash' => $hash)) . '",
               "cancelUrl":"http://stagingpubli.com' . $this->generateUrl('b2c_paypal_cancel',array('hash' => $hash)) . '",
               "requestEnvelope":{
                  "errorLanguage":"en_US",
                  "detailLevel":"ReturnAll"
               }
            }';

        // CONFIGURACION DE ACCESO
        $headers = array(
            'X-PAYPAL-SECURITY-USERID: martin.alfonsi-facilitator_api1.gmail.com',
            'X-PAYPAL-SECURITY-PASSWORD: 1403486090',
            'X-PAYPAL-SECURITY-SIGNATURE: AcfQubg8OjO92GeiyZriM1DO8OJqAQ9KY7bf.FF..Ebu4C9ced0BEfHP',
            'X-PAYPAL-APPLICATION-ID: APP-80W284485P519543T',
            'X-PAYPAL-REQUEST-DATA-FORMAT: JSON',
            'X-PAYPAL-RESPONSE-DATA-FORMAT: JSON'
        );

        curl_setopt($r, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($r, CURLOPT_URL,'https://svcs.sandbox.paypal.com/AdaptivePayments/Pay');
        curl_setopt($r, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($r, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($r, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($r);
        curl_close($r);

        $data = json_decode($response);

        if( ! isset($data->error) ) {
            if(strtoupper($data->responseEnvelope->ack) == 'SUCCESS')
            {
                $token      = $data->payKey;
                $payPalURL  = 'https://www.sandbox.paypal.com/cgi-bin/webscr?cmd=_ap-payment&paykey='.$token;
                header("Location: " . $payPalURL);
                exit;
            }
        } else {
            return new Response( $response );
        }
    }

    /**
     * @Route("/cancel/{hash}", name="b2c_paypal_cancel")
     * @Method("GET")
     */
    public function cancelAction(Request $request,$hash)
    {
        return new Response("<script>window.close();</script>");
    }

    /**
     * @Route("/return/{hash}", name="b2c_paypal_return")
     * @Method("GET")
     */
    public function returnAction(Request $request,$hash)
    {
        $manager = $this->get('campania_manager');

        $manager->pagarCampania($hash);

        return new Response("<script>window.close();</script>");
    }
}
