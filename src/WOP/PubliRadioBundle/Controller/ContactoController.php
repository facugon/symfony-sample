<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
//use Symfony\Component\HttpFoundation\Response;

use IAR\ContactoBundle\Entity\Contactame;
use IAR\ContactoBundle\Form\ContactameType;

class ContactoController extends Controller
{
    /**
     * Obtengo las campañas del usuario que consulta
     * @Route("/contactar/")
     * @Template()
     */
    public function contactarAction(Request $request)
    {
        $contacto = new Contactame();
        $form = $this->createCreateForm($contacto);
        $form->handleRequest($request);

        if( $form->isValid() )
        {
            $em = $this->getDoctrine()->getManager();
            $contacto->setDatetime(new \DateTime());
            $em->persist($contacto);
            $em->flush();

            return $this->redirect($this->generateUrl('b2c_index'));
        }

        return array (
            'contacto' => $contacto,
            'form'     => $form->createView()
        );
    }

    /**
    * Creates a form to create a Contactame entity.
    * @param Contactame $entity The entity
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Contactame $entity)
    {
        $form = $this->createForm(new ContactameType(), $entity, array(
            'action' => $this->generateUrl('wop_publiradio_contacto_contactar'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

}
