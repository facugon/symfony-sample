<?php

namespace WOP\PubliRadioBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Solicitud;

/**
 * Solicitud controller.
 *
 * @Route("/admin/solicitud")
 */
class SolicitudController extends Controller
{
    /**
     * Lists all Solicitud entities.
     *
     * @Route("/", name="admin_solicitud")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Solicitud')->findAll();

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Solicitud entity.
     *
     * @Route("/{id}", name="admin_solicitud_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Solicitud')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Solicitud entity.');
        }

        return array('entity' => $entity);
    }
}
