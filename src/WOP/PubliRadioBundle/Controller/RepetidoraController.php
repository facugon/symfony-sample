<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Repetidora;
use WOP\PubliRadioBundle\Form\RepetidoraType;

/**
 * Repetidora controller.
 *
 * @Route("/admin/repetidora")
 */
class RepetidoraController extends Controller
{
    /**
     * Lists all Repetidora entities.
     *
     * @Route("/", name="admin_repetidora")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Repetidora')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Repetidora entity.
     *
     * @Route("/", name="admin_repetidora_create")
     * @Method("POST")
     * @Template("WOPPubliRadioBundle:Repetidora:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Repetidora();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_repetidora_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Repetidora entity.
    *
    * @param Repetidora $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Repetidora $entity)
    {
        $form = $this->createForm(new RepetidoraType(), $entity, array(
            'action' => $this->generateUrl('admin_repetidora_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Repetidora entity.
     *
     * @Route("/new", name="admin_repetidora_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Repetidora();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Repetidora entity.
     *
     * @Route("/{id}", name="admin_repetidora_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Repetidora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repetidora entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Repetidora entity.
     *
     * @Route("/{id}/edit", name="admin_repetidora_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Repetidora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repetidora entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Repetidora entity.
    *
    * @param Repetidora $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Repetidora $entity)
    {
        $form = $this->createForm(new RepetidoraType(), $entity, array(
            'action' => $this->generateUrl('admin_repetidora_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Repetidora entity.
     *
     * @Route("/{id}", name="admin_repetidora_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:Repetidora:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Repetidora')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Repetidora entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('admin_repetidora_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Repetidora entity.
     *
     * @Route("/{id}", name="admin_repetidora_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('WOPPubliRadioBundle:Repetidora')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Repetidora entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_repetidora'));
    }

    /**
     * Creates a form to delete a Repetidora entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_repetidora_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
