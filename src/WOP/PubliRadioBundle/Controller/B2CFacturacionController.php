<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WOP\PubliRadioBundle\Entity\Facturacion;
use WOP\PubliRadioBundle\Form\FacturacionType;

/**
 * Facturacion controller.
 *
 * @Route("/facturacion")
 */
class B2CFacturacionController extends Controller
{
    /**
     * Displays a form to edit an existing Facturacion entity.
     *
     * @Route("/edit/{hash}", name="b2c_backoffice_facturacion_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($hash)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->findByCampaniaHash($hash);

        if(!$entity) {
            throw $this->createNotFoundException('No se pudo encontrar el registro de Facturacion.');
        }

        $editForm = $this->createEditForm($entity[0], $hash);

        return array (
            'facturacion' => $entity,
            'form' => $editForm->createView()
        );
    }

    /**
    * Creates a form to edit a Facturacion entity.
    *
    * @param Facturacion $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Facturacion $entity, $hash)
    {
        $form = $this->createForm(new FacturacionType(), $entity, array(
            'action' => $this->generateUrl('b2c_backoffice_facturacion_update', array('hash' => $hash)),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Facturacion entity.
     *
     * @Route("/{hash}", name="b2c_backoffice_facturacion_update")
     * @Method("PUT")
     * @Template("WOPPubliRadioBundle:B2CFacturacion:edit.html.twig")
     */
    public function updateAction(Request $request, $hash)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Facturacion')->findByCampaniaHash($hash);

        if( !$entity )
            throw $this->createNotFoundException('Unable to find Facturacion entity.');

        $form = $this->createEditForm($entity[0], $hash);
        $form->handleRequest($request);

        if( $form->isValid() )
        {
            $em->flush();
            return $this->redirect($this->generateUrl('wop_publiradio_campania_detalles', array('hash' => $hash)));
        }

        return array( 'facturacion' => $entity, 'form' => $form->createView() );
    }
}
