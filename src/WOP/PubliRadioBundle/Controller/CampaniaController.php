<?php

namespace WOP\PubliRadioBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;

use WOP\PubliRadioBundle\Entity\Solicitud;
use WOP\PubliRadioBundle\Entity\Campania;

use WOP\PubliRadioBundle\Entity\Mensajeria;
use WOP\PubliRadioBundle\Form\MensajeriaType;

use WOP\PubliRadioBundle\Entity\Presupuesto;
use WOP\PubliRadioBundle\Form\PresupuestameType;

class CampaniaController extends Controller
{
    /**
     * Obtengo las campañas del usuario que consulta
     * @Route("/miscampanias")
     * @Method("GET")
     * @Template()
     */
    public function miscampaniasAction()
    {
        $manager = $this->get('campania_manager');

        $campanias = $manager->getMisCampanias();

        return array( 'campanias' => $campanias);
    }

    /**
     * Obtengo las campañas del usuario que consulta
     * @Route("/admin/campanias")
     * @Method("GET")
     * @Template()
     */
    public function campaniasAction()
    {
        $manager = $this->get('campania_manager');

        $campanias = $manager->getCampanias();

        return array( 'campanias' => $campanias);
    }

    /**
     * @Route("/campania/detalles/{hash}")
     * @Method("GET")
     * @Template()
     */
    public function detallesAction($hash)
    {
        $manager = $this->get('campania_manager');
        $solicitud = $manager->getCampania($hash);

        $campania = $this->getDoctrine()->getManager()
            ->getRepository('WOPPubliRadioBundle:Campania')
            ->findOneByHash($hash);

        $facturacion = $this->getDoctrine()->getManager()
            ->getRepository('WOPPubliRadioBundle:Facturacion')
            ->findOneByCampaniaHash($hash);

        $result = array (
            'campania'                => $solicitud['campania']                , 
            'facturacion'            => $facturacion,
            'preciototal'             => $solicitud['preciototal']             , 
            'preciototalcondescuento' => $solicitud['preciototalcondescuento'] , 
        );

        if( ! empty($campania) )
            $result['preciofinalcondescuento'] = $campania->getPresupuesto() ;

        return $result;
    }

    /**
     * @Route("/campania/espacios/{hash}")
     * @Method("GET")
     * @Template()
     */
    public function espaciosAction($hash)
    {
        $manager = $this->get('campania_manager');

        $espacios = $manager->getCampaniaEspacios($hash);

        return array( 'espacios' => $espacios );
    }

    /**
     * @Route("/campania/pagar/{hash}")
     * @Method("POST")
     */
    public function pagarAction($hash)
    {
        $manager = $this->get('campania_manager');

        $manager->pagarCampania($hash);

        return new Response( json_encode(array('result' => 'success')), 200 );
    }

    /**
     * @Route("/campania/estado/{hash}")
     * @Method("GET")
     */
    public function estadoAction($hash)
    {
        $manager = $this->get('campania_manager');

        $solicitud = $manager->getCampania($hash);

        return new Response( json_encode(array('result' => 'success','data' => $solicitud)), 200 );
    }

    /**
     * @Route("/campania/crearpresupuestofinal/{hash}/{precio}")
     * @Method("POST")
     */
    public function crearpresupuestofinalAction($hash,$precio)
    {
        $manager = $this->get('campania_manager');

        $manager->crearpresupuestofinalcampania($hash,$precio);

        $this->enviarmailconfirmacionpresupuesto($hash);

        return new Response( json_encode(array('result' => 'success')), 200 );
    }

    /**
     * @Route("/campania/revisar/{hash}")
     * @Method("POST")
     */
    public function revisarAction($hash)
    {
        $manager = $this->get('campania_manager');

        $manager->revisarCampania($hash);

        return new Response( json_encode(array('result' => 'success')), 200 );
    }

    /**
     * @Route("/campania/presupuestame/", name="wop_publiradio_campania_presupuestame")
     * @Template("WOPPubliRadioBundle:Campania:presupuestame.html.twig")
     */
    public function presupuestameAction(Request $request)
    {
        $tipo = $request->query->get('tipo');

        if( empty($tipo) )
            $tipo = "presupuestame";

        $entity = new Presupuesto();
        $form = $this->createCreateForm($entity,$tipo);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('b2c_index'));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'tipo'   => $tipo
        );
    }

    /**
    * Creates a form to create a Presupuesto entity.
    * @param Presupuesto $entity The entity
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Presupuesto $entity, $tipo)
    {
        $form = $this->createForm(new PresupuestameType(), $entity, array(
            'action' => $this->generateUrl('wop_publiradio_campania_presupuestame'),
            'method' => 'POST',
        ));

        $form->add('submit','submit',array('label' => 'ENVIAR'));
        $form->add('tipo','hidden',array('data' => $tipo));

        return $form;
    }

    /**
     * @Route("/campania/comentarios/{hash}", name="wop_publiradio_campania_comentarios")
     * @Template()
     */
    public function comentariosAction(Request $request)
    {
        $hash = $request->get('hash');

        $em = $this->getDoctrine()->getManager();

        $mensajeria = new Mensajeria();
        $form = $this->createComentarioForm($mensajeria,$hash);
        $form->handleRequest($request);

        if ($form->isValid())
        {
            $user = $this->get('security.context')->getToken()->getUser();
            $mensajeria->setUser( $user );
            $mensajeria->setDatetime( new \DateTime() );
            $mensajeria->setSolicitudHash( $hash );
            $em->persist($mensajeria);
            $em->flush();

            if( $user->hasRole('ROLE_ADMIN') ) {
                $user = $this->get('campania_manager')->getusercampania($hash);
                $emailTo = $user->getEmail();
            } else {
                $emailTo = 'publi@stagingpubli.com';
            }

            // Para enviar un correo HTML, debe establecerse la cabecera Content-type
            $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
            $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            
            // Cabeceras adicionales
            $cabeceras .= 'From: PUBLIENRADIO <info@publienradio.es>' . "\r\n";
            $cabeceras .= 'Bcc: publi@stagingpubli.com' . "\r\n";

            $message = $this->renderView('WOPPubliRadioBundle:Mail:mensaje_nuevo.html.twig', array('hash' => $hash)) ;
            $subject = 'Has recibido un nuevo comentario';

            mail( $emailTo , $subject , $message , $cabeceras );
        }

        $comentarios = $em
            ->getRepository('WOPPubliRadioBundle:Mensajeria')
            ->findByHash($hash);

        return array( 
            'comentarios' => $comentarios,
            'form' => $this->createComentarioForm(new Mensajeria(),$hash),
            'hash' => $hash
        );
    }

    public function createComentarioForm(Mensajeria $entity, $hash)
    {
        $form = $this->createForm(new MensajeriaType(), $entity, array(
            'action' => $this->generateUrl('wop_publiradio_campania_comentarios', array('hash' => $hash) ),
            'method' => 'POST',
        ));

        $form->add('submit' , 'submit' , array('label' => 'ENVIAR'));
        $form->add('solicitudHash'   , 'hidden' , array('data' => $hash));

        return $form;
    }

    /**
     * @Route("/campania/confirmar/{hash}", name="wop_publiradio_campania_confirmar")
     * @Method("POST")
     */
    public function confirmarpresupuestofinalAction($hash)
    {
        $manager = $this->get('campania_manager');

        $manager->confirmarpresupuestofinal($hash);

        return new Response( json_encode(array('result' => 'success')), 200 );
    }

    private function enviarmailconfirmacionpresupuesto($hash)
    {
        $usuario = $this->get('campania_manager')->getusercampania($hash);

        // Para enviar un correo HTML, debe establecerse la cabecera Content-type
        $cabeceras  = 'MIME-Version: 1.0' . "\r\n";
        $cabeceras .= 'Content-type: text/html; charset=utf-8' . "\r\n";

        // Cabeceras adicionales
        $cabeceras .= 'From: PUBLIENRADIO <info@publienradio.es>' . "\r\n";
        $cabeceras .= 'Bcc: publi@stagingpubli.com' . "\r\n";

        $message = $this->renderView('WOPPubliRadioBundle:Mail:confirmacion_presupuesto.html.twig');
        $subject = 'CONFIRMACION PRESUPUESTO';

        mail( $usuario->getEmail() , $subject , $message , $cabeceras );

    }
}
