<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Precio;

/**
 * Precio controller.
 *
 * @Route("/api/precio", defaults={"_format": "json"})
 */
class PrecioController extends FOSRestController
{
    /**
     * Get Precio entities with some caracteristics.
     *
     * @Route("/fetch/", name="api_precio_fetch")
     * @Method("GET")
     * @Rest\View
     */
    public function fetchAction()
    {
        $repetidora = $_GET['repetidora'];
        $horario = $_GET['horario'];

        $em = $this->getDoctrine()->getManager();

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('WOPPubliRadioBundle:Precio', 'p');
        $rsm->addFieldResult('p', 'id', 'id');     // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('p', 'precio', 'valor');

        $sql = '
            SELECT p.id, p.precio 
            FROM Precio p 
            WHERE p.horario_id = :horario AND p.repetidora_id = :repetidora';

        $entities = $em->createNativeQuery($sql,$rsm)
            ->setParameter('horario',$horario)
            ->setParameter('repetidora',$repetidora)
            ->getResult();

        if( empty($entities) )
            return array();
        return $entities[0];
    }
}
