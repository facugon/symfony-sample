<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest,
    FOS\RestBundle\Controller\FOSRestController,
    FOS\RestBundle\Controller\Annotations\QueryParam ;
//use FOS\RestBundle\Request\ParamFetcherInterface;

use WOP\PubliRadioBundle\Entity\Solicitud ;
use WOP\PubliRadioBundle\Entity\Facturacion ;

/**
 * Solicitud controller. Registro la solicitud
 *
 * @Route("/api/solicitud", defaults={"_format": "json"})
 */
class SolicitudController extends FOSRestController
{
    /**
     * Create new Solicitud
     *
     * @param integer $userid
     *
     * @Route("/{userid}", name="api_solicitud_post")
     * @Method("POST")
     * @Rest\View
     */
    public function postAction($userid)
    {
        if( 0 === strpos($this->getRequest()->headers->get('Content-Type'),'application/json') )
        {
            $em = $this->getDoctrine()->getManager();

            $input = json_decode( $this->getRequest()->getContent(), true );
            $request = $this->getRequest()->request;
            $request->replace( is_array($input) ? $input : array() );

            $selecciones = $request->get('selecciones');
            $solicitud_queryhash = md5( time() );
            $solicitud_datetime = new \DateTime();

            $total = 0 ;

            foreach($selecciones as $seleccion)
            {
                foreach($seleccion['duracionesprecios'] as $dp)
                {
                    $total += $dp['total_calculado'] ;

                    $solicitud = new Solicitud();
                    $solicitud->setUserId( $request->get('userid') );
                    $solicitud->setDateTime( $solicitud_datetime );
                    $solicitud->setQueryHash( $solicitud_queryhash );

                    $solicitud->setRepetidoraId( $seleccion['repetidora_id'] );
                    $solicitud->setRepetidoraNombre('');
                    $solicitud->setPrecioId( $seleccion['precio_id'] );
                    $solicitud->setPrecioTotal( $dp['total_calculado'] );
                    $solicitud->setPrecioDescuento( 0 );
                    $solicitud->setPrecioDuracion( $dp['valor_duracion'] );

                    $solicitud->setEstado( Solicitud::SinPagar );

                    if( $dp['lugar'] == 1 )
                        $solicitud->setZona( 'Nacional' );
                    else
                        $solicitud->setZona( $dp['zona'] );

                    $solicitud->setLugarId( $dp['lugar'] );
                    $solicitud->setLugarNombre('');
                    $solicitud->setProgramaId( $dp['programa_id'] );
                    $solicitud->setProgramaNombre( $dp['programa_nombre'] );
                    $solicitud->setHorarioId( $dp['horario_id'] );
                    $solicitud->setDuracionSegundos( $dp['segundos'] );
                    $solicitud->setCantidadAnuncios( $dp['cantidad'] );

                    $em->persist( $solicitud );
                }
            }

            $facturacion = new Facturacion();
            $facturacion->setCampaniaHash($solicitud_queryhash);
            $em->persist($facturacion);
            $em->flush();

            $this->calculardescuento($total, $solicitud_queryhash);
            return array('result'=>'success');
        }
    }

    private function calculardescuento($total,$hash)
    {
        $logger = $this->get('logger');

        $descuentos = $this->getDoctrine()->getManager()->getRepository('WOPPubliRadioBundle:Descuento')->findAll();
        
        $aplicar = null;
        foreach( $descuentos as $descuento ) {
            if( $descuento->getRangoInicial() < $total && ( $descuento->getRangoFinal() > $total || $descuento->getRangoFinal() == 0 ) )
                $aplicar = $descuento ;
        }

        $logger->info('Aplicando descuento ' . $aplicar);

        //$total * ( $aplicar->getDescuento() / 100 ) ;
        return $this->get('campania_manager')->actualizarpreciocondescuento($aplicar,$hash);
    }
}
