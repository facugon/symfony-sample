<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Provincia;

/**
 * Provincia controller.
 *
 * @Route("/api/provincia", defaults={"_format": "json"})
 */
class ProvinciaController extends FOSRestController
{
    /**
     * Lists all Provincia entities.
     *
     * @Route("/", name="api_provincia_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Provincia')->findAll();

        return array( 'entities' => $entities,);
    }

    /**
     * Get a Provincia
     *
     * @Route("/{id}", name="api_provincia_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Provincia')->find($id);

        if ( ! $entity ) {
            throw $this->createNotFoundException('Provincia not found.');
        }

        return array('entity' => $entity);
    }
}
