<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Doctrine\ORM\Query\ResultSetMapping;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Descuento;

/**
 * Descuento controller.
 *
 * @Route("/api/descuento", defaults={"_format": "json"})
 */
class DescuentoController extends FOSRestController
{
    /**
     * Obtiene el Descuento que aplica a un precio
     *
     * @Route("/find/{precio}", name="api_descuento_find")
     * @Method("GET")
     * @Rest\View
     */
    public function findAction($precio)
    {
        $em = $this->getDoctrine()->getManager();

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('WOPPubliRadioBundle:Descuento', 'd');
        $rsm->addFieldResult('d','id','id');
        $rsm->addFieldResult('d','descuento','descuento');

        $sql = '
            SELECT d.id, d.descuento
            FROM Descuento d
            WHERE d.rango_inicial < :precio AND d.rango_final >= :precio';

        $entity = $em->createNativeQuery($sql,$rsm)
            ->setParameter('precio',$precio)
            ->getResult();

        if( empty($entity) )
            return array();
        return $entity[0];
    }

    /**
     * Lists all Emisora entities.
     *
     * @Route("/", name="api_descuento_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Descuento')->findAll();

        if( empty($entities) )
            return array();

        return $entities;
    }
}
