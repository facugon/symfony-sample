<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Emisora;

/**
 * Emisora controller.
 *
 * @Route("/api/emisora", defaults={"_format": "json"})
 */
class EmisoraController extends FOSRestController
{
    /**
     * Lists all Emisora entities.
     *
     * @Route("/", name="api_emisora_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Emisora')->findAll();

        return array( 'entities' => $entities );
    }

    /**
     * Get a Emisora
     *
     * @Route("/{id}", name="api_emisora_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Emisora')->find($id);

        if ( ! $entity ) {
            throw $this->createNotFoundException('Emisora not found.');
        }

        return array('entity' => $entity);
    }
}
