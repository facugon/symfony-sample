<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\ResultSetMapping;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Programa;

/**
 * Programa controller.
 *
 * @Route("/api/programa", defaults={"_format": "json"})
 */
class ProgramaController extends FOSRestController
{
    /**
     * Lists all Programa entities.
     *
     * @Route("/", name="api_programa_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rsm = new ResultSetMapping;
        $rsm->addEntityResult('WOPPubliRadioBundle:Programa', 'p');
        $rsm->addFieldResult('p', 'pid', 'id');     // ($alias, $columnName, $fieldName)
        $rsm->addFieldResult('p', 'nombre', 'nombre');

        $sql = '
            SELECT p.id pid, p.nombre nombre
            FROM Horario hs
            INNER JOIN Programa p ON p.id = hs.programa_id
            ';

        $entities = $em
            ->createNativeQuery($sql,$rsm)
            ->getResult();

        return array('programas' => $entities);
    }

    /**
     * Get a Programa
     *
     * @Route("/{id}", name="api_programa_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Programa')->find($id);

        if ( ! $entity ) {
            throw $this->createNotFoundException('Programa not found.');
        }

        return array('programa' => $entity);
    }

    /**
     * Lists all Programa entities with some caracteristics.
     *
     * @Route("/fetch/", name="api_programa_fetch")
     * @Method("GET")
     */
    public function fetchAction()
    {
        $repetidora = $_GET['repetidora']; // muy cabeza

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('WOPPubliRadioBundle:Programa')->findAllByRepetidora($repetidora);

        return new Response(json_encode(array('programas' => $entities)), 200);
    }
}
