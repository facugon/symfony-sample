<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Comunidad;

/**
 * Comunidad controller.
 *
 * @Route("/api/comunidad", defaults={"_format": "json"})
 */
class ComunidadController extends FOSRestController
{
    /**
     * Lists all Comunidad entities.
     *
     * @Route("/", name="api_comunidad_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('WOPPubliRadioBundle:Comunidad')->findAll();

        return array( 'entities' => $entities );
    }

    /**
     * Get a Comunidad
     *
     * @Route("/{id}", name="api_comunidad_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Comunidad')->find($id);

        if ( ! $entity ) {
            throw $this->createNotFoundException('Comunidad not found.');
        }

        return array('entity' => $entity);
    }
}
