<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Repetidora;

/**
 * Repetidora controller.
 *
 * @Route("/api/repetidora", defaults={"_format": "json"})
 */
class RepetidoraController extends FOSRestController
{
    /**
     * Lists all Repetidora entities.
     *
     * @Route("/", name="api_repetidora_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $qb = $this->getDoctrine()->getManager()
            ->getRepository('WOPPubliRadioBundle:Repetidora')->createQueryBuilder('r');

        $entities = $qb->select('r.id, r.nombre')
            ->getQuery()
            ->getResult();

        return array( 'entities' => $entities );
    }

    /**
     * Get a Repetidora
     *
     * @Route("/{id}", name="api_repetidora_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $qb = $this->getDoctrine()->getManager()
            ->getRepository('WOPPubliRadioBundle:Repetidora')->createQueryBuilder('r');

        $entity = $qb->select('r.id, r.nombre')
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();

        if ( ! $entity ) {
            throw $this->createNotFoundException('Repetidora not found.');
        }

        return array('entity' => $entity);
    }

    /**
     * Lists all Repetidora entities with some caracteristics.
     *
     * @Route("/{categoria}/{lugar}", 
     *      requirements={"categoria" = "\d"},
     *      name="api_repetidora_fetch"
     *  )
     * @Method("GET")
     * @Rest\View
     */
    public function fetchAction($categoria, $lugar)
    {
        $qb = $this->getDoctrine()->getManager()
            ->getRepository('WOPPubliRadioBundle:Repetidora')->createQueryBuilder('r');

        $qb->select('r.id, r.nombre repetidora, e.nombre emisora, e.path')
            ->join('r.emisora','e')
            ->groupBy('e') ;

        $categorias = array(
            1 => 'Nacional'  , // this should be boolean
            2 => 'Comunidad' , 
            3 => 'Provincia' , 
            4 => 'Municipio'
        );

        switch($categoria)
        {
            case 1 : // cuando es nacional no tiene lugar de alcance. es Nacional
                $qb->where('r.nacional = 1'); 
                break;
            case 2 : 
                $qb->where('r.comunidad = :comunidad')
                    ->setParameter('comunidad',$lugar);
                break;
            case 3 :
                $qb->where('r.provincia = :provincia')
                    ->setParameter('provincia',$lugar);
                break;
            case 4 :
                $qb->where('r.municipio = :muni')
                    ->setParameter('muni',$lugar);
                break;
        }

        $entities = $qb->getQuery()->getResult();

        return array( 'repetidoras' => $entities );
    }
}
