<?php

namespace WOP\PubliRadioAPIBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;

use WOP\PubliRadioBundle\Entity\Municipio;

/**
 * Municipio controller.
 *
 * @Route("/api/municipio", defaults={"_format": "json"})
 */
class MunicipioController extends FOSRestController
{
    /**
     * Lists all Municipio entities.
     *
     * @Route("/", name="api_municipio_getall")
     * @Method("GET")
     * @Rest\View
     */
    public function getallAction()
    {
        $provincia = isset ( $_GET['provincia'] ) ? $_GET['provincia'] : 0 ;
        if( ! empty($provincia) && is_numeric($provincia) )
        {
        $query = $this
            ->container->get('database_connection')
            ->query("SELECT m.id, m.nombre FROM Municipio m WHERE provincia_id = {$provincia} ORDER BY m.nombre");
        return array( 'entities' => $query->fetchAll() );
        }
        else return array();

    }

    /**
     * Get a Municipio
     *
     * @Route("/{id}", name="api_municipio_get")
     * @Method("GET")
     * @Rest\View
     */
    public function getAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('WOPPubliRadioBundle:Municipio')->find($id);

        if ( ! $entity ) {
            throw $this->createNotFoundException('Municipio not found.');
        }

        return array('entity' => $entity);
    }
}
