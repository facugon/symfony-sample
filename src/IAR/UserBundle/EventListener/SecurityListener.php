<?php

namespace IAR\UserBundle\EventListener;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request ;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;


class SecurityListener
{
    protected $router;
    protected $security;
    protected $dispatcher;

    protected $request;

    public function setRequest(Request $request = null)
    {
        $this->request = $request;
    }

    public function __construct(Router $router, SecurityContext $security, ContainerAwareEventDispatcher $dispatcher)
    {
        $this->router = $router;
        $this->security = $security;
        $this->dispatcher = $dispatcher;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->dispatcher->addListener(KernelEvents::RESPONSE, array($this, 'onKernelResponse'));
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->security->isGranted('ROLE_ADMIN') || $this->security->isGranted('ROLE_SUPER_ADMIN')) {
            $route = $this->router->generate('admin');
        }
        else if ($this->security->isGranted('ROLE_USER')) {
            $route = $this->router->generate('wop_publiradio_campania_miscampanias');
        }
        else {
            $route = $this->router->generate('admin');
        }

        $response = $this->request->isXmlHttpRequest() ?
            new Response(json_encode(array(
                'redirection' => $route,
                'user'        => $this->security->getToken()->getUser()->getId()
            ),200)) : 
            new RedirectResponse($route);

        $event->setResponse($response);
    }
}
