<?php

namespace IAR\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class IARUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
