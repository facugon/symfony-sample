<?php

namespace IAR\UserBundle\Form;

//use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationType extends BaseType
//class RegistrationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre', null, array('error_bubbling' => true))
            ->add('apellido', null, array('error_bubbling' => true))
            ->add('email','repeated',array(
                'type'           => 'email',
                'required'       => true,
                'first_options'  => array('label' => 'Email'),
                'second_options' => array('label' => 'Repite Email'),
                'error_bubbling' => true
            ))
            ->add('plainPassword','repeated',array(
                'type'           => 'password',
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'required'       => true,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repite Password'),
                'error_bubbling' => true
            ))
            ->add('captcha','captcha', array('error_bubbling' => true,'as_url' => true, 'reload' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'IAR\UserBundle\Entity\User',
            'csrf_protection' => false,
            'intention'       => 'registration'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'iar_userbundle_registration';
    }
}
