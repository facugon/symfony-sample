<?php

namespace IAR\UserBundle\Controller;

use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse as JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AJAXRegistrationController extends BaseController
{
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->container->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->container->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->container->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if( null !== $event->getResponse() ) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        if( 'POST' === $request->getMethod() )
        {
            $form->bind($request);

            if( $form->isValid() )
            {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                if( $request->isXmlHttpRequest() ) {
                    //$serializer = \JMS\Serializer\SerializerBuilder::create()->build();
                    return new JsonResponse(array('result' => 'success','data' => array('userid' => $user->getId())), 200);
                }
                else {
                    if( null === $response = $event->getResponse() ) {
                        $url = $this->container->get('router')->generate('fos_user_registration_confirmed');
                        $response = new RedirectResponse($url);
                    }
                    $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
                    return $response;
                }
            }
            else if($request->isXmlHttpRequest()) {
                $errors = array();
                foreach( $form->getErrors() as $error )
                    $errors[] = $error->getMessage() ;
                return new JsonResponse(array('error' => 'invalid form data', 'errors' => $errors, 'code' => '2'), 400);
            }
        }

        $tpl = $request->get('modal') == 1 ? 'IARUserBundle:AJAXRegistration:register_modal.html.'.$this->getEngine() : 'IARUserBundle:AJAXRegistration:register.html.'.$this->getEngine() ;

        return $this->container->get('templating')->renderResponse($tpl, array( 'form' => $form->createView()));
    }
}
