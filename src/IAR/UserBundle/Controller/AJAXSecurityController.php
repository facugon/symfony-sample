<?php

namespace IAR\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController as BaseController;

class AJAXSecurityController extends BaseController
{
    protected function renderLogin(array $data)
    {
        $template = sprintf('IARUserBundle:AJAXSecurity:login.html.%s', $this->container->getParameter('fos_user.template.engine'));

        return $this->container->get('templating')->renderResponse($template, $data);
    }
}
