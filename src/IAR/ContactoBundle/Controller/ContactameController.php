<?php

namespace IAR\ContactoBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use IAR\ContactoBundle\Entity\Contactame;
use IAR\ContactoBundle\Form\ContactameType;

/**
 * Contactame controller.
 *
 * @Route("/admin/contactame")
 */
class ContactameController extends Controller
{
    /**
     * Lists all Contactame entities.
     *
     * @Route("/", name="contactame")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('IARContactoBundle:Contactame')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Contactame entity.
     *
     * @Route("/", name="contactame_create")
     * @Method("POST")
     * @Template("IARContactoBundle:Contactame:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Contactame();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('contactame_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
    * Creates a form to create a Contactame entity.
    *
    * @param Contactame $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Contactame $entity)
    {
        $form = $this->createForm(new ContactameType(), $entity, array(
            'action' => $this->generateUrl('contactame_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Contactame entity.
     *
     * @Route("/new", name="contactame_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Contactame();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Contactame entity.
     *
     * @Route("/{id}", name="contactame_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IARContactoBundle:Contactame')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contactame entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Contactame entity.
     *
     * @Route("/{id}/edit", name="contactame_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IARContactoBundle:Contactame')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contactame entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Contactame entity.
    *
    * @param Contactame $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Contactame $entity)
    {
        $form = $this->createForm(new ContactameType(), $entity, array(
            'action' => $this->generateUrl('contactame_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Contactame entity.
     *
     * @Route("/{id}", name="contactame_update")
     * @Method("PUT")
     * @Template("IARContactoBundle:Contactame:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('IARContactoBundle:Contactame')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Contactame entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('contactame_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Contactame entity.
     *
     * @Route("/{id}", name="contactame_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('IARContactoBundle:Contactame')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Contactame entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('contactame'));
    }

    /**
     * Creates a form to delete a Contactame entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contactame_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
