<?php

namespace IAR\ContactoBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactameType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nombre')
            ->add('apellido')
            ->add('email')
            ->add('email','repeated',array(
                'type'           => 'email',
                'required'       => true,
                'first_options'  => array('label' => 'Email'),
                'second_options' => array('label' => 'Repite Email'),
                'error_bubbling' => true
            ))
            ->add('mensaje')
            ->add('captcha','captcha', array('error_bubbling' => true,'as_url' => true, 'reload' => true, 'attr' => array('class' => 'captcha')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'IAR\ContactoBundle\Entity\Contactame'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'iar_contactobundle_contactame';
    }
}
